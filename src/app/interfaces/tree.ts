import { NzTreeNode } from 'ng-zorro-antd';

export interface ITreeNode {
    getTreeNode(): NzTreeNode;
}