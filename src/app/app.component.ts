import { Component, OnInit } from '@angular/core';
import { ConfigurationService, HttpConfigService, AuthService } from './services';
import { UploaderService } from './libs/drive/components/uploader/services/uploader.service';
import { NzNotificationService } from 'ng-zorro-antd';
import { HttpRequest } from '@angular/common/http';
import { NotificationConfig } from './notification.config';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  exportAs: 'app-root',
})
export class AppComponent implements OnInit {
  config: any;

  public constructor(
    private configurationService: ConfigurationService, 
    private httpConfigService: HttpConfigService, 
    private uploderService: UploaderService,
    private authService: AuthService,
    private notificationService: NzNotificationService
    ) { }
  ngOnInit(): void {

    this.configurationService.loadConfiguration('assets/config.json').subscribe(config => {
      this.config = config;
      this.uploderService.configure(config.drive.url);

      this.httpConfigService.on401UnAuthorize.subscribe(() => {
        if (confirm('شما دسترسی لازم را ندارید\nآیا تمایل به ورود مجدد را دارید؟')) {
          this.authService.logout().subscribe(op => {
            location.href = config.login;
          });
        }
      });
    });
    NotificationConfig.Configure(this.notificationService, this.httpConfigService);
  }
}
