import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { RequestHistoryGetDto, RequestHistoryAddDto, RequestHistoryChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class RequestHistoryService extends BaseCrudService<RequestHistoryGetDto, RequestHistoryAddDto, RequestHistoryChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.requestHistory);
        this.configureMakers(RequestHistoryGetDto, RequestHistoryAddDto, RequestHistoryChangeDto);
    }
}