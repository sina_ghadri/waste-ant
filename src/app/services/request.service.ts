import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { RequestGetDto, RequestAddDto, RequestChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class RequestService extends BaseCrudService<RequestGetDto, RequestAddDto, RequestChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.request);
        this.configureMakers(RequestGetDto, RequestAddDto, RequestChangeDto);
    }
}