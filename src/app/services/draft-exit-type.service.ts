import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { DraftExitTypeGetDto, DraftExitTypeAddDto, DraftExitTypeChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class DraftExitTypeService extends BaseCrudService<DraftExitTypeGetDto, DraftExitTypeAddDto, DraftExitTypeChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.draftExitType);
        this.configureMakers(DraftExitTypeGetDto, DraftExitTypeAddDto, DraftExitTypeChangeDto);
    }
}