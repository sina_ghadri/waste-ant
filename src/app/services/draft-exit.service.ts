import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { DraftExitGetDto, DraftExitAddDto, DraftExitChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class DraftExitService extends BaseCrudService<DraftExitGetDto, DraftExitAddDto, DraftExitChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.draftExit);
        this.configureMakers(DraftExitGetDto, DraftExitAddDto, DraftExitChangeDto);
    }
}