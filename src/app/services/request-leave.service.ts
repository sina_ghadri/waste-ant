import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { RequestLeaveGetDto, RequestLeaveAddDto, RequestLeaveChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class RequestLeaveService extends BaseCrudService<RequestLeaveGetDto, RequestLeaveAddDto, RequestLeaveChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.requestLeave);
        this.configureMakers(RequestLeaveGetDto, RequestLeaveAddDto, RequestLeaveChangeDto);
    }
}