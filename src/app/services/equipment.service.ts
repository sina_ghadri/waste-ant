import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { EquipmentGetDto, EquipmentAddDto, EquipmentChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class EquipmentService extends BaseCrudService<EquipmentGetDto, EquipmentAddDto, EquipmentChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.equipment);
        this.configureMakers(EquipmentGetDto, EquipmentAddDto, EquipmentChangeDto);
    }
}