import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { RouteItemGetDto, RouteItemAddDto, RouteItemChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class RouteItemService extends BaseCrudService<RouteItemGetDto, RouteItemAddDto, RouteItemChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.routeItem);
        this.configureMakers(RouteItemGetDto, RouteItemAddDto, RouteItemChangeDto);
    }
}