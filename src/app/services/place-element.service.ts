import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { PlaceElementGetDto, PlaceElementAddDto, PlaceElementChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class PlaceElementService extends BaseCrudService<PlaceElementGetDto, PlaceElementAddDto, PlaceElementChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.placeElement);
        this.configureMakers(PlaceElementGetDto, PlaceElementAddDto, PlaceElementChangeDto);
    }
}