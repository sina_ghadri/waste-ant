import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { CleaningAreaGetDto, CleaningAreaAddDto, CleaningAreaChangeDto, OperationResultType, Cordination } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class CleaningAreaService extends BaseCrudService<CleaningAreaGetDto, CleaningAreaAddDto, CleaningAreaChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.cleaningArea);
        this.configureMakers(CleaningAreaGetDto, CleaningAreaAddDto, CleaningAreaChangeDto);
    }
    setCordination(id: number, cord: Cordination): Observable<OperationResultType<CleaningAreaGetDto>> {
        return this.http.post<OperationResultType<CleaningAreaGetDto>>(this.baseUrl + `${id}/Cordination`, cord);
    }
    removeCordination(id: number): Observable<OperationResultType<CleaningAreaGetDto>> {
        return this.http.delete<OperationResultType<CleaningAreaGetDto>>(this.baseUrl + `${id}/Cordination`);
    }
}