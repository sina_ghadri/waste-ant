import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { UserService } from '../libs/identity/services';
import { ICrudService } from '../interfaces';
import { ConfigurationService } from '../libs/bitspco/services';
import { Observable } from 'rxjs';
import { OperationResultType, UserPlaceGetDto, UserPlaceAddDto, UserUseTypeAddDto, UserUseTypeGetDto, UserGetDto } from '../models';

@Injectable({providedIn: 'root'})
export class UserExtendedService extends UserService implements ICrudService {
    private _baseUrl: string;

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService);

        this._baseUrl = configService.configuration.api.user;
    }

    getAllPlace(id: number): Observable<OperationResultType<UserPlaceGetDto[]>> {
        return this.http.get<OperationResultType<UserPlaceGetDto[]>>(this._baseUrl + `${id}/Places`);
    }
    getAllUseType(id: number): Observable<OperationResultType<UserUseTypeGetDto[]>> {
        return this.http.get<OperationResultType<UserUseTypeGetDto[]>>(this._baseUrl + `${id}/UseTypes`);
    }
    addPlace(id: number, obj: UserPlaceAddDto): Observable<OperationResultType<UserPlaceGetDto>> {
        return this.http.post<OperationResultType<UserPlaceGetDto>>(this._baseUrl + `${id}/Places`, obj);
    }
    
    addUseType(id: number, obj: UserUseTypeAddDto): Observable<OperationResultType<UserUseTypeGetDto>> {
        return this.http.post<OperationResultType<UserUseTypeGetDto>>(this._baseUrl + `${id}/UseTypes`, obj);
    }
    
    removePlace(id: number, placeId: number): Observable<OperationResultType<UserPlaceGetDto>> {
        return this.http.delete<OperationResultType<UserPlaceGetDto>>(this._baseUrl + `${id}/Places/${placeId}`);
    }
    
    removeUseType(id: number, useTypeId: number): Observable<OperationResultType<UserUseTypeGetDto>> {
        return this.http.delete<OperationResultType<UserUseTypeGetDto>>(this._baseUrl + `${id}/UseTypes/${useTypeId}`);
    }
}