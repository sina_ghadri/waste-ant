import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { ShiftPartGetDto, ShiftPartAddDto, ShiftPartChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class ShiftPartService extends BaseCrudService<ShiftPartGetDto, ShiftPartAddDto, ShiftPartChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.shiftPart);
        this.configureMakers(ShiftPartGetDto, ShiftPartAddDto, ShiftPartChangeDto);
    }
}