import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { DocumentGetDto, DocumentAddDto, DocumentChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class DocumentService extends BaseCrudService<DocumentGetDto, DocumentAddDto, DocumentChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.document);
        this.configureMakers(DocumentGetDto, DocumentAddDto, DocumentChangeDto);
    }
}