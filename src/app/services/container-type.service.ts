import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { ContainerTypeGetDto, ContainerTypeAddDto, ContainerTypeChangeDto, Color, OperationResultType, OperationResult } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class ContainerTypeService extends BaseCrudService<ContainerTypeGetDto, ContainerTypeAddDto, ContainerTypeChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.containerType);
        this.configureMakers(ContainerTypeGetDto, ContainerTypeAddDto, ContainerTypeChangeDto);
    }

    getColors(): Observable<OperationResultType<Color[]>> {
        return new Observable(ob => {
            const op = new OperationResultType<Color[]>();
            op.Success = true;
            op.Data = [
                { Code: 'red', Name: 'قرمز' },
                { Code: 'blue', Name: 'آبی' },
                { Code: 'yellow', Name: 'زرد' },
                { Code: 'green', Name: 'سبز' },
                { Code: 'pink', Name: 'صورتی' },
            ];
            ob.next(op);
        })
    }
}