import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { RequestSwapGetDto, RequestSwapAddDto, RequestSwapChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class RequestSwapService extends BaseCrudService<RequestSwapGetDto, RequestSwapAddDto, RequestSwapChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.requestSwap);
        this.configureMakers(RequestSwapGetDto, RequestSwapAddDto, RequestSwapChangeDto);
    }
}