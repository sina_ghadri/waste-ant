import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { DayPartScheduleGetDto, DayPartScheduleAddDto, DayPartScheduleChangeDto, OperationResultType } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class DayPartScheduleService extends BaseCrudService<DayPartScheduleGetDto, DayPartScheduleAddDto, DayPartScheduleChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.dayPartSchedule);
        this.configureMakers(DayPartScheduleGetDto, DayPartScheduleAddDto, DayPartScheduleChangeDto);
    }

    active(id: number): Observable<OperationResultType<DayPartScheduleGetDto>> {
        return this.http.put<OperationResultType<DayPartScheduleGetDto>>(this.baseUrl + `${id}/Active`, {});
    }
    deactive(id: number): Observable<OperationResultType<DayPartScheduleGetDto>> {
        return this.http.put<OperationResultType<DayPartScheduleGetDto>>(this.baseUrl + `${id}/Deactive`, {});
    }
}