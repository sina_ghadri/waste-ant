import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { WasteTypeGetDto, WasteTypeAddDto, WasteTypeChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class WasteTypeService extends BaseCrudService<WasteTypeGetDto, WasteTypeAddDto, WasteTypeChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.wasteType);
        this.configureMakers(WasteTypeGetDto, WasteTypeAddDto, WasteTypeChangeDto);
    }
}