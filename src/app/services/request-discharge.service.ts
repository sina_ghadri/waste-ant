import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { RequestDischargeGetDto, RequestDischargeAddDto, RequestDischargeChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class RequestDischargeService extends BaseCrudService<RequestDischargeGetDto, RequestDischargeAddDto, RequestDischargeChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.requestDischarge);
        this.configureMakers(RequestDischargeGetDto, RequestDischargeAddDto, RequestDischargeChangeDto);
    }
}