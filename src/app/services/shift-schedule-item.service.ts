import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { ShiftScheduleItemGetDto, ShiftScheduleItemAddDto, ShiftScheduleItemChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class ShiftScheduleItemService extends BaseCrudService<ShiftScheduleItemGetDto, ShiftScheduleItemAddDto, ShiftScheduleItemChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.shiftScheduleItem);
        this.configureMakers(ShiftScheduleItemGetDto, ShiftScheduleItemAddDto, ShiftScheduleItemChangeDto);
    }
}