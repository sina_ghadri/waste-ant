import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { PlaceGetDto, PlaceAddDto, PlaceChangeDto, OperationResultCount, OperationResultType, Cordination, StationGetDto, RouteGetDto, ElevatorGetDto, CleaningAreaGetDto, PlaceElementGetDto, RouteItemGetDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';
import { KeyValue } from '@angular/common';
import { StationService } from './station.service';
import { PlaceElementService } from './place-element.service';
import { RouteService } from './route.service';
import { RouteItemService } from './route-item.service';
import { CleaningAreaService } from './cleaning-area.service';
import { ElevatorService } from './elevator.service';

@Injectable({ providedIn: 'root' })
export class PlaceService extends BaseCrudService<PlaceGetDto, PlaceAddDto, PlaceChangeDto> implements ICrudService {

    constructor(
        http: HttpClient, 
        configService: ConfigurationService,
        private stationService: StationService,
        private placeElementService: PlaceElementService,
        private routeService: RouteService,
        private routeItemService: RouteItemService,
        private cleaningAreaService: CleaningAreaService,
        private elevatorService: ElevatorService,
        ) {
        super(http, configService.configuration.api.place);
        this.configureMakers(PlaceGetDto, PlaceAddDto, PlaceChangeDto);
    }

    getTypes(): Observable<OperationResultType<KeyValue<number, string>[]>> {
        return new Observable(ob => {
            const op = new OperationResultType<KeyValue<number, string>[]>();
            op.Success = true;
            op.Data = [
                { key: 0, value: 'عادی' },
                { key: 1, value: 'آسانسور' },
                { key: 2, value: 'انبار' },
            ];
            ob.next(op);
        })
    }
    getByParentId(parentId?: number): Observable<OperationResultCount<PlaceGetDto[]>> {
        if (!parentId) parentId = 0;
        return this.cache.getItem('getByParentId/' + parentId, this.http.get<OperationResultCount<PlaceGetDto[]>>(this.baseUrl + `${parentId}/Children`).map(op => this.mapper(op)));
    }
    getStations(id: number): Observable<OperationResultType<StationGetDto[]>> {
        return this.cache.getItem('getStations/' + id, this.http.get<OperationResultType<StationGetDto[]>>(this.baseUrl + `${id}/Stations`).map(op => this.stationService.mapper(op)));
    }
    getElements(id: number): Observable<OperationResultType<PlaceElementGetDto[]>> {
        return this.cache.getItem('getElements/' + id, this.http.get<OperationResultType<PlaceElementGetDto[]>>(this.baseUrl + `${id}/Elements`).map(op => this.placeElementService.mapper(op)));
    }
    getRoutes(id: number): Observable<OperationResultType<RouteGetDto[]>> {
        return this.cache.getItem('getRoutes/' + id, this.http.get<OperationResultType<RouteGetDto[]>>(this.baseUrl + `${id}/Routes`).map(op => this.routeService.mapper(op)));
    }
    getRoutItems(id: number): Observable<OperationResultType<RouteItemGetDto[]>> {
        return this.cache.getItem('getRoutItems/' + id, this.http.get<OperationResultType<RouteItemGetDto[]>>(this.baseUrl + `${id}/RouteItems`).map(op => this.routeItemService.mapper(op)));
    }
    getCleaningAreas(id: number): Observable<OperationResultType<CleaningAreaGetDto[]>> {
        return this.cache.getItem('getCleaningAreas/' + id, this.http.get<OperationResultType<CleaningAreaGetDto[]>>(this.baseUrl + `${id}/CleaningAreas`).map(op => this.cleaningAreaService.mapper(op)));
    }
    getElevators(id: number): Observable<OperationResultType<ElevatorGetDto[]>> {
        return this.cache.getItem('getElevators/' + id, this.http.get<OperationResultType<ElevatorGetDto[]>>(this.baseUrl + `${id}/Elevators`).map(op => this.elevatorService.mapper(op)));
    }
    getDynamicImageUrl(id: number, cord: Cordination): string {
        return this.baseUrl + `${id}/Image?x=${cord.X}&y=${cord.Y}&width=${cord.Width}&height=${cord.Height}`;
    }
    setCordination(id: number, cord: Cordination): Observable<OperationResultType<PlaceGetDto>> {
        return this.http.post<OperationResultType<PlaceGetDto>>(this.baseUrl + `${id}/Cordination`, cord).map(op => this.mapper(op));
    }
    removeCordination(id: number): Observable<OperationResultType<PlaceGetDto>> {
        return this.http.delete<OperationResultType<PlaceGetDto>>(this.baseUrl + `${id}/Cordination`).map(op => this.mapper(op));
    }
}