import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { RequestCleaningGetDto, RequestCleaningAddDto, RequestCleaningChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class RequestCleaningService extends BaseCrudService<RequestCleaningGetDto, RequestCleaningAddDto, RequestCleaningChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.requestCleaning);
        this.configureMakers(RequestCleaningGetDto, RequestCleaningAddDto, RequestCleaningChangeDto);
    }
}