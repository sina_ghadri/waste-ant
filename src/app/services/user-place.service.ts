import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { UserPlaceGetDto, UserPlaceAddDto, UserPlaceChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class UserPlaceService extends BaseCrudService<UserPlaceGetDto, UserPlaceAddDto, UserPlaceChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.userPlace);
        this.configureMakers(UserPlaceGetDto, UserPlaceAddDto, UserPlaceChangeDto);
    }
}