import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { StationGetDto, StationAddDto, StationChangeDto, OperationResultType, Cordination, InspectionGetDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class StationService extends BaseCrudService<StationGetDto, StationAddDto, StationChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.station);
        this.configureMakers(StationGetDto, StationAddDto, StationChangeDto);
    }
    getLastInspection(id: number): Observable<OperationResultType<InspectionGetDto>> {
        return this.http.get<OperationResultType<InspectionGetDto>>(this.baseUrl + `${id}/LastInspection`);
    }
    setCordination(id: number, cord: Cordination): Observable<OperationResultType<StationGetDto>> {
        return this.http.post<OperationResultType<StationGetDto>>(this.baseUrl + `${id}/Cordination`, cord);
    }
    removeCordination(id: number): Observable<OperationResultType<StationGetDto>> {
        return this.http.delete<OperationResultType<StationGetDto>>(this.baseUrl + `${id}/Cordination`);
    }
}