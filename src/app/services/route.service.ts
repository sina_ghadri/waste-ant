import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { RouteGetDto, RouteAddDto, RouteChangeDto, RouteItemAddDto, OperationResultType, RouteItemGetDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class RouteService extends BaseCrudService<RouteGetDto, RouteAddDto, RouteChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.route);
        this.configureMakers(RouteGetDto, RouteAddDto, RouteChangeDto);
    }
    getItems(id: number): Observable<OperationResultType<RouteItemGetDto>> {
        return this.http.get<OperationResultType<RouteItemGetDto>>(this.baseUrl + `${id}/Items`);
    }
    addItem(id: number, item: RouteItemAddDto): Observable<OperationResultType<RouteItemGetDto>> {
        return this.http.post<OperationResultType<RouteItemGetDto>>(this.baseUrl + `${id}/Items`, item);
    }
    removeItem(id: number, stationId: number): Observable<OperationResultType<RouteItemGetDto>> {
        return this.http.delete<OperationResultType<RouteItemGetDto>>(this.baseUrl + `${id}/Items/${stationId}`);
    }
}