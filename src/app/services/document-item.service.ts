import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { DocumentItemGetDto, DocumentItemAddDto, DocumentItemChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class DocumentItemService extends BaseCrudService<DocumentItemGetDto, DocumentItemAddDto, DocumentItemChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.documentItem);
        this.configureMakers(DocumentItemGetDto, DocumentItemAddDto, DocumentItemChangeDto);
    }
}