import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { DayPartScheduleItemGetDto, DayPartScheduleItemAddDto, DayPartScheduleItemChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class DayPartScheduleItemService extends BaseCrudService<DayPartScheduleItemGetDto, DayPartScheduleItemAddDto, DayPartScheduleItemChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.dayPartScheduleItem);
        this.configureMakers(DayPartScheduleItemGetDto, DayPartScheduleItemAddDto, DayPartScheduleItemChangeDto);
    }
}