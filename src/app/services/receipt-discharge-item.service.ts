import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { ReceiptDischargeItemGetDto, ReceiptDischargeItemAddDto, ReceiptDischargeItemChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class ReceiptDischargeItemService extends BaseCrudService<ReceiptDischargeItemGetDto, ReceiptDischargeItemAddDto, ReceiptDischargeItemChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.receiptDischargeItem);
        this.configureMakers(ReceiptDischargeItemGetDto, ReceiptDischargeItemAddDto, ReceiptDischargeItemChangeDto);
    }
}