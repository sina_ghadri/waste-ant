import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { ShiftGetDto, ShiftAddDto, ShiftChangeDto, OperationResultType, ShiftPartGetDto, ShiftRoleGetDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class ShiftService extends BaseCrudService<ShiftGetDto, ShiftAddDto, ShiftChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.shift);
        this.configureMakers(ShiftGetDto, ShiftAddDto, ShiftChangeDto);
    }

    getAllPart(id: number): Observable<OperationResultType<ShiftPartGetDto[]>> {
        return this.http.get<OperationResultType<ShiftPartGetDto[]>>(this.baseUrl + `${id}/Parts`);
    }
    getAllRoles(id: number): Observable<OperationResultType<ShiftRoleGetDto[]>> {
        return this.http.get<OperationResultType<ShiftRoleGetDto[]>>(this.baseUrl + `${id}/Roles`);
    }
}