import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { ElementGetDto, ElementAddDto, ElementChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class ElementService extends BaseCrudService<ElementGetDto, ElementAddDto, ElementChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.element);
        this.configureMakers(ElementGetDto, ElementAddDto, ElementChangeDto);
    }
}