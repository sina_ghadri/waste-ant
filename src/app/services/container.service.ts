import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { ContainerGetDto, ContainerAddDto, ContainerChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class ContainerService extends BaseCrudService<ContainerGetDto, ContainerAddDto, ContainerChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.container);
        this.configureMakers(ContainerGetDto, ContainerAddDto, ContainerChangeDto);
    }
}