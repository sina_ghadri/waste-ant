import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { DayTypeGetDto, DayTypeAddDto, DayTypeChangeDto, Color, OperationResultType, DayPartGetDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class DayTypeService extends BaseCrudService<DayTypeGetDto, DayTypeAddDto, DayTypeChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.dayType);
        this.configureMakers(DayTypeGetDto, DayTypeAddDto, DayTypeChangeDto);
    }

    getColors(): Observable<OperationResultType<Color[]>> {
        return new Observable(ob => {
            const op = new OperationResultType<Color[]>();
            op.Success = true;
            op.Data = [
                { Code: '#f5222d', Name: 'قرمز' },
                { Code: '#096dd9', Name: 'آبی' },
                { Code: '#faad14', Name: 'زرد' },
                { Code: '#41c1a8', Name: 'سبز' },
                { Code: '#945cab', Name: 'بنفش' },
                { Code: '#41c1a8', Name: 'اقیانوسی' },
            ];
            ob.next(op);
        })
    }

    getParts(id: number): Observable<OperationResultType<DayPartGetDto[]>> {
        return this.http.get<OperationResultType<DayPartGetDto[]>>(this.baseUrl + id + '/Parts');
    }
}