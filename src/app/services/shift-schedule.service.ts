import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { ShiftScheduleGetDto, ShiftScheduleAddDto, ShiftScheduleChangeDto, OperationResultType } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class ShiftScheduleService extends BaseCrudService<ShiftScheduleGetDto, ShiftScheduleAddDto, ShiftScheduleChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.shiftSchedule);
        this.configureMakers(ShiftScheduleGetDto, ShiftScheduleAddDto, ShiftScheduleChangeDto);
    }

    
    getAllByRange(roleId: number, placeId: number, from: string, to: string): Observable<OperationResultType<ShiftScheduleGetDto[]>> {
        return this.http.get<OperationResultType<ShiftScheduleGetDto[]>>(this.baseUrl + `${roleId}/${placeId}/${from}/${to}`).map(op => this.mapper(op));
    }
    setStartTime(id: number, time: string): Observable<OperationResultType<ShiftScheduleGetDto>> {
        return this.http.put<OperationResultType<ShiftScheduleGetDto>>(this.baseUrl + `${id}/SetStartTime/${time}`, {});
    }
    setEndTime(id: number, time: string): Observable<OperationResultType<ShiftScheduleGetDto>> {
        return this.http.put<OperationResultType<ShiftScheduleGetDto>>(this.baseUrl + `${id}/SetEndTime/${time}`, {});
    }
    
}