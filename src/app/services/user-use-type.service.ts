import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { UserUseTypeGetDto, UserUseTypeAddDto, UserUseTypeChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class UserUseTypeService extends BaseCrudService<UserUseTypeGetDto, UserUseTypeAddDto, UserUseTypeChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.userUseType);
        this.configureMakers(UserUseTypeGetDto, UserUseTypeAddDto, UserUseTypeChangeDto);
    }
}