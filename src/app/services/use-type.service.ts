import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { UseTypeGetDto, UseTypeAddDto, UseTypeChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class UseTypeService extends BaseCrudService<UseTypeGetDto, UseTypeAddDto, UseTypeChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.useType);
        this.configureMakers(UseTypeGetDto, UseTypeAddDto, UseTypeChangeDto);
    }
}