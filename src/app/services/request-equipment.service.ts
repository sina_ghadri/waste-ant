import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { RequestEquipmentGetDto, RequestEquipmentAddDto, RequestEquipmentChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class RequestEquipmentService extends BaseCrudService<RequestEquipmentGetDto, RequestEquipmentAddDto, RequestEquipmentChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.requestEquipment);
        this.configureMakers(RequestEquipmentGetDto, RequestEquipmentAddDto, RequestEquipmentChangeDto);
    }
}