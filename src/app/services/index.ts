export * from '../libs/bitspco/services';
export * from '../libs/identity/services';

export * from './calendar.service';
export * from './cleaning-area-equipment.service';
export * from './cleaning-area.service';
export * from './container-type.service';
export * from './container.service';
export * from './day-part-name.service';
export * from './day-part-schedule-item.service';
export * from './day-part-schedule.service';
export * from './day-part.service';
export * from './day-type.service';
export * from './document-item.service';
export * from './document.service';
export * from './draft-exit-type.service';
export * from './draft-exit.service';
export * from './element.service';
export * from './elevator.service';
export * from './equipment.service';
export * from './inspection-index.service';
export * from './inspection.service';
export * from './place-element.service';
export * from './place.service';
export * from './place.service';
export * from './receipt-discharge-item.service';
export * from './receipt-discharge.service';
export * from './receipt-swap-item.service';
export * from './receipt-swap.service';
export * from './receipt-swap.service';
export * from './receipt-swap.service';
export * from './receipt-swap.service';
export * from './receipt-swap.service';
export * from './request-cleaning.service';
export * from './request-discharge.service';
export * from './request-elevator.service';
export * from './request-equipment.service';
export * from './request-history.service';
export * from './request-leave.service';
export * from './request-swap.service';
export * from './request.service';
export * from './route-item.service';
export * from './route.service';
export * from './setting.service';
export * from './shift-part-name.service';
export * from './shift-part.service';
export * from './shift-role.service';
export * from './shift-schedule-item.service';
export * from './shift-schedule.service';
export * from './shift.service';
export * from './station.service';
export * from './use-type-index.service';
export * from './use-type.service';
export * from './user.service';
export * from './user-place.service';
export * from './user-use-type.service';
export * from './waste-type.service';





import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { IndexGetDto, IndexAddDto, IndexChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class IndexService extends BaseCrudService<IndexGetDto, IndexAddDto, IndexChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.indices);
        this.configureMakers(IndexGetDto, IndexAddDto, IndexChangeDto);
    }
}