import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { CalendarGetDto, CalendarAddDto, CalendarChangeDto, OperationResultType } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class CalendarService extends BaseCrudService<CalendarGetDto, CalendarAddDto, CalendarChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.calendar);
        this.configureMakers(CalendarGetDto, CalendarAddDto, CalendarChangeDto);
    }

    getAllByTimeRange(from: string, to: string): Observable<OperationResultType<CalendarGetDto[]>> {
        return this.http.get<OperationResultType<CalendarGetDto[]>>(this.baseUrl + `${from}/${to}`);
    }
    setDayType(date: string, dayTypeId: number): Observable<OperationResultType<CalendarGetDto>> {
        return this.http.put<OperationResultType<CalendarGetDto>>(this.baseUrl + `${date}/SetDayType/${dayTypeId}`, {});
    }
}