import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { ReceiptSwapItemGetDto, ReceiptSwapItemAddDto, ReceiptSwapItemChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class ReceiptSwapItemService extends BaseCrudService<ReceiptSwapItemGetDto, ReceiptSwapItemAddDto, ReceiptSwapItemChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.receiptSwapItem);
        this.configureMakers(ReceiptSwapItemGetDto, ReceiptSwapItemAddDto, ReceiptSwapItemChangeDto);
    }
}