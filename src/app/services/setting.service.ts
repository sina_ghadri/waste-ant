import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../libs/bitspco/services/configuration.service';
import { SettingGetDto, SettingAddDto, SettingChangeDto } from '../models';
import { ICrudService } from '../libs/bitspco/interfaces/crud-service';
import { BaseCrudService } from '../libs/bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class SettingService extends BaseCrudService<SettingGetDto, SettingAddDto, SettingChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.api.setting);
        this.configureMakers(SettingGetDto, SettingAddDto, SettingChangeDto);
    }
}