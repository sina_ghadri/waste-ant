import { NzNotificationService } from 'ng-zorro-antd';
import { HttpConfigService, HttpContext, HttpStatus, HttpMethod } from './services';
import { HttpRequest } from '@angular/common/http';

export class NotificationConfig {
    static Configure(notification: NzNotificationService, http: HttpConfigService) {
        setTimeout(() => {
            notification.config({
                nzPlacement: 'bottomLeft'
            });
        }, 1000)
        http.onsuccess.subscribe((context: HttpContext) => {
            let message = '';
            let title = 'موفقیت';
            let type = 'success';
            switch (context.Request.method) {
                case HttpMethod.GET: break;
                case HttpMethod.POST: message = 'عملیات ثبت با موفقیت انجام شد'; break;
                case HttpMethod.PATCH: message = 'تغییرات با موفقیت انجام شد'; break;
                case HttpMethod.PUT: message = 'عملیات با موفقیت انجام شد'; break;
                case HttpMethod.DELETE: message = 'عملیات حذف با موفقیت انجام شد'; break;
            }
            const body = context.Response.body;
            if (body.Message) {
                if (body.Success === false) {
                    type = 'error';
                    title = 'خطا';
                }
                message = body.Message;
            }
            if (message) notification.create(type, title, message);
        });
        http.onerror.subscribe((context: HttpContext) => {
            let message = '';
            let title = '';
            switch(context.Response.status)
            {
                case HttpStatus.NoResponse: title = 'خطا'; message = 'ارتباط با سرور ممکن نیست'; break;
                case HttpStatus.BadRequest: title = 'خطا ارسال درخواست'; message = 'ارسال درخواست نامعتبر (Bad Request)'; break;
                case HttpStatus.UnAuthorize: title = 'عدم دسترسی'; message = 'دسترسی چنین درخواستی را ندارید (UnAuthorize)'; break;

                case HttpStatus.InternalServerError: title = 'خطا سرور'; message = 'خطا در سیستم (Internal Server Error)'; break;
                case HttpStatus.InternalServerError: title = 'سرور خطا'; message = 'درگاه نامعتبر (Bad Gateway)'; break;
            }
            if (context.Response.body && context.Response.body.Message && context.Response.body.Message.length < 30) message = context.Response.body.Message;
            notification.create('error', title, message);
        });
    }
}