import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RequestDischargeRoutingModule } from './request-discharge.routing';
import { RequestDischargeIndexComponent } from './index/index.component';
import { RequestDischargeEditComponent } from './edit/edit.component';
import { RequestDischargeDetailComponent } from './detail/detail.component';
import { RequestDischargeCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RequestDischargeRoutingModule,
    ],
    declarations: [
        RequestDischargeIndexComponent,
        RequestDischargeCreateComponent,
        RequestDischargeEditComponent,
        RequestDischargeDetailComponent,
    ]
})
export class RequestDischargeModule {}