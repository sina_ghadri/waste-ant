import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RequestDischargeService } from "../../../services";
import { RequestDischargeGetDto, RequestDischargeAddDto, RequestDischargeChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-request-discharge-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RequestDischargeIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: RequestDischargeService;

  constructor(service: RequestDischargeService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
