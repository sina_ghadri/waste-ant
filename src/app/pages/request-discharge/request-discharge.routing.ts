import { RouterModule } from '@angular/router';
import { RequestDischargeIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: RequestDischargeIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class RequestDischargeRoutingModule {}