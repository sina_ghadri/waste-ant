import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestDischargeService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RequestDischargeAddDto } from 'src/app/models';

@Component({
    selector: 'app-request-discharge-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RequestDischargeCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:RequestDischargeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}