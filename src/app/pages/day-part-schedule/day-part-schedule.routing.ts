import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DayPartScheduleComponent } from './day-part-schedule.component';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: DayPartScheduleComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class DayPartScheduleRoutingModule {}