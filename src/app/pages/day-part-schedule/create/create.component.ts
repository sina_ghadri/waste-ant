import { Component, OnInit, Input } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartScheduleService, DayTypeService, DayPartService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { DayPartScheduleAddDto, DayTypeGetDto, DayPartGetDto, DayPartScheduleItemAddDto, DayPartScheduleType } from 'src/app/models';
import { FormGroup, FormArray, FormControl, AbstractControl } from '@angular/forms';

@Component({
    selector: 'app-day-part-schedule-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class DayPartScheduleCreateComponent extends BaseCreateComponent implements OnInit {
    @Input() placeId: number;

    dayTypes: DayTypeGetDto[];
    dayParts: DayPartGetDto[];

    isLoadingParts: boolean = false;

    get Items(): DayPartScheduleItemAddDto[] { return this.f.Items; }

    constructor(
        private dayTypeService: DayTypeService,
        private dayPartService: DayPartService,
        private currentService: DayPartScheduleService,
        formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.dayTypeService.getAll().subscribe(op => this.dayTypes = op.Data);
    }
    getItems(): AbstractControl[] {
        return (this.form.controls.Items as FormArray).controls;
    }
    dayTypeChanged() {
        this.isLoadingParts = true;
        this.dayTypeService.getParts(this.f.DayTypeId).subscribe(op => this.dayParts = op.Data, e => this.isLoadingParts = false, () => this.isLoadingParts = false);
    }
    makeFormGroup(obj: any): FormGroup {
        obj = new DayPartScheduleAddDto;
        obj.PlaceId = this.placeId;
        return super.makeFormGroup(obj);
    }

    addNormalCleaningItem() {
        this.addItem(DayPartScheduleType.NormalCleaning);
    }
    addMinorCleaningItem() {
        this.addItem(DayPartScheduleType.MinorCleaning);
    }
    addItem(type: DayPartScheduleType) {
        const item = new DayPartScheduleItemAddDto;
        item.Type = type;
        let items = this.form.controls.Items as FormArray;
        items.controls.push(this.fb.formGroup(item));
    }
    removeItem(item: FormControl) {
        let items = this.form.controls.Items as FormArray;
        items.controls.splice(items.controls.indexOf(item), 1);
    }
}