import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartScheduleService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { DayPartScheduleGetDto } from 'src/app/models';

@Component({
    selector: 'app-day-part-schedule-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DayPartScheduleDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:DayPartScheduleService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}