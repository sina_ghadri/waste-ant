import { Component, OnInit, TemplateRef, ViewChild, SimpleChanges, Input } from "@angular/core";
import { DayPartScheduleService, DayPartScheduleItemService } from "../../../services";
import { DayPartScheduleGetDto, DayPartScheduleAddDto, DayPartScheduleChangeDto, ODataQueryOptions, DayPartScheduleItemGetDto, DayPartScheduleType } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-day-part-schedule-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class DayPartScheduleIndexComponent extends BaseTableLayoutComponent implements OnInit {
  @Input() placeId: number;
  private currentService: DayPartScheduleService;

  constructor(
    service: DayPartScheduleService,
    dayPartScheduleItemService: DayPartScheduleItemService,
    modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    super.ngOnInit();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.placeId) {
      this.load();
      this.pageIndex = 1;
    }
  }
  load(query?: ODataQueryOptions) {
    query = new ODataQueryOptions;
    if (this.placeId) query.filter = `PlaceId eq ${this.placeId}`;
    super.load(query);
  }
  create() {
    this.selectedItem = new DayPartScheduleAddDto;
    this.selectedItem.PlaceId = this.placeId;
    super.create();
  }

  getNormalItems(items: DayPartScheduleItemGetDto[]) {
    return items.filter(x => x.Type == DayPartScheduleType.NormalCleaning);
  }
  getMinorItems(items: DayPartScheduleItemGetDto[]) {
    return items.filter(x => x.Type == DayPartScheduleType.MinorCleaning);
  }
  getItemsString(items: DayPartScheduleItemGetDto[]): string {
    return items.map(x => x.Time).join(',');
  }
  changeActivation(id: number, isChecked: boolean) {
    const item = this.items.find(x => x.Id == id);
    if (item) {
      if (isChecked) {
        this.currentService.active(id).subscribe(op => {
          if(!op.Success) item.IsActive = false;
        })
      }
      else {
        this.currentService.deactive(id).subscribe(op => {
          if(!op.Success) item.IsActive = true;
        })
      }
    }
  }
}
