import { Component, OnInit } from "@angular/core";
import { RequestSwapGetDto, UserGetDto, DayPartScheduleGetDto, ShiftGetDto } from 'src/app/models';

@Component({
    selector: 'app-dashboard-swapper',
    templateUrl: './swapper.component.html',
    styleUrls: ['./swapper.component.scss']
})
export class DashboardSwapperComponent implements OnInit {
    
    shift: ShiftGetDto;
    dayPartSchedule:DayPartScheduleGetDto;
    shakespeare: UserGetDto;
    requests: RequestSwapGetDto[];
    
    userContainers: any[];
    stationContainers: any[];

    ngOnInit(): void {
        
    }
}