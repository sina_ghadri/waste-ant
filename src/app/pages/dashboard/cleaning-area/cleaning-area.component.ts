import { Component, OnInit } from "@angular/core";
import { ShiftGetDto, DayPartScheduleGetDto, UserGetDto, RequestCleaningGetDto } from 'src/app/models';

@Component({
    selector: 'app-dashboard-cleaning-area',
    templateUrl: './cleaning-area.component.html',
    styleUrls: ['./cleaning-area.component.scss']
})
export class DashboardCleaningAreaComponent implements OnInit {

    shift: ShiftGetDto;
    dayPartSchedule:DayPartScheduleGetDto;
    shakespeare: UserGetDto;
    requests: RequestCleaningGetDto[];
    
    ngOnInit(): void {
        
    }
}