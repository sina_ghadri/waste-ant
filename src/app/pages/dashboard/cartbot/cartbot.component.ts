import { Component, OnInit } from "@angular/core";
import { RequestService } from 'src/app/services';
import { NzModalService } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { BaseTableLayoutComponent } from 'src/app/libs/bitspco/components/base/table.layout.component';
import { RequestAddDto } from 'src/app/models';

@Component({
    selector: 'app-dashboard-cartbot',
    templateUrl: './cartbot.component.html',
    styleUrls: ['./cartbot.component.scss']
})
export class DashboardCartbotComponent extends BaseTableLayoutComponent implements OnInit {
    private currentService: RequestService;

    constructor(
        service: RequestService,
        modalService: NzModalService,
        formBuilder: RxFormBuilder,
        private rxfb: RxFormBuilder) {
        super(service, formBuilder);
        this.currentService = service;
        this.modalService = modalService;
    }

    ngOnInit(): void {
        this.load();
    }
    create() {
        this.form = this.rxfb.formGroup(RequestAddDto, new RequestAddDto);
        super.create();
    }
}