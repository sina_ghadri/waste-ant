import { Component, OnInit } from "@angular/core";
import { ShiftGetDto, DayPartScheduleGetDto, UserGetDto, RequestDischargeGetDto } from 'src/app/models';

@Component({
    selector: 'app-dashboard-discharge',
    templateUrl: './discharge.component.html',
    styleUrls: ['./discharge.component.scss']
})
export class DashboardDischargeComponent implements OnInit {

    shift: ShiftGetDto;
    dayPartSchedule:DayPartScheduleGetDto;
    shakespeare: UserGetDto;
    requests: RequestDischargeGetDto[];
    
    ngOnInit(): void {
        
    }
}