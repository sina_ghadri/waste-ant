import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { DashboardGeneralComponent } from './general/general.component';
import { DashboardCalendarComponent } from './calendar/calendar.component';
import { DashboardCartbotComponent } from './cartbot/cartbot.component';
import { DashboardDischargeComponent } from './discharge/discharge.component';
import { DashboardManagementComponent } from './management/management.component';
import { DashboardCleaningAreaComponent } from './cleaning-area/cleaning-area.component';
import { DashboardSwapperComponent } from './swapper/swapper.component';
import { DashboardTransportationComponent } from './transportation/transportation.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '', component: DashboardComponent, children: [
                    { path: '', pathMatch: 'full', redirectTo: 'general' },
                    { path: 'calendar', component: DashboardCalendarComponent },
                    { path: 'cartbot', component: DashboardCartbotComponent },
                    { path: 'cleaning-area', component: DashboardCleaningAreaComponent },
                    { path: 'discharge', component: DashboardDischargeComponent },
                    { path: 'general', component: DashboardGeneralComponent },
                    { path: 'management', component: DashboardManagementComponent },
                    { path: 'swapper', component: DashboardSwapperComponent },
                    { path: 'transportation', component: DashboardTransportationComponent },
                ]
            }
        ])
    ],
    exports: [RouterModule],
    declarations: []
})
export class DashboardRoutingModule { }