import { Component, OnInit } from "@angular/core";
import { RequestElevatorGetDto, UserGetDto, ShiftGetDto } from 'src/app/models';

@Component({
    selector: 'app-dashboard-transportation',
    templateUrl: './transportation.component.html',
    styleUrls: ['./transportation.component.scss']
})
export class DashboardTransportationComponent implements OnInit {
    
    shift: ShiftGetDto;
    shakespeare: UserGetDto;
    requests: RequestElevatorGetDto[];

    ngOnInit(): void {
        
    }
}