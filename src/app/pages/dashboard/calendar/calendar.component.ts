import { Component, OnInit } from "@angular/core";
import { JDate, DayTypeGetDto, CalendarGetDto, Day } from 'src/app/models';
import { CalendarService, DayTypeService } from 'src/app/services';

@Component({
    selector: 'app-dashboard-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss']
})
export class DashboardCalendarComponent implements OnInit {
    selectedDay: Day;

  days: Day[];
  dayType: DayTypeGetDto;
  items: CalendarGetDto[];
  dayTypes: DayTypeGetDto[];

  dayTypeId: number;
  year: number;
  month: number;


  constructor(private service: CalendarService, private dayTypeService: DayTypeService) {
  }

  ngOnInit(): void {
    this.year = JDate.now.getYear();
    this.month = JDate.now.getMonth();

    
    this.dayTypeService.getAll().subscribe(op => {
      this.dayTypes = op.Data;
    });

    this.onchange();
  }
  onchange() {
    this.loadDays();
    this.loadCalendars()
  }
  loadCalendars() {
    if(!this.year || !this.month) return;

    const date = JDate.parseJDate(`${this.year}/${this.month}/01`).setDay(1);
    const firstDay = date.clone();
    const lastDay = date.clone().addMonth(1).addDays(-1);

    const from = JDate.format(firstDay.toDate(), 'YYYY-MM-DD');
    const to = JDate.format(lastDay.toDate(), 'YYYY-MM-DD');

    this.service.getAllByTimeRange(from, to).subscribe(op => {
      this.items = op.Data;
      this.matchCalendars();
    });
  }
  loadDays() {
    if(!this.year || !this.month) return;

    const date = JDate.parseJDate(`${this.year}/${this.month}/01`, 'YYYY/MM/DD').setDay(1);
    const tmpDay = date.clone();
    tmpDay.addDays(-tmpDay.getDayOfWeek());

    this.days = [];
    for (let i = 0; i < 42; i++) {
      const dayNumber = tmpDay.getDay();
      const day = new Day();
      day.text = (dayNumber > 9 ? '' : '0') + dayNumber;
      day.value = JDate.format(tmpDay.toDate(), 'YYYY-MM-DD');

      if (tmpDay.getMonth() == this.month) day.monthDistance = 0;
      else if (tmpDay.getMonth() == 12 && this.month == 1) day.monthDistance = -1;
      else if (tmpDay.getMonth() == 1 && this.month == 12) day.monthDistance = 1;
      else if (tmpDay.getMonth() < this.month) day.monthDistance = -1;
      else day.monthDistance = 1;
      this.days.push(day);
      if (day.monthDistance > 0 && this.days.length % 7 == 0) break;
      tmpDay.addDays(1);
    }
  }
  matchCalendars()
  {
    for (let i = 0; i < this.days.length; i++) {
      const day = this.days[i];
      const calendar = this.items.find(x => x.Date.substr(0, 10) == day.value);
      if (calendar) {
        day.tooltip = calendar.DayType.Name;
        day.color = calendar.DayType.Color;
      }
      else {
        day.tooltip = '';
        day.color = '';
      }
    }
  }

  getDayRows() {
    return [0, 1, 2, 3, 4, 5, 6]
  }
  getDaysByRowIndex(index) {
    return this.days ? this.days.slice(index * 7, (index + 1) * 7) : [];
  }
}