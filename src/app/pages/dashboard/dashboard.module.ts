import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { DashboardRoutingModule } from './dashboard.routing';
import { DashboardComponent } from './dashboard.component';
import { DashboardCalendarComponent } from './calendar/calendar.component';
import { DashboardCartbotComponent } from './cartbot/cartbot.component';
import { DashboardCleaningAreaComponent } from './cleaning-area/cleaning-area.component';
import { DashboardDischargeComponent } from './discharge/discharge.component';
import { DashboardGeneralComponent } from './general/general.component';
import { DashboardManagementComponent } from './management/management.component';
import { DashboardSwapperComponent } from './swapper/swapper.component';
import { DashboardTransportationComponent } from './transportation/transportation.component';
import { LayoutMapModule } from 'src/app/layouts/map/map.module';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        DashboardRoutingModule,

        LayoutMapModule,
        LayoutTableModule,
    ],
    declarations: [
        DashboardComponent,
        DashboardCalendarComponent,
        DashboardCartbotComponent,
        DashboardCleaningAreaComponent,
        DashboardDischargeComponent,
        DashboardGeneralComponent,
        DashboardManagementComponent,
        DashboardSwapperComponent,
        DashboardTransportationComponent
    ]
})
export class DashboardModule { }