import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RequestLeaveRoutingModule } from './request-leave.routing';
import { RequestLeaveIndexComponent } from './index/index.component';
import { RequestLeaveEditComponent } from './edit/edit.component';
import { RequestLeaveDetailComponent } from './detail/detail.component';
import { RequestLeaveCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RequestLeaveRoutingModule,
    ],
    declarations: [
        RequestLeaveIndexComponent,
        RequestLeaveCreateComponent,
        RequestLeaveEditComponent,
        RequestLeaveDetailComponent,
    ]
})
export class RequestLeaveModule {}