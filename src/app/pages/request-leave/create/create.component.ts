import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestLeaveService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RequestLeaveAddDto } from 'src/app/models';

@Component({
    selector: 'app-request-leave-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RequestLeaveCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:RequestLeaveService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}