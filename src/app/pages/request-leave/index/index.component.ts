import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RequestLeaveService } from "../../../services";
import { RequestLeaveGetDto, RequestLeaveAddDto, RequestLeaveChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-request-leave-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RequestLeaveIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: RequestLeaveService;

  constructor(service: RequestLeaveService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
