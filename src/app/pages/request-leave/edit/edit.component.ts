import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestLeaveService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { RequestLeaveChangeDto } from 'src/app/models';

@Component({
    selector: 'app-request-leave-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class RequestLeaveEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:RequestLeaveService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}