import { RouterModule } from '@angular/router';
import { RequestLeaveIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: RequestLeaveIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class RequestLeaveRoutingModule {}