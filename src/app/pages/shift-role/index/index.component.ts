import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ShiftRoleService } from "../../../services";
import { ShiftRoleGetDto, ShiftRoleAddDto, ShiftRoleChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-shift-role-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ShiftRoleIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ShiftRoleService;

  constructor(service: ShiftRoleService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
