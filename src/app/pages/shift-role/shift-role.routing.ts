import { RouterModule } from '@angular/router';
import { ShiftRoleIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ShiftRoleIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ShiftRoleRoutingModule {}