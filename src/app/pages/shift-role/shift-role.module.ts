import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ShiftRoleRoutingModule } from './shift-role.routing';
import { ShiftRoleIndexComponent } from './index/index.component';
import { ShiftRoleEditComponent } from './edit/edit.component';
import { ShiftRoleDetailComponent } from './detail/detail.component';
import { ShiftRoleCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ShiftRoleRoutingModule,
    ],
    declarations: [
        ShiftRoleIndexComponent,
        ShiftRoleCreateComponent,
        ShiftRoleEditComponent,
        ShiftRoleDetailComponent,
    ]
})
export class ShiftRoleModule {}