import { Component, OnInit } from "@angular/core";
import { BaseTreeLayoutComponent } from 'src/app/libs/bitspco/components/base/tree.layout.component';
import { PlaceService } from 'src/app/services';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { PlaceGetDto } from 'src/app/models';
import { NzTreeNode } from 'ng-zorro-antd';

@Component({
    selector: 'app-cleaning-area',
    templateUrl: './cleaning-area.component.html',
    styleUrls: ['./cleaning-area.component.scss']
})
export class CleaningAreaComponent extends BaseTreeLayoutComponent implements OnInit {
    private currentService: PlaceService;
  
    constructor(service: PlaceService, formBuilder: RxFormBuilder) {
      super(service, formBuilder);
      this.currentService = service;
    }
  
    ngOnInit(): void {
      this.load();
    }
}