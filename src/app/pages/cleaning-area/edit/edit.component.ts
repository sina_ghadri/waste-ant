import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { CleaningAreaService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { CleaningAreaChangeDto } from 'src/app/models';

@Component({
    selector: 'app-cleaning-area-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class CleaningAreaEditComponent extends BaseEditComponent implements OnInit {

    constructor(service: CleaningAreaService, formBuiler: RxFormBuilder) {
        super(service, formBuiler);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}