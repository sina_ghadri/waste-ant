import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CleaningAreaComponent } from './cleaning-area.component';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: CleaningAreaComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class CleaningAreaRoutingModule {}