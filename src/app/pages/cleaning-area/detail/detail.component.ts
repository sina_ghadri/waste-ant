import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { CleaningAreaService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { CleaningAreaGetDto } from 'src/app/models';

@Component({
    selector: 'app-cleaning-area-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class CleaningAreaDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:CleaningAreaService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}