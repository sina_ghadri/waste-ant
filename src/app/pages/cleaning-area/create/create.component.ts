import { Component, OnInit, Input } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { CleaningAreaService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { CleaningAreaAddDto } from 'src/app/models';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-cleaning-area-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CleaningAreaCreateComponent extends BaseCreateComponent implements OnInit {
    @Input() placeId: number;

    constructor(private currentService:CleaningAreaService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    makeFormGroup(obj: any): FormGroup {
        obj = new CleaningAreaAddDto;
        obj.PlaceId = this.placeId;
        return super.makeFormGroup(obj);
    }
}