import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { CleaningAreaRoutingModule } from './cleaning-area.routing';
import { CleaningAreaIndexComponent } from './index/index.component';
import { CleaningAreaEditComponent } from './edit/edit.component';
import { CleaningAreaDetailComponent } from './detail/detail.component';
import { CleaningAreaCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';
import { LayoutTreeModule } from 'src/app/layouts/tree/tree.module';
import { CleaningAreaComponent } from './cleaning-area.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        CleaningAreaRoutingModule,

        LayoutTableModule,
        LayoutTreeModule,
    ],
    declarations: [
        CleaningAreaComponent,
        CleaningAreaIndexComponent,
        CleaningAreaCreateComponent,
        CleaningAreaEditComponent,
        CleaningAreaDetailComponent,
    ],
    exports: [
        CleaningAreaIndexComponent,
        CleaningAreaCreateComponent,
        CleaningAreaEditComponent,
        CleaningAreaDetailComponent,
    ]
})
export class CleaningAreaModule {}