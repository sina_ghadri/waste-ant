import { Component, OnInit } from "@angular/core";
import { IndexService } from "../../../services";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { BaseTreeLayoutComponent } from '../../../libs/bitspco/components/base/tree.layout.component';
import { IndexGetDto } from 'src/app/models';
import { NzTreeNode } from 'ng-zorro-antd';

@Component({
  selector: "app-index-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class IndexIndexComponent extends BaseTreeLayoutComponent implements OnInit {
  private currentService: IndexService;

  constructor(service: IndexService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }
  createTreeNode(item: IndexGetDto): NzTreeNode {
    return new NzTreeNode({
      key: item.Id.toString(),
      title: item.Name,
    });
  }

}
