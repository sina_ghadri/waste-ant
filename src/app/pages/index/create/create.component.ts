import { Component, OnInit, Input } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { IndexService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { IndexAddDto } from 'src/app/models';

@Component({
    selector: 'app-index-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class IndexCreateComponent extends BaseCreateComponent implements OnInit {
    @Input() parentId: number;

    constructor(private currentService:IndexService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    makeFormGroup() {
        return super.makeFormGroup({ ParentId: this.parentId });
    }
}