import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { IndexRoutingModule } from './index.routing';
import { IndexIndexComponent } from './index/index.component';
import { IndexEditComponent } from './edit/edit.component';
import { IndexDetailComponent } from './detail/detail.component';
import { IndexCreateComponent } from './create/create.component';
import { LayoutTreeModule } from 'src/app/layouts/tree/tree.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        IndexRoutingModule,
        LayoutTreeModule,
    ],
    declarations: [
        IndexIndexComponent,
        IndexCreateComponent,
        IndexEditComponent,
        IndexDetailComponent,
    ]
})
export class IndexModule {}