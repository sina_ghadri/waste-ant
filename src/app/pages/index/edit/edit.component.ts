import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { IndexService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { IndexChangeDto, IndexGetDto } from 'src/app/models';

@Component({
    selector: 'app-index-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class IndexEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:IndexService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    edit(item: IndexGetDto) {
        this.form = this.makeFormGroup(item);
    }
}