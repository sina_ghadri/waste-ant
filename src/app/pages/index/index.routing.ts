import { RouterModule } from '@angular/router';
import { IndexIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: IndexIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class IndexRoutingModule {}