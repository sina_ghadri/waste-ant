import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { IndexService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { IndexGetDto } from 'src/app/models';

@Component({
    selector: 'app-index-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class IndexDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:IndexService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}