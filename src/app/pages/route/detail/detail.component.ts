import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RouteService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { RouteGetDto } from 'src/app/models';

@Component({
    selector: 'app-route-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class RouteDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:RouteService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}