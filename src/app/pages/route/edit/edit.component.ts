import { Component, OnInit, Input } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RouteService, ElevatorService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ElevatorGetDto, PlaceGetDto } from 'src/app/models';

@Component({
    selector: 'app-route-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class RouteEditComponent extends BaseEditComponent implements OnInit {
    @Input() place: PlaceGetDto;
    elevators: ElevatorGetDto[];

    constructor(
        private currentService:RouteService,
        private elevatorService: ElevatorService,
        formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    getElevators(): ElevatorGetDto[] {
        return this.elevators ? this.elevators.filter(x =>
            (x.PlaceId == this.place.Id) ||
            (this.place.ParentId && x.PlaceId == this.place.ParentId) ||
            (this.place.Parent && this.place.Parent.ParentId && x.PlaceId == this.place.Parent.ParentId)
        ) : [];
    }
    ngOnInit(): void {
        super.ngOnInit();
        this.elevatorService.getAll().subscribe(op => this.elevators = op.Data);
    }

}