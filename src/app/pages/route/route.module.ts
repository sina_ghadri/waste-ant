import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RouteRoutingModule } from './route.routing';
import { RouteIndexComponent } from './index/index.component';
import { RouteEditComponent } from './edit/edit.component';
import { RouteDetailComponent } from './detail/detail.component';
import { RouteCreateComponent } from './create/create.component';
import { LayoutTreeModule } from 'src/app/layouts/tree/tree.module';
import { RouteComponent } from './route.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RouteRoutingModule,

        LayoutTableModule,
        LayoutTreeModule,
    ],
    declarations: [
        RouteComponent,
        RouteIndexComponent,
        RouteCreateComponent,
        RouteEditComponent,
        RouteDetailComponent,
    ],
    exports: [
        RouteIndexComponent,
        RouteCreateComponent,
        RouteEditComponent,
        RouteDetailComponent,
    ]
})
export class RouteModule {}