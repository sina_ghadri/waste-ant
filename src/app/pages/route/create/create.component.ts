import { Component, OnInit, Input, OnChanges, SimpleChanges } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RouteService, ElevatorService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RouteAddDto, ElevatorGetDto, PlaceGetDto } from 'src/app/models';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-route-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RouteCreateComponent extends BaseCreateComponent implements OnInit {
    @Input() placeId: number;
    @Input() place: PlaceGetDto;
    elevators: ElevatorGetDto[];

    constructor(
        private currentService: RouteService,
        private elevatorService: ElevatorService,
        formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.elevatorService.getAll().subscribe(op => this.elevators = op.Data);
    }
    getElevators(): ElevatorGetDto[] {
        return this.elevators ? this.elevators.filter(x =>
            (x.PlaceId == this.place.Id) ||
            (this.place.ParentId && x.PlaceId == this.place.ParentId) ||
            (this.place.Parent && this.place.Parent.ParentId && x.PlaceId == this.place.Parent.ParentId)
        ) : [];
    }
    makeFormGroup(obj: any): FormGroup {
        obj = new RouteAddDto;
        obj.PlaceId = this.placeId;
        return super.makeFormGroup(obj);
    }
}