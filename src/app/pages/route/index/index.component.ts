import { Component, OnInit, TemplateRef, ViewChild, Input, SimpleChanges, OnChanges } from "@angular/core";
import { RouteService } from "../../../services";
import { RouteGetDto, RouteAddDto, RouteChangeDto, ODataQueryOptions } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef, NzTreeNode } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-route-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RouteIndexComponent extends BaseTableLayoutComponent implements OnInit, OnChanges {
  @Input() placeId: number;
    @Input() place: NzTreeNode;
    private currentService: RouteService;

  constructor(service: RouteService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    super.ngOnInit();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.placeId) {
      this.load();
      this.pageIndex = 1;
    }
  }
  load(query?: ODataQueryOptions) {
    query = new ODataQueryOptions;
    if (this.placeId) query.filter = `PlaceId eq ${this.placeId}`;
    super.load(query);
  }
  create() {
    this.selectedItem = new RouteAddDto;
    this.selectedItem.PlaceId = this.placeId;
    super.create();
  }
}
