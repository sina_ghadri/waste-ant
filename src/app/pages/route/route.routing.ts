import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { RouteComponent } from './route.component';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: RouteComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class RouteRoutingModule {}