import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptSwapService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ReceiptSwapChangeDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-swap-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ReceiptSwapEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:ReceiptSwapService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}