import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ReceiptSwapService } from "../../../services";
import { ReceiptSwapGetDto, ReceiptSwapAddDto, ReceiptSwapChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-receipt-swap-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ReceiptSwapIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ReceiptSwapService;

  constructor(service: ReceiptSwapService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
