import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ReceiptSwapRoutingModule } from './receipt-swap.routing';
import { ReceiptSwapIndexComponent } from './index/index.component';
import { ReceiptSwapEditComponent } from './edit/edit.component';
import { ReceiptSwapDetailComponent } from './detail/detail.component';
import { ReceiptSwapCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ReceiptSwapRoutingModule,
    ],
    declarations: [
        ReceiptSwapIndexComponent,
        ReceiptSwapCreateComponent,
        ReceiptSwapEditComponent,
        ReceiptSwapDetailComponent,
    ]
})
export class ReceiptSwapModule {}