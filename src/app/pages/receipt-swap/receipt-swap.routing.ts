import { RouterModule } from '@angular/router';
import { ReceiptSwapIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ReceiptSwapIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ReceiptSwapRoutingModule {}