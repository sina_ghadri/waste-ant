import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptSwapService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ReceiptSwapAddDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-swap-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ReceiptSwapCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:ReceiptSwapService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}