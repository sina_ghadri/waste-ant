import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptSwapService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { ReceiptSwapGetDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-swap-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class ReceiptSwapDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:ReceiptSwapService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}