import { Component, OnInit } from "@angular/core";
import { PlaceService } from "../../../services";
import { NzModalService, NzTreeNode, NzFormatEmitEvent } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { BaseTreeLayoutComponent } from 'src/app/libs/bitspco/components/base/tree.layout.component';
import { PlaceGetDto } from 'src/app/models';

@Component({
  selector: "app-place-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class PlaceIndexComponent extends BaseTreeLayoutComponent implements OnInit {
  private currentService: PlaceService;

  constructor(service: PlaceService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }
  createTreeNode(item: PlaceGetDto): NzTreeNode {
    return new NzTreeNode({
      key: item.Id.toString(),
      title: item.Name,
    });
  }
}
