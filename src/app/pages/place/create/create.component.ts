import { Component, OnInit, Input } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { PlaceService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { KeyValue } from '@angular/common';
import { Uploader } from 'src/app/libs/drive/components/uploader/models/uploader';
import { UploaderService } from 'src/app/libs/drive/components/uploader/services/uploader.service';

@Component({
    selector: 'app-place-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class PlaceCreateComponent extends BaseCreateComponent implements OnInit {
    @Input() parentId: number;

    uploader: Uploader = new Uploader;
    chooseFlag: boolean = false;
    types: KeyValue<number, string>[];

    constructor(private currentService:PlaceService, formBuilder: RxFormBuilder, private uploaderService: UploaderService) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.uploader.service = this.uploaderService;
        this.currentService.getTypes().subscribe(op => this.types = op.Data);
    }

    makeFormGroup() {
        return super.makeFormGroup({ ParentId: this.parentId });
    }
    
    choose() {
        this.chooseFlag = true;
    }
    chooseComplete(e) {
        if (e) {
            this.form.get('Image').setValue(e);
        }
        this.chooseFlag = false;
    }
}