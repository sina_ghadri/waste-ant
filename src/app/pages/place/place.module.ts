import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { PlaceRoutingModule } from './place.routing';
import { PlaceIndexComponent } from './index/index.component';
import { PlaceEditComponent } from './edit/edit.component';
import { PlaceDetailComponent } from './detail/detail.component';
import { PlaceCreateComponent } from './create/create.component';
import { LayoutTreeModule } from 'src/app/layouts/tree/tree.module';
import { UploaderModule } from 'src/app/libs/drive/components/uploader/uploader.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        PlaceRoutingModule,
        LayoutTreeModule,
        UploaderModule
    ],
    declarations: [
        PlaceIndexComponent,
        PlaceCreateComponent,
        PlaceEditComponent,
        PlaceDetailComponent,
    ],
    exports: [
        PlaceIndexComponent,
        PlaceCreateComponent,
        PlaceEditComponent,
        PlaceDetailComponent,
    ]
})
export class PlaceModule {}