import { Component, OnInit, Input } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { PlaceService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { PlaceChangeDto } from 'src/app/models';
import { Uploader } from 'src/app/libs/drive/components/uploader/models/uploader';
import { KeyValue } from '@angular/common';
import { UploaderService } from 'src/app/libs/drive/components/uploader/services/uploader.service';

@Component({
    selector: 'app-place-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class PlaceEditComponent extends BaseEditComponent implements OnInit {
    @Input() parentId: number;

    uploader: Uploader = new Uploader;
    chooseFlag: boolean = false;
    types: KeyValue<number, string>[];

    constructor(private currentService: PlaceService, formBuilder: RxFormBuilder, private uploaderService: UploaderService) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.uploader.service = this.uploaderService;
        this.currentService.getTypes().subscribe(op => this.types = op.Data);
    }

    choose() {
        this.chooseFlag = true;
    }
    chooseComplete(e) {
        if (e) {
            this.form.get('Image').setValue(e);
        }
        this.chooseFlag = false;
    }
}