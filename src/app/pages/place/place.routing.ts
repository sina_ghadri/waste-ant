import { RouterModule } from '@angular/router';
import { PlaceIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: PlaceIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class PlaceRoutingModule {}