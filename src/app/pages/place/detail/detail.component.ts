import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { PlaceService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { PlaceGetDto } from 'src/app/models';
import { KeyValue } from '@angular/common';

@Component({
    selector: 'app-place-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class PlaceDetailComponent extends BaseDetailComponent implements OnInit {

    types: KeyValue<number, string>[];

    constructor(private currentService: PlaceService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.currentService.getTypes().subscribe(op => this.types = op.Data);
    }

}