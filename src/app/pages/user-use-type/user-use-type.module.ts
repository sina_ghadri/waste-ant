import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { UserUseTypeRoutingModule } from './user-use-type.routing';
import { UserUseTypeIndexComponent } from './index/index.component';
import { UserUseTypeEditComponent } from './edit/edit.component';
import { UserUseTypeDetailComponent } from './detail/detail.component';
import { UserUseTypeCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        UserUseTypeRoutingModule,
    ],
    declarations: [
        UserUseTypeIndexComponent,
        UserUseTypeCreateComponent,
        UserUseTypeEditComponent,
        UserUseTypeDetailComponent,
    ],
    exports: [
        UserUseTypeIndexComponent,
        UserUseTypeCreateComponent,
        UserUseTypeEditComponent,
        UserUseTypeDetailComponent,
    ]
})
export class UserUseTypeModule {}