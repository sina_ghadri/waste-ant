import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { UserUseTypeService } from "../../../services";
import { UserUseTypeGetDto, UserUseTypeAddDto, UserUseTypeChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-user-use-type-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class UserUseTypeIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: UserUseTypeService;

  constructor(service: UserUseTypeService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
