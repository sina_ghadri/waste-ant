import { RouterModule } from '@angular/router';
import { UserUseTypeIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: UserUseTypeIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class UserUseTypeRoutingModule {}