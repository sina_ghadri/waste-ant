import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UserUseTypeService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { UserUseTypeChangeDto } from 'src/app/models';

@Component({
    selector: 'app-user-use-type-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class UserUseTypeEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:UserUseTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}