import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UserUseTypeService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { UserUseTypeGetDto } from 'src/app/models';

@Component({
    selector: 'app-user-use-type-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class UserUseTypeDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:UserUseTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}