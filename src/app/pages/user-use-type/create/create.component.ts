import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UserUseTypeService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { UserUseTypeAddDto } from 'src/app/models';

@Component({
    selector: 'app-user-use-type-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class UserUseTypeCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:UserUseTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}