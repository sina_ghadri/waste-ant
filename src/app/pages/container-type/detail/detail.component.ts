import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ContainerTypeService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { ContainerTypeGetDto } from 'src/app/models';

@Component({
    selector: 'app-container-type-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class ContainerTypeDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:ContainerTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
}