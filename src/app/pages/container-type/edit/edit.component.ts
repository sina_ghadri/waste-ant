import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ContainerTypeService, WasteTypeService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ContainerTypeChangeDto, WasteTypeGetDto, Color } from 'src/app/models';

@Component({
    selector: 'app-container-type-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ContainerTypeEditComponent extends BaseEditComponent implements OnInit {

    colors: Color[]
    wasteTypes: WasteTypeGetDto[];

    constructor(private currentService:ContainerTypeService, private wasteTypeService: WasteTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.currentService.getColors().subscribe(op => this.colors = op.Data);
        this.wasteTypeService.getAll().subscribe(op => this.wasteTypes = op.Data);
    }
}