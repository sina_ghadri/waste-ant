import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ContainerTypeService, WasteTypeService } from "../../../services";
import { ContainerTypeGetDto, ContainerTypeAddDto, ContainerTypeChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-container-type-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ContainerTypeIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ContainerTypeService;

  constructor(
    service: ContainerTypeService, 
    modalService: NzModalService, 
    formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }
}
