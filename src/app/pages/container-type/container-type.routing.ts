import { RouterModule } from '@angular/router';
import { ContainerTypeIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ContainerTypeIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ContainerTypeRoutingModule {}