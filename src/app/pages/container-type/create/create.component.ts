import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ContainerTypeService, WasteTypeService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ContainerTypeAddDto, WasteTypeGetDto, Color } from 'src/app/models';

@Component({
    selector: 'app-container-type-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ContainerTypeCreateComponent extends BaseCreateComponent implements OnInit {

    colors: Color[]
    wasteTypes: WasteTypeGetDto[];

    constructor(private currentService:ContainerTypeService, private wasteTypeService: WasteTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.currentService.getColors().subscribe(op => this.colors = op.Data);
        this.wasteTypeService.getAll().subscribe(op => this.wasteTypes = op.Data);
    }
}