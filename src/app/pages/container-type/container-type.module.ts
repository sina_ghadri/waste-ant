import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ContainerTypeRoutingModule } from './container-type.routing';
import { ContainerTypeIndexComponent } from './index/index.component';
import { ContainerTypeEditComponent } from './edit/edit.component';
import { ContainerTypeDetailComponent } from './detail/detail.component';
import { ContainerTypeCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ContainerTypeRoutingModule,
        LayoutTableModule,
    ],
    declarations: [
        ContainerTypeIndexComponent,
        ContainerTypeCreateComponent,
        ContainerTypeEditComponent,
        ContainerTypeDetailComponent,
    ]
})
export class ContainerTypeModule {}