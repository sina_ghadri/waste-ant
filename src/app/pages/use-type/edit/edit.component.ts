import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UseTypeService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { UseTypeChangeDto, UseTypeGetDto } from 'src/app/models';

@Component({
    selector: 'app-use-type-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class UseTypeEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService: UseTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    edit(item: UseTypeGetDto) {
        this.form = this.makeFormGroup(item);
    }
}