import { Component, OnInit, Input } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UseTypeService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { UseTypeAddDto, UseTypeGetDto } from 'src/app/models';

@Component({
    selector: 'app-use-type-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class UseTypeCreateComponent extends BaseCreateComponent implements OnInit {
    @Input() parentId: number;

    constructor(private currentService: UseTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    makeFormGroup() {
        return super.makeFormGroup({ ParentId: this.parentId });
    }
}