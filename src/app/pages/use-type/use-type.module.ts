import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { UseTypeRoutingModule } from './use-type.routing';
import { UseTypeIndexComponent } from './index/index.component';
import { UseTypeEditComponent } from './edit/edit.component';
import { UseTypeDetailComponent } from './detail/detail.component';
import { UseTypeCreateComponent } from './create/create.component';
import { LayoutTreeModule } from 'src/app/layouts/tree/tree.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        UseTypeRoutingModule,
        LayoutTreeModule,
    ],
    declarations: [
        UseTypeIndexComponent,
        UseTypeCreateComponent,
        UseTypeEditComponent,
        UseTypeDetailComponent,
    ]
})
export class UseTypeModule {}