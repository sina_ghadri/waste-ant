import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UseTypeService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { UseTypeGetDto } from 'src/app/models';

@Component({
    selector: 'app-use-type-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class UseTypeDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:UseTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}