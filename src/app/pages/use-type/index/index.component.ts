import { Component, OnInit } from "@angular/core";
import { UseTypeService } from "../../../services";
import { UseTypeGetDto, UseTypeAddDto, UseTypeChangeDto } from "../../../models";
import { NzModalService, NzTreeNodeOptions, NzTreeNode } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { BaseTreeLayoutComponent } from '../../../libs/bitspco/components/base/tree.layout.component';

@Component({
  selector: "app-use-type-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class UseTypeIndexComponent extends BaseTreeLayoutComponent implements OnInit {

  constructor(private currentService: UseTypeService, formBuilder: RxFormBuilder) {
    super(currentService, formBuilder);
  }

  ngOnInit(): void {
    this.load();
  }
  createTreeNode(item: UseTypeGetDto): NzTreeNode {
    return new NzTreeNode({
      key: item.Id.toString(),
      title: item.Name,
    });
  }
}
