import { RouterModule } from '@angular/router';
import { UseTypeIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: UseTypeIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class UseTypeRoutingModule {}