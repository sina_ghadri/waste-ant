import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AccountComponent } from './account.component';
import { AccountProfileComponent } from './profile/profile.component';
import { AccountSettingComponent } from './setting/setting.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '', component: AccountComponent, children: [
                    { path: '', pathMatch: 'full', redirectTo: 'profile' },
                    { path: 'profile', component: AccountProfileComponent },
                    { path: 'setting', component: AccountSettingComponent },
                ]
            }
        ])
    ],
    exports: [RouterModule],
    declarations: []
})
export class AccountRoutingModule { }