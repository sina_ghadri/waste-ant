import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UploaderService } from 'src/app/libs/drive/components/uploader/services/uploader.service';
import { BaseFormComponent } from 'src/app/libs/bitspco/components/base/form.component';
import { TokenService } from 'src/app/services';

@Component({
    selector: 'app-setting',
    templateUrl: 'setting.component.html',
    styleUrls: ['./setting.component.scss']
})
export class AccountSettingComponent extends BaseFormComponent implements OnInit {

    constructor(formBuilder: RxFormBuilder) {
        super(formBuilder);
    }

    ngOnInit(): void {
        this.form = this.makeFormGroup({});
    }
}