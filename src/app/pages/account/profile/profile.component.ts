import { Component, OnInit } from "@angular/core";
import { FormGroup } from '@angular/forms';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UserProfile, TokenGetDto, UserChangeProfileDto, JDate } from 'src/app/models';
import { Uploader } from 'src/app/libs/drive/components/uploader/models/uploader';
import { UploaderService } from 'src/app/libs/drive/components/uploader/services/uploader.service';
import { BaseFormComponent } from 'src/app/libs/bitspco/components/base/form.component';
import { TokenService, AuthService, UserService } from 'src/app/services';

@Component({
    selector: 'app-profile',
    templateUrl: 'profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class AccountProfileComponent extends BaseFormComponent implements OnInit {
    progress: boolean;

    form: FormGroup;
    uploader: Uploader = new Uploader;
    uploadName: string = '';

    tokens: TokenGetDto[];

    constructor(
        private userService: UserService,
        private authService: AuthService,
        private formBuilder: RxFormBuilder,
        private tokenService: TokenService,
        private uploderService: UploaderService) {
        super(formBuilder);
    }

    ngOnInit(): void {
        this.form = this.makeFormGroup(new UserProfile);

        this.authService.getLoginInfo().subscribe(loginInfo => {
            const user = loginInfo.User;
            this.form.get('StartTime').setValue(JDate.toJDate(JDate.parseDate(user.CreationTime, 'YYYY/MM/DD hh:mm:ss')).format('YYYY/MM/DD'));
            if (user.ExpireTime) this.form.get('EndTime').setValue(JDate.toJDate(JDate.parseDate(user.ExpireTime, 'YYYY/MM/DD hh:mm:ss')).format('YYYY/MM/DD'));
            const more = user.More ? JSON.parse(user.More) : {};
            for (const key in more) {
                if (more.hasOwnProperty(key)) {
                    const value = more[key];
                    this.form.get(key).setValue(value);
                }
            }
        });

        this.uploader.service = this.uploderService;
        this.tokenService.getAllLast10().subscribe(op => this.tokens = op.Data);
    }
    chooseFile(name) {
        this.uploadName = name;
    }
    chooseFileComplete(e) {
        if (e) {
            if (this.uploadName == 'thumbnail') this.form.get('Thumbnail').setValue(e);
            else if (this.uploadName == 'brand') this.form.get('BrandLogo').setValue(e);
        }
        this.uploadName = '';
    }
    submit() {
        const obj = new UserChangeProfileDto;
        obj.Name = this.f.Firstname + ' ' + this.f.Lastname;
        obj.Thumbnail = this.f.Thumbnail;
        obj.More = JSON.stringify(this.form.value);

        this.progress = true;

        for (const i in this.form.controls) {
            this.form.controls[i].markAsDirty();
            this.form.controls[i].updateValueAndValidity();
        }
        if (this.form.valid) {
            this.userService.changeProfile(obj).subscribe(op => {
                if (op.Success) {
                    this.authService.getLoginInfo(true).subscribe(op => {
                        this.progress = false;
                    }, e => this.progress = false);
                }
            }, e => {
                this.progress = false;
            })
        }
    }
}