import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { AccountRoutingModule } from './account.routing';
import { UploaderModule } from 'src/app/libs/drive/components/uploader/uploader.module';
import { AccountComponent } from './account.component';
import { AccountProfileComponent } from './profile/profile.component';
import { AccountSettingComponent } from './setting/setting.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        AccountRoutingModule,

        UploaderModule
    ],
    declarations: [
        AccountComponent,
        AccountProfileComponent,
        AccountSettingComponent
    ]
})
export class AccountModule {}