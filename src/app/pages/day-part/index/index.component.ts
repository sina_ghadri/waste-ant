import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { DayPartService } from "../../../services";
import { DayPartGetDto, DayPartAddDto, DayPartChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-day-part-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class DayPartIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: DayPartService;

  constructor(service: DayPartService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
