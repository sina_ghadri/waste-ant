import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { DayPartAddDto } from 'src/app/models';

@Component({
    selector: 'app-day-part-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class DayPartCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:DayPartService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}