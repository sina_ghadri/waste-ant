import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { DayPartChangeDto } from 'src/app/models';

@Component({
    selector: 'app-day-part-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class DayPartEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:DayPartService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}