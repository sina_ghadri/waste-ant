import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { DayPartRoutingModule } from './day-part.routing';
import { DayPartIndexComponent } from './index/index.component';
import { DayPartEditComponent } from './edit/edit.component';
import { DayPartDetailComponent } from './detail/detail.component';
import { DayPartCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        DayPartRoutingModule,

        LayoutTableModule,
    ],
    declarations: [
        DayPartIndexComponent,
        DayPartCreateComponent,
        DayPartEditComponent,
        DayPartDetailComponent,
    ]
})
export class DayPartModule {}