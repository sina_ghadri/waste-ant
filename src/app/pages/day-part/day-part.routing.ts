import { RouterModule } from '@angular/router';
import { DayPartIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: DayPartIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class DayPartRoutingModule {}