import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RequestSwapService } from "../../../services";
import { RequestSwapGetDto, RequestSwapAddDto, RequestSwapChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-request-swap-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RequestSwapIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: RequestSwapService;

  constructor(service: RequestSwapService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
