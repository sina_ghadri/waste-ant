import { RouterModule } from '@angular/router';
import { RequestSwapIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: RequestSwapIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class RequestSwapRoutingModule {}