import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestSwapService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RequestSwapAddDto } from 'src/app/models';

@Component({
    selector: 'app-request-swap-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RequestSwapCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:RequestSwapService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}