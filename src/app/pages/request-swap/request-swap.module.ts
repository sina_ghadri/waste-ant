import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RequestSwapRoutingModule } from './request-swap.routing';
import { RequestSwapIndexComponent } from './index/index.component';
import { RequestSwapEditComponent } from './edit/edit.component';
import { RequestSwapDetailComponent } from './detail/detail.component';
import { RequestSwapCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RequestSwapRoutingModule,
    ],
    declarations: [
        RequestSwapIndexComponent,
        RequestSwapCreateComponent,
        RequestSwapEditComponent,
        RequestSwapDetailComponent,
    ]
})
export class RequestSwapModule {}