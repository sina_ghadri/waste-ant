export enum Tools {
    Select = 0,
    DrawPlace = 1,
    DrawStation = 2,
    DrawElement = 3,
    DrawRoute = 4,
    DrawCleaningArea = 5,
    Eraser = 6,
    Move = 7,
}
export enum MouseSate {
    Up = 0,
    Down = 1,
}
