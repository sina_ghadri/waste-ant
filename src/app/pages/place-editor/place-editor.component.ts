import { Component, OnInit, EventEmitter, ElementRef, HostBinding, ViewChild, TemplateRef, SimpleChanges, OnChanges } from "@angular/core";
import { PlaceGetDto, PlaceAddDto, StationGetDto, ElementGetDto, RouteGetDto, CleaningAreaGetDto, ElevatorGetDto, PlaceElementGetDto, StationAddDto, ElementAddDto, PlaceElementAddDto, CleaningAreaAddDto, RouteAddDto, Cordination, RouteItemGetDto, RouteItemAddDto, Map, MapColors } from 'src/app/models';
import { PlaceService, StationService, PlaceElementService, ElementService, RouteService, CleaningAreaService } from 'src/app/services';
import { Point, Rectangle, IMapItem, Place, Station, Element, CleaningArea, Route, RouteItem } from '../../models';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { MouseSate, Tools } from './models';

@Component({
    selector: 'app-place-editor',
    templateUrl: './place-editor.component.html',
    styleUrls: ['./place-editor.component.scss']
})
export class PlaceEditorComponent implements OnInit, OnChanges {
    @HostBinding('class.fullscreen') isFullscreen: boolean = false;

    allPlaces: PlaceGetDto[];

    places: PlaceGetDto[] = [];
    stations: StationGetDto[] = [];
    elements: ElementGetDto[] = [];
    placeElements: PlaceElementGetDto[] = [];
    routes: RouteGetDto[] = [];
    routeItems: RouteItemGetDto[] = [];
    cleaningAreas: CleaningAreaGetDto[] = [];
    elevators: ElevatorGetDto[] = [];

    isLoading: boolean = false;


    items: IMapItem[];
    selectedItem: IMapItem;

    place: PlaceGetDto;

    get Place(): PlaceGetDto { return this.place; }
    set Place(value: PlaceGetDto) { this.reset(); this.place = value; this.loadItems(); }

    selectedPlaces: PlaceGetDto[] = [];

    selectedPlace: PlaceGetDto;
    selectedStation: StationGetDto;
    selectedElement: ElementGetDto;
    selectedRoute: RouteGetDto;
    selectedRouteChange = () => { this.items = this.getItems(); }
    selectedCleaningArea: CleaningAreaGetDto;

    scale: number = 1;
    scales: number[] = [0.25, 0.50, 0.75, 1, 1.25, 1.50, 1.75, 2, 2.5, 3, 4];

    startPoint: Point;
    movePoint: Point;
    endPoint: Point;
    mouseState: MouseSate = MouseSate.Up;

    selectedTools: Tools = Tools.Select;
    selectedArea: Rectangle;

    onClick: EventEmitter<any> = new EventEmitter;
    onDblClick: EventEmitter<any> = new EventEmitter;
    onMouseDown: EventEmitter<any> = new EventEmitter;
    onMouseUp: EventEmitter<any> = new EventEmitter;
    onMouseMove: EventEmitter<any> = new EventEmitter;
    onScroll: EventEmitter<any> = new EventEmitter;

    @ViewChild('newPlaceTpl', { static: false })
    newPlaceTpl: TemplateRef<any>;
    newPlaceObj: PlaceAddDto = new PlaceAddDto;
    newPlaceModal: NzModalRef;

    @ViewChild('newStationTpl', { static: false })
    newStationTpl: TemplateRef<any>;
    newStationObj: StationAddDto;
    newStationModal: NzModalRef;

    @ViewChild('newElementTpl', { static: false })
    newElementTpl: TemplateRef<any>;
    newElementObj: ElementAddDto;
    newElementModal: NzModalRef;

    @ViewChild('newRouteTpl', { static: false })
    newRouteTpl: TemplateRef<any>;
    newRouteObj: RouteAddDto;
    newRouteModal: NzModalRef;

    @ViewChild('newCleaningAreaTpl', { static: false })
    newCleaningAreaTpl: TemplateRef<any>;
    newCleaningAreaObj: CleaningAreaAddDto;
    newCleaningAreaModal: NzModalRef;

    get svg(): HTMLElement { return this.element.nativeElement.querySelector('.items-layer>svg'); }
    get svgWidth(): number { return this.svg ? this.svg.clientWidth : 0; }
    get svgHeight(): number { return this.svg ? this.svg.clientHeight : 0; }

    get image(): HTMLImageElement { return this.element.nativeElement.querySelector('.items-layer>img'); }
    get imageWidth(): number { return this.image ? this.image.naturalWidth : 0; }
    get imageHeight(): number { return this.image ? this.image.naturalHeight : 0; }

    get LastSelectedPlace(): PlaceGetDto { return (this.selectedPlaces.length > 0 ? this.selectedPlaces[this.selectedPlaces.length - 1] : null); }

    get Places(): PlaceGetDto[] { return this.places ? this.places : []; }
    get PlacesCordinated(): PlaceGetDto[] { return this.Places.filter(x => x.Cordination); }
    get PlacesNotCordinate(): PlaceGetDto[] { return this.Places.filter(x => !x.Cordination); }
    set Places(value: PlaceGetDto[]) { this.places = value; this.items = this.getItems(); }

    get Stations(): StationGetDto[] { return this.stations ? this.stations : []; }
    get StationsCordinated(): StationGetDto[] { return this.Stations.filter(x => x.Cordination); }
    get StationsNotCordinate(): StationGetDto[] { return this.Stations.filter(x => !x.Cordination); }
    set Stations(value: StationGetDto[]) { this.stations = value; this.items = this.getItems(); }

    get Elements(): ElementGetDto[] { return this.elements ? this.elements : []; }
    get ElementsCordinated(): PlaceElementGetDto[] { return this.PlaceElementsCordinated.filter(x => x.Cordination); }
    set Elements(value: ElementGetDto[]) { this.elements = value; this.items = this.getItems(); }

    get PlaceElements(): PlaceElementGetDto[] { return this.placeElements ? this.placeElements : []; }
    get PlaceElementsCordinated(): PlaceElementGetDto[] { return this.PlaceElements.filter(x => x.Cordination); }
    set PlaceElements(value: PlaceElementGetDto[]) { this.placeElements = value; this.items = this.getItems(); }

    get CleaningAreas(): CleaningAreaGetDto[] { return this.cleaningAreas ? this.cleaningAreas : []; }
    get CleaningAreasCordinated(): CleaningAreaGetDto[] { return this.CleaningAreas.filter(x => x.Cordination); }
    get CleaningAreasNotCordinate(): CleaningAreaGetDto[] { return this.CleaningAreas.filter(x => !x.Cordination); }
    set CleaningAreas(value: CleaningAreaGetDto[]) { this.cleaningAreas = value; this.items = this.getItems(); }

    get Elevators(): ElevatorGetDto[] { return this.elevators ? this.elevators : []; }
    set Elevators(value: ElevatorGetDto[]) { this.elevators = value; this.items = this.getItems(); }

    get Routes(): RouteGetDto[] { return this.routes ? this.routes : []; }
    set Routes(value: RouteGetDto[]) { this.routes = value; this.items = this.getItems(); }

    get RouteItems(): RouteItemGetDto[] { return this.routeItems ? this.routeItems : []; }
    set RouteItems(value: RouteItemGetDto[]) { this.routeItems = value; this.items = this.getItems(); }


    constructor(
        private element: ElementRef,
        private placeService: PlaceService,
        private stationService: StationService,
        private elementService: ElementService,
        private routeService: RouteService,
        private cleaningAreaService: CleaningAreaService,
        private placeElementService: PlaceElementService,
        private modalService: NzModalService,
        private hotkeysService: HotkeysService
    ) {

    }
    ngOnInit(): void {

        this.placeService.getAll().subscribe(op => {
            this.allPlaces = op.Data;
            this.selectPlace();
        });
        this.elementService.getAll().subscribe(op => this.Elements = op.Data);

        this.onClick.subscribe(e => { if (this.selectedTools == Tools.Eraser) this.erase(); });
        this.onClick.subscribe(e => { if (this.selectedTools == Tools.DrawPlace) this.drawingPlace(); });

        this.onDblClick.subscribe(e => { if (this.selectedTools == Tools.Select) this.selectReset(); });
        this.onDblClick.subscribe(e => { if (this.selectedTools == Tools.DrawPlace) this.drawPlaceReset(); });
        this.onDblClick.subscribe(e => { if (this.selectedTools == Tools.DrawStation) this.drawStationReset(); });
        this.onDblClick.subscribe(e => { if (this.selectedTools == Tools.DrawElement) this.drawElementReset(); });
        this.onDblClick.subscribe(e => { if (this.selectedTools == Tools.DrawRoute) this.drawRouteReset(); });
        this.onDblClick.subscribe(e => { if (this.selectedTools == Tools.DrawCleaningArea) this.drawCleaningAreaReset(); });
        this.onDblClick.subscribe(e => { if (this.selectedTools == Tools.Eraser) this.eraseReset(); });
        this.onDblClick.subscribe(e => { if (this.selectedTools == Tools.Move) this.moveReset(); });

        this.onMouseMove.subscribe(e => { if (this.selectedTools == Tools.Select) this.selecting(); });
        this.onMouseMove.subscribe(e => { if (this.selectedTools == Tools.DrawPlace) this.drawingPlace(); });
        this.onMouseMove.subscribe(e => { if (this.selectedTools == Tools.DrawCleaningArea) this.drawingCleaningArea(); });
        this.onMouseMove.subscribe(e => { if (this.selectedTools == Tools.DrawRoute) this.drawingRoute(); });
        this.onMouseMove.subscribe(e => { if (this.selectedTools == Tools.Move) this.moving(); });

        //this.onMouseUp.subscribe(e => { if (this.selectedTools == Tools.DrawPlace) this.drawPlace(); });
        this.onMouseUp.subscribe(e => { if (this.selectedTools == Tools.DrawStation) this.drawStation(); });
        this.onMouseUp.subscribe(e => { if (this.selectedTools == Tools.DrawRoute) this.drawRoute(); });
        this.onMouseUp.subscribe(e => { if (this.selectedTools == Tools.DrawCleaningArea) this.drawCleaningArea(); });
        this.onMouseUp.subscribe(e => { if (this.selectedTools == Tools.DrawElement) this.drawElement(); });
        this.onMouseUp.subscribe(e => { if (this.selectedTools == Tools.Move) this.move(); });

        this.hotkeysService.add(new Hotkey('enter', (e: KeyboardEvent) => {
            if (this.selectedArea) {
                if (this.selectedTools == Tools.DrawPlace && this.selectedPlace) this.setPlaceCordination(this.selectedPlace);
                if (this.selectedTools == Tools.DrawStation && this.selectedStation) this.setStationCordination(this.selectedStation);
                if (this.selectedTools == Tools.DrawElement && this.selectedElement) this.setElementCordination(this.selectedElement);
                if (this.selectedTools == Tools.DrawCleaningArea && this.selectedCleaningArea) this.setCleaningAreaCordination(this.selectedCleaningArea);
            }
            return false;
        }))
    }

    ngOnChanges(changes: SimpleChanges): void {

    }

    getToolClass(tool: Tools): string {
        return (tool == this.selectedTools ? 'primary' : 'dashed');
    }
    reset() {
        this.places = null;
        this.stations = null;
        this.placeElements = null;
        this.routes = null;
        this.routeItems = null;
        this.cleaningAreas = null;
        this.elevators = null;

        this.items = this.getItems();
    }
    isTool(tool: Tools): boolean {
        return this.selectedTools == tool;
    }
    loadended() {
        this.isLoading = false;
        this.items = this.getItems();
    }
    placeCompare(o1: PlaceGetDto, o2: PlaceGetDto) { return (o1 && o2 && o1.Id === o2.Id); }
    selectPlace(value?: PlaceGetDto) {
        this.selectedPlace = value;

        this.reset();
        if (this.selectedPlace) {
            this.Place = this.selectedPlace;
            this.selectedPlaces.push(this.selectedPlace);
            setTimeout(() => this.selectedPlace = null);
        }
        this.loadItems();
    }
    selectPrePlace() {
        this.selectedPlaces.pop();
        this.Place = this.LastSelectedPlace;
        this.loadItems();
    }
    selectNewPlace() {
        if (confirm('در صورت تایید تغییرات وارد شده از بین خواهد رفت. آیا مطمئن هستید؟')) {
            this.reset();
            this.Place = null;
            if (this.Places.length == 0) this.selectPrePlace();
        }
    }
    selectArea() {
        if (this.mouseState == MouseSate.Down) {
            const rect = Rectangle.create(this.startPoint.toPercentage(this.svgWidth, this.svgHeight), this.movePoint.toPercentage(this.svgWidth, this.svgHeight));
            if (rect.width > 1 && rect.height > 1) this.selectedArea = rect;
            else this.selectedArea = null;
        }
    }
    clearSelectedArea() {
        if (this.selectedArea && this.selectedArea.width > 3 && this.selectedArea.height > 3) this.selectedArea = null;
    }
    selectPoint() {
        const heightUnit = this.imageWidth / this.imageHeight;
        const rect = Rectangle.create(this.startPoint.toPercentage(this.svgWidth, this.svgHeight).add(-1 / this.scale, -heightUnit / this.scale), this.startPoint.toPercentage(this.svgWidth, this.svgHeight).add(1 / this.scale, heightUnit / this.scale));
        this.selectedArea = rect;
    }
    clearSelectedPoint() {
        if (this.selectedArea && this.selectedArea.width <= 3 && this.selectedArea.height <= 3) this.selectedArea = null;
    }
    loadItems() {
        this.Places = this.allPlaces.filter(x => x.ParentId == (this.Place && this.Place.Id ? this.Place.Id : null));

        if (this.Places.length == 0) {
            if (this.Place) {
                this.placeService.getStations(this.Place.Id).subscribe(op => this.Stations = op.Data.filter(x => x.PlaceId == this.place.Id));
                this.placeService.getElements(this.Place.Id).subscribe(op => this.PlaceElements = op.Data.filter(x => x.PlaceId == this.place.Id));
                this.placeService.getRoutes(this.Place.Id).subscribe(op => this.Routes = op.Data.filter(x => x.PlaceId == this.place.Id));
                this.placeService.getRoutItems(this.Place.Id).subscribe(op => this.RouteItems = op.Data.filter(x => x.Station.PlaceId == this.place.Id));
                this.placeService.getCleaningAreas(this.Place.Id).subscribe(op => this.CleaningAreas = op.Data.filter(x => x.PlaceId == this.place.Id));
                this.placeService.getElevators(this.Place.Id).subscribe(op => this.Elevators = op.Data.filter(x => x.PlaceId == this.place.Id));
            }
        }

        this.isLoading = true;
    }
    changeTool(tool: Tools) {
        this.selectedTools = tool;
        switch (tool) {
            case Tools.Select: this.selectInit(); break;
            case Tools.DrawPlace: this.drawPlaceInit(); break;
            case Tools.DrawStation: this.drawStationInit(); break;
            case Tools.DrawElement: this.drawElementInit(); break;
            case Tools.DrawRoute: this.drawRouteInit(); break;
            case Tools.DrawCleaningArea: this.drawCleaningAreaInit(); break;
            case Tools.Eraser: this.eraseInit(); break;
            case Tools.Move: this.moveInit(); break;
        }
        this.items = this.getItems();
    }
    getPoint(e: any): Point {
        var rect = this.svg.getBoundingClientRect();
        var x = e.clientX - rect.left;
        var y = e.clientY - rect.top;
        return new Point(x, y);
    }
    getImage(): string {
        if (this.Place && this.Place.Image) return this.Place.Image;
        else if (this.LastSelectedPlace && this.LastSelectedPlace.Image) return this.LastSelectedPlace.Image;
        else if (this.selectedPlaces.length > 1) {
            const prePlace = this.selectedPlaces[this.selectedPlaces.length - 2];
            if (prePlace && prePlace.Image) return this.placeService.getDynamicImageUrl(prePlace.Id, this.LastSelectedPlace.Cordination);
        }
        return null;
    }
    getItems(): IMapItem[] {
        let places = [], stations = [], elements = [], cleaningAreas = [], routes = [];
        
        const resolutionScale = window.innerWidth / 1920;

        if (!this.isLoading) {
            const map = new Map;
            map.width = this.svgWidth;
            map.height = this.svgHeight;
            if (this.selectedTools == Tools.Select || this.selectedTools == Tools.Eraser || this.selectedTools == Tools.DrawPlace) {
                places = this.PlacesCordinated.map(x => {
                    const item = new Place(x, Cordination.toRectangle(x.Cordination, this.imageWidth, this.imageHeight))
                    item.map = map;
                    return item;
                });
            }
            if (this.selectedTools == Tools.Select || this.selectedTools == Tools.Eraser || this.selectedTools == Tools.DrawStation || this.selectedTools == Tools.DrawRoute) {
                stations = this.StationsCordinated.map(x => {
                    const item = new Station(x, new Point(x.Cordination.X, x.Cordination.Y));
                    item.map = map;
                    item.scale = this.scale * resolutionScale;
                    return item;
                });
            }
            if (this.selectedTools == Tools.Select || this.selectedTools == Tools.Eraser || this.selectedTools == Tools.DrawElement) {
                elements = this.ElementsCordinated.map(x => {
                    x.Element = this.Elements.find(y => y.Id == x.ElementId);
                    x.Place = this.Places.find(y => y.Id == x.PlaceId);
                    const item = new Element(x, new Point(x.Cordination.X, x.Cordination.Y));
                    item.map = map;
                    item.scale = this.scale * resolutionScale;
                    return item;
                });
            }
            if (this.selectedTools == Tools.Select || this.selectedTools == Tools.Eraser || this.selectedTools == Tools.DrawCleaningArea) {
                cleaningAreas = this.CleaningAreasCordinated.map(x => {
                    const item = new CleaningArea(x, Cordination.toRectangle(x.Cordination, this.imageWidth, this.imageHeight));
                    item.map = map;
                    return item;
                });
            }
            if (this.selectedTools == Tools.Select || this.selectedTools == Tools.DrawRoute) {
                routes = this.Routes.map((x, i) => {
                    const item = new Route(x);
                    item.color = MapColors[i % 11];
                    if(this.selectedRoute && this.selectedRoute.Id == x.Id) item.color = "#f00";
                    item.scale = this.scale * resolutionScale;
                    const routeItems = this.RouteItems.filter(y => y.RouteId == x.Id);
                    for (let i = 0; i < routeItems.length; i++) {
                        const routeItem = routeItems[i];
                        const station = stations.find(y => y.obj.Id == routeItem.StationId);
                        if (station) item.items.push(new RouteItem(item, station, i));
                    }
                    item.map = map;
                    return item;
                });
            }
        }
        return [].concat(cleaningAreas, routes, places, stations, elements);
    }
    onClickEvent(e: MouseEvent) {
        this.onClick.emit(e);
    }
    onDblClickEvent(e: MouseEvent) {
        this.onDblClick.emit(e);
    }
    onMouseDownEvent(e: any) {
        this.mouseState = MouseSate.Down;
        this.startPoint = this.getPoint(e);
        this.onMouseDown.emit(e);
    }
    onMouseUpEvent(e: any) {
        this.mouseState = MouseSate.Up;
        this.endPoint = this.getPoint(e);
        this.onMouseUp.emit(e);
    }
    onMouseMoveEvent(e: any) {
        this.movePoint = this.getPoint(e);
        this.onMouseMove.emit(e);
    }
    onScrollEvent(e: MouseWheelEvent) {
        this.onScroll.emit(e);
    }
    onMouseUpItem(item: IMapItem, e: MouseEvent) {
    }
    onMouseDownItem(item: IMapItem, e: MouseEvent) {
        if (this.selectedTools == Tools.Eraser) {
            if (confirm(`آیا از حذف مورد ((${item.getName()})) اطمینان دارید؟`)) {
                if (item instanceof Place) {
                    this.isLoading = true;
                    this.isLoading = true;
                    this.placeService.removeCordination(item.obj.Id).subscribe(op => {
                        if (op.Success) {
                            item.obj.Cordination = op.Data.Cordination;
                            this.items = this.getItems();
                        }
                    }, e => this.loadended(), () => this.loadended())
                }
                if (item instanceof Station) {
                    this.isLoading = true;
                    this.stationService.removeCordination(item.obj.Id).subscribe(op => {
                        if (op.Success) {
                            item.obj.Cordination = op.Data.Cordination;
                            this.items = this.getItems();
                        }
                    }, e => this.loadended(), () => this.loadended())
                }
                if (item instanceof Element) {
                    this.isLoading = true;
                    this.placeElementService.remove(item.obj.Id).subscribe(op => {
                        if (op.Success) {
                            this.PlaceElements.splice(this.PlaceElements.indexOf(this.PlaceElements.find(x => x.Id == op.Data.Id)), 1);
                            this.items = this.getItems();
                        }
                    }, e => this.loadended(), () => this.loadended())
                }
                if (item instanceof CleaningArea) {
                    this.isLoading = true;
                    this.cleaningAreaService.removeCordination(item.obj.Id).subscribe(op => {
                        if (op.Success) {
                            item.obj.Cordination = op.Data.Cordination;
                            this.items = this.getItems();
                        }
                    }, e => this.loadended(), () => this.loadended())
                }
            }
        }
        else if (this.selectedTools == Tools.Move) {

        }
        else if (this.selectedTools == Tools.DrawRoute) {
            if (item instanceof Station) {
                this.toggleRouteItem(item);
            }
        }
    }
    zoomIn() {
        let index = this.scales.indexOf(this.scale);
        if (index + 1 < this.scales.length) this.scale = this.scales[index + 1];
        this.zoomChange();
    }
    zoomOut() {
        let index = this.scales.indexOf(this.scale);
        if (index > 0) this.scale = this.scales[index - 1];
        this.zoomChange();
    }
    zoomChange() {
        setTimeout(() => this.items = this.getItems());
    }
    selectInit() {

    }
    selecting() {
        this.selectArea();
    }
    selectReset() {
        this.clearSelectedArea();

    }
    drawPlaceInit() {

    }
    drawingPlace() {
        this.selectArea();
    }
    newPlace() {
        this.newPlaceModal = this.modalService.create({
            nzTitle: 'بخش جدید',
            nzContent: this.newPlaceTpl,
            nzFooter: null,
        })
    }
    newPlaceComplete(op) {
        if (op && op.Data) {
            const Place = op.Data;
            this.Places.push(Place);
            this.items = this.getItems();
        }
        this.newPlaceModal.destroy();
    }
    setPlaceCordination(item: PlaceGetDto) {
        if (item && this.selectedArea) {
            this.isLoading = true;
            this.placeService.setCordination(item.Id, this.selectedArea.toCordination()).subscribe(op => {
                if (op.Success) {
                    item.Cordination = op.Data.Cordination;
                    this.clearSelectedArea();
                    if (this.PlacesNotCordinate.length > 0) this.selectedPlace = this.PlacesNotCordinate[0];
                    else this.selectedPlace = null;
                }
            }, e => this.loadended(), () => this.loadended());
        }
    }
    drawPlaceReset() {
        this.clearSelectedArea();

    }
    drawStationInit() {
        this.clearSelectedArea();
    }
    drawStation() {
        this.selectPoint();
    }
    newStation() {
        this.newStationObj = new StationAddDto();
        this.newStationObj.Place = this.Place;
        this.newStationModal = this.modalService.create({
            nzTitle: 'ایستگاه جدید',
            nzContent: this.newStationTpl,
            nzFooter: null,
        })
    }
    newStationComplete(op) {
        if (op && op.Data) {
            const item = op.Data;
            this.stations.push(item);
            this.selectedStation = item;
        }
        this.newPlaceModal.destroy();
    }
    setStationCordination(item: StationGetDto) {
        if (item && this.selectedArea) {
            this.isLoading = true;
            this.stationService.setCordination(item.Id, this.selectedArea.getCenter().toCordination()).subscribe(op => {
                if (op.Success) {
                    item.Cordination = op.Data.Cordination;
                    if (this.StationsNotCordinate.length > 0) this.selectedStation = this.StationsNotCordinate[0];
                    else this.selectedStation = null;
                    this.clearSelectedPoint();
                    this.items = this.getItems();
                }
            }, e => this.loadended(), () => this.loadended());
        }
    }
    drawStationReset() {

    }
    drawElementInit() {
        this.clearSelectedArea();

    }
    drawElement() {
        this.selectPoint();
    }
    newElement() {
        this.newElementObj = new ElementAddDto();
        this.newElementModal = this.modalService.create({
            nzTitle: 'المان جدید',
            nzContent: this.newElementTpl,
            nzFooter: null,
        })
    }
    newElementComplete(op) {
        if (op && op.Data) {
            const element = op.Data;
            this.Elements.push(element);
            this.selectedElement = element;
        }
        this.newElementModal.destroy();
    }
    setElementCordination(element: ElementGetDto) {
        if (element && this.selectedArea) {
            const pe = new PlaceElementAddDto;
            pe.ElementId = element.Id;
            pe.PlaceId = this.Place.Id;
            pe.Cordination = this.selectedArea.getCenter().toCordination()
            this.isLoading = true;
            this.placeElementService.create(pe).subscribe(op => {
                if (op.Success) {
                    this.PlaceElements.push(op.Data);
                    this.clearSelectedPoint();
                    this.items = this.getItems();
                }
            }, e => this.loadended(), () => this.loadended());
        }
    }
    drawElementReset() {

    }

    newRoute() {
        this.newRouteModal = this.modalService.create({
            nzTitle: 'مسیر جدید',
            nzContent: this.newRouteTpl,
            nzFooter: null,
        })
    }
    newRouteComplete(op) {
        if (op && op.Data) {
            const item = op.Data;
            this.Routes.push(item);
            this.selectedRoute = item;
            this.items = this.getItems();
        }
        this.newRouteModal.destroy();
    }
    toggleRouteItem(station: Station) {
        if (!this.selectedRoute) return;
        if (this.RouteItems.find(x => x.StationId == station.obj.Id && x.RouteId == this.selectedRoute.Id))
            this.removeRouteItem(station.obj.Id);
        else if (!this.RouteItems.find(x => x.StationId == station.obj.Id))
            this.addRouteItem(station.obj);
    }
    addRouteItem(station: StationGetDto) {
        const items = this.RouteItems.filter(x => x.RouteId == this.selectedRoute.Id);
        const item = new RouteItemAddDto();
        item.StationId = station.Id;
        item.Order = (items.length > 0 ? items[items.length - 1].Order + 1 : 0);
        this.routeService.addItem(this.selectedRoute.Id, item).subscribe(op => {
            if (op.Success) {
                this.RouteItems.push(op.Data);
                this.items = this.getItems();
            }
        })
    }
    removeRouteItem(stationId: number) {
        this.routeService.removeItem(this.selectedRoute.Id, stationId).subscribe(op => {
            if (op.Success) {
                this.RouteItems.splice(this.RouteItems.indexOf(this.RouteItems.find(x => x.StationId == stationId)), 1);
                this.items = this.getItems();
            }
        })
    }
    drawRouteInit() {
        this.clearSelectedArea();

    }
    drawingRoute() {

    }
    drawRoute() {

    }
    drawRouteReset() {

    }

    newCleaningArea() {
        this.newCleaningAreaModal = this.modalService.create({
            nzTitle: 'محوطه رفت و روب جدید',
            nzContent: this.newCleaningAreaTpl,
            nzFooter: null,
        })
    }
    newCleaningAreaComplete(op) {
        if (op && op.Data) {
            const item = op.Data;
            this.CleaningAreas.push(item);
            this.selectedCleaningArea = item;
            this.items = this.getItems();
        }
        this.newCleaningAreaModal.destroy();
    }
    setCleaningAreaCordination(item: CleaningAreaGetDto) {
        if (item && this.selectedArea) {
            this.isLoading = true;
            this.cleaningAreaService.setCordination(item.Id, this.selectedArea.toCordination()).subscribe(op => {
                if (op.Success) {
                    item.Cordination = op.Data.Cordination;
                    this.clearSelectedArea();
                    if (this.CleaningAreasNotCordinate.length > 0) this.selectedCleaningArea = this.CleaningAreasNotCordinate[0];
                    else this.selectedCleaningArea = null;
                }
            }, e => this.loadended(), () => this.loadended());
        }
    }
    drawCleaningAreaInit() {

    }
    drawingCleaningArea() {
        this.selectArea();
    }
    drawCleaningArea() {

    }
    drawCleaningAreaReset() {
        this.clearSelectedArea();

    }
    eraseInit() {

    }
    erase() {

    }
    eraseReset() {
        this.clearSelectedArea();

    }
    moveInit() {

    }
    moving() {

    }
    move() {

    }
    moveReset() {
        this.clearSelectedArea();

    }
    fullscreen() {
        this.isFullscreen = true;
    }
    fullscreenExit() {
        this.isFullscreen = false;
    }
    help() {

    }
}