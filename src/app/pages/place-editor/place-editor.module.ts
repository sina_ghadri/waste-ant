import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { HotkeyModule, HotkeysService } from 'angular2-hotkeys';

import { PlaceEditorRoutingModule } from './place-editor.routing';
import { PlaceEditorComponent } from './place-editor.component';
import { PlaceModule } from '../place/place.module';
import { SafeHtmlModule } from 'src/app/libs/bitspco/pipes/safe.pipe';
import { StationModule } from '../station/station.module';
import { ElementModule } from '../element/element.module';
import { RouteModule } from '../route/route.module';
import { CleaningAreaModule } from '../cleaning-area/cleaning-area.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        PlaceEditorRoutingModule,

        HotkeyModule,

        PlaceModule,
        StationModule,
        ElementModule,
        RouteModule,
        CleaningAreaModule,

        SafeHtmlModule,
    ],
    declarations: [
        PlaceEditorComponent
    ],
    providers: [HotkeysService]
})
export class PlaceEditorModule { }