import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PlaceEditorComponent } from './place-editor.component';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: PlaceEditorComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class PlaceEditorRoutingModule {}