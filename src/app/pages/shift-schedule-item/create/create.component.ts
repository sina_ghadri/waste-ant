import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ShiftScheduleItemService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ShiftScheduleItemAddDto } from 'src/app/models';

@Component({
    selector: 'app-shift-schedule-item-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ShiftScheduleItemCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:ShiftScheduleItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}