import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ShiftScheduleItemService } from "../../../services";
import { ShiftScheduleItemGetDto, ShiftScheduleItemAddDto, ShiftScheduleItemChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-shift-schedule-item-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ShiftScheduleItemIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ShiftScheduleItemService;

  constructor(service: ShiftScheduleItemService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
