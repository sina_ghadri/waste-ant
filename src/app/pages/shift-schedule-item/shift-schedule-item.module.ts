import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ShiftScheduleItemRoutingModule } from './shift-schedule-item.routing';
import { ShiftScheduleItemIndexComponent } from './index/index.component';
import { ShiftScheduleItemEditComponent } from './edit/edit.component';
import { ShiftScheduleItemDetailComponent } from './detail/detail.component';
import { ShiftScheduleItemCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ShiftScheduleItemRoutingModule,
    ],
    declarations: [
        ShiftScheduleItemIndexComponent,
        ShiftScheduleItemCreateComponent,
        ShiftScheduleItemEditComponent,
        ShiftScheduleItemDetailComponent,
    ]
})
export class ShiftScheduleItemModule {}