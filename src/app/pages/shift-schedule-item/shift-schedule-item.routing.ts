import { RouterModule } from '@angular/router';
import { ShiftScheduleItemIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ShiftScheduleItemIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ShiftScheduleItemRoutingModule {}