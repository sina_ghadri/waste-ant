import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ShiftScheduleItemService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ShiftScheduleItemChangeDto } from 'src/app/models';

@Component({
    selector: 'app-shift-schedule-item-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ShiftScheduleItemEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:ShiftScheduleItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}