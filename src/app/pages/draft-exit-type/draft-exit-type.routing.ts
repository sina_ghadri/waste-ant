import { RouterModule } from '@angular/router';
import { DraftExitTypeIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: DraftExitTypeIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class DraftExitTypeRoutingModule {}