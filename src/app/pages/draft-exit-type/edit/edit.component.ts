import { Component, OnInit } from "@angular/core";
import { DraftExitTypeService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
    selector: 'app-draft-exit-type-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class DraftExitTypeEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:DraftExitTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}