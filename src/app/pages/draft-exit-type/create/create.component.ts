import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DraftExitTypeService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { DraftExitTypeAddDto } from 'src/app/models';

@Component({
    selector: 'app-draft-exit-type-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class DraftExitTypeCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:DraftExitTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}