import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { DraftExitTypeService } from "../../../services";
import { DraftExitTypeGetDto, DraftExitTypeAddDto, DraftExitTypeChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-draft-exit-type-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class DraftExitTypeIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: DraftExitTypeService;

  constructor(service: DraftExitTypeService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
