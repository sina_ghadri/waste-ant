import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DraftExitTypeService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { DraftExitTypeGetDto } from 'src/app/models';

@Component({
    selector: 'app-draft-exit-type-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DraftExitTypeDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:DraftExitTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}