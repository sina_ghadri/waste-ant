import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { DraftExitTypeRoutingModule } from './draft-exit-type.routing';
import { DraftExitTypeIndexComponent } from './index/index.component';
import { DraftExitTypeEditComponent } from './edit/edit.component';
import { DraftExitTypeDetailComponent } from './detail/detail.component';
import { DraftExitTypeCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        DraftExitTypeRoutingModule,
    ],
    declarations: [
        DraftExitTypeIndexComponent,
        DraftExitTypeCreateComponent,
        DraftExitTypeEditComponent,
        DraftExitTypeDetailComponent,
    ]
})
export class DraftExitTypeModule {}