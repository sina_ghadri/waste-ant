import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { InspectionRoutingModule } from './inspection.routing';
import { InspectionIndexComponent } from './index/index.component';
import { MapViewerModule } from 'src/app/components/map-viewer/map-viewer.module';
import { LayoutMapModule } from 'src/app/layouts/map/map.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        InspectionRoutingModule,

        LayoutMapModule
    ],
    declarations: [
        InspectionIndexComponent,
    ]
})
export class InspectionModule {}