import { RouterModule } from '@angular/router';
import { InspectionIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: InspectionIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class InspectionRoutingModule {}