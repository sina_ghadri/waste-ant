import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { InspectionService, PlaceService, IndexService, StationService } from "../../../services";
import { InspectionGetDto, InspectionAddDto, InspectionChangeDto, Place, PlaceGetDto, Station, StationGetDto, IndexGetDto, InspectionIndexAddDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { LayoutMapComponent } from 'src/app/layouts/map/map.component';

@Component({
  selector: "app-inspection-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class InspectionIndexComponent implements OnInit {
  place: PlaceGetDto;
  station: Station;
  indices: IndexGetDto[] = [];
  inspection: InspectionAddDto;
  isCreatingInspection: boolean;

  inspectionModal: NzModalRef;
  @ViewChild('inspectionTpl', { static: false }) inspectionTpl: TemplateRef<any>;

  @ViewChild(LayoutMapComponent, { static: false }) map: LayoutMapComponent;

  get Indices(): IndexGetDto[] {
    if (!this.indices) this.indices = [];
    return this.indices;
  }

  get InspectionIndices(): InspectionIndexAddDto[] {
    if (!this.inspection) return [];
    if (!this.inspection.Indices) this.inspection.Indices = [];
    return this.inspection.Indices;
  }

  constructor(
    private service: InspectionService,
    private modalService: NzModalService,
    private placeService: PlaceService,
    private indexService: IndexService,
    private stationService: StationService,
  ) {

  }

  ngOnInit(): void {
  }
  onback() {
    this.place = this.place.Parent;
  }
  onstation(obj: Station) {
    obj.onclick.subscribe((e: { event: any, item: Station }) => {
      this.station = e.item;
      if (!this.Indices.length) {
        this.indices = [];
        this.indexService.getAll().subscribe(op => {
          this.indices = op.Data.filter(x => !x.ParentId);
          for (let i = 0; i < this.indices.length; i++) {
            const index = this.indices[i];
            index.Children = op.Data.filter(x => x.ParentId == index.Id);
          }
        })
      }

      this.inspection = new InspectionAddDto;
      this.inspection.Station = e.item.obj;
      this.inspection.StationId = this.inspection.Station.Id;

      this.stationService.getLastInspection(e.item.obj.Id).subscribe(op => {
        if (op.Success) {
          const indices = op.Data.Indices;
          for (let i = 0; i < indices.length; i++) {
            const inspection_index = indices[i];
            this.setInspection(inspection_index.IndexId, inspection_index.Grade);
          }
        }
      });

      this.inspectionModal = this.modalService.create({
        nzWidth: '768px',
        nzTitle: 'بازرسی',
        nzContent: this.inspectionTpl,
        nzFooter: null
      })

    });
  }
  toggle(li: HTMLElement) {
    for (let i = 0; i < li.parentNode.children.length; i++) {
      const item = li.parentNode.children[i] as HTMLElement;
      if (item != li) item.classList.remove('open');
    }
    if (li.classList.contains('open')) {
      li.classList.remove('open');
    }
    else {
      li.classList.add('open');

    }
  }
  getIndexGrade(parent: IndexGetDto): number {
    if (!parent.Children) return 0;
    let sumGrade = 0;
    let sumFactor = 0;
    for (let i = 0; i < parent.Children.length; i++) {
      const child = parent.Children[i];
      const item = this.InspectionIndices.find(x => x.IndexId == child.Id);
      const factor = (child.SignificanceFactor ? child.SignificanceFactor : 1);
      if (item) {
        sumGrade += item.Grade * factor;
        sumFactor += factor;
      }
    }
    return (sumFactor > 0 ? sumGrade / sumFactor : 0);
  }
  getIndexGradeClass(parent: IndexGetDto): string {
    const grade = this.getIndexGrade(parent);
    if (grade > 0.5) return 'verygood';
    else if (grade > 0) return 'good';
    else if (grade < -0.5) return 'verybad';
    else if (grade < 0) return 'bad';
    return 'normal';
  }
  getIndexGradeOpacity(parent: IndexGetDto): number {
    const grade = this.getIndexGrade(parent);

    if (grade > 0.5) return grade;
    else if (grade > 0) return grade * 2;
    else if (grade < -0.5) return -grade;
    else if (grade < 0) return -grade * 2;
    return 0.2;
  }
  checkInspection(indexId: number, grade: number) {
    const item = this.InspectionIndices.find(x => x.IndexId == indexId);
    if (item) {
      if (item.Grade == grade) return true;
    } else if (grade == 0) return true;
    return false;
  }
  setInspection(indexId: number, grade: number) {
    let item = this.InspectionIndices.find(x => x.IndexId == indexId);
    if (item) {
      if (grade) {
        item.Grade = grade;
      } else {
        this.InspectionIndices.splice(this.InspectionIndices.indexOf(item), 1);
      }
    } else {
      if (grade) {
        item = new InspectionIndexAddDto;
        item.IndexId = indexId;
        item.Grade = grade;
        this.InspectionIndices.push(item);
      }
    }
  }
  submit() {
    this.isCreatingInspection = true;
    this.service.create(this.inspection).subscribe(op => {
      if (op.Success) {
        this.station.obj.Grade = op.Data.Station.Grade;
        this.inspectionModal.destroy();
      }
      this.isCreatingInspection = false;
    }, e => this.isCreatingInspection = false);
  }
  close() {
    this.inspectionModal.destroy();
  }
}
