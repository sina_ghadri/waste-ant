import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestEquipmentService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RequestEquipmentAddDto } from 'src/app/models';

@Component({
    selector: 'app-request-equipment-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RequestEquipmentCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:RequestEquipmentService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}