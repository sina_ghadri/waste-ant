import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RequestEquipmentRoutingModule } from './request-equipment.routing';
import { RequestEquipmentIndexComponent } from './index/index.component';
import { RequestEquipmentEditComponent } from './edit/edit.component';
import { RequestEquipmentDetailComponent } from './detail/detail.component';
import { RequestEquipmentCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RequestEquipmentRoutingModule,
    ],
    declarations: [
        RequestEquipmentIndexComponent,
        RequestEquipmentCreateComponent,
        RequestEquipmentEditComponent,
        RequestEquipmentDetailComponent,
    ]
})
export class RequestEquipmentModule {}