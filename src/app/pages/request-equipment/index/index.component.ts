import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RequestEquipmentService } from "../../../services";
import { RequestEquipmentGetDto, RequestEquipmentAddDto, RequestEquipmentChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-request-equipment-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RequestEquipmentIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: RequestEquipmentService;

  constructor(service: RequestEquipmentService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
