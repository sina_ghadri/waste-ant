import { RouterModule } from '@angular/router';
import { RequestEquipmentIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: RequestEquipmentIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class RequestEquipmentRoutingModule {}