import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { UserPlaceRoutingModule } from './user-place.routing';
import { UserPlaceIndexComponent } from './index/index.component';
import { UserPlaceEditComponent } from './edit/edit.component';
import { UserPlaceDetailComponent } from './detail/detail.component';
import { UserPlaceCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        UserPlaceRoutingModule,
    ],
    declarations: [
        UserPlaceIndexComponent,
        UserPlaceCreateComponent,
        UserPlaceEditComponent,
        UserPlaceDetailComponent,
    ],
    exports: [
        UserPlaceIndexComponent,
        UserPlaceCreateComponent,
        UserPlaceEditComponent,
        UserPlaceDetailComponent,
    ]
})
export class UserPlaceModule {}