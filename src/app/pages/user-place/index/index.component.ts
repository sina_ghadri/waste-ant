import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { UserPlaceService } from "../../../services";
import { UserPlaceGetDto, UserPlaceAddDto, UserPlaceChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-user-place-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class UserPlaceIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: UserPlaceService;

  constructor(service: UserPlaceService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
