import { RouterModule } from '@angular/router';
import { UserPlaceIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: UserPlaceIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class UserPlaceRoutingModule {}