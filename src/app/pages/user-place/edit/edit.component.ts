import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UserPlaceService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { UserPlaceChangeDto } from 'src/app/models';

@Component({
    selector: 'app-user-place-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class UserPlaceEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:UserPlaceService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}