import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UserPlaceService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { UserPlaceGetDto } from 'src/app/models';

@Component({
    selector: 'app-user-place-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class UserPlaceDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:UserPlaceService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}