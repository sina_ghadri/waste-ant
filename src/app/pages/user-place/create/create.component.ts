import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UserPlaceService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { UserPlaceAddDto } from 'src/app/models';

@Component({
    selector: 'app-user-place-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class UserPlaceCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:UserPlaceService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}