import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ContainerService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ContainerAddDto } from 'src/app/models';

@Component({
    selector: 'app-container-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ContainerCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:ContainerService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}