import { RouterModule } from '@angular/router';
import { ContainerIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ContainerIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ContainerRoutingModule {}