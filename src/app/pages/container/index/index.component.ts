import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ContainerService } from "../../../services";
import { ContainerGetDto, ContainerAddDto, ContainerChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-role-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ContainerIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ContainerService;

  constructor(service: ContainerService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
