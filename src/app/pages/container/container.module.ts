import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ContainerRoutingModule } from './container.routing';
import { ContainerIndexComponent } from './index/index.component';
import { ContainerEditComponent } from './edit/edit.component';
import { ContainerDetailComponent } from './detail/detail.component';
import { ContainerCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ContainerRoutingModule,

        LayoutTableModule
    ],
    declarations: [
        ContainerIndexComponent,
        ContainerCreateComponent,
        ContainerEditComponent,
        ContainerDetailComponent,
    ]
})
export class ContainerModule {}