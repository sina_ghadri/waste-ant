import { RouterModule } from '@angular/router';
import { CalendarComponent } from './calendar.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: CalendarComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class CalendarRoutingModule {}