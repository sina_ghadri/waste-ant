import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { RequestGetDto } from 'src/app/models';

@Component({
    selector: 'app-request-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class RequestDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:RequestService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}