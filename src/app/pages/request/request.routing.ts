import { RouterModule } from '@angular/router';
import { RequestIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: RequestIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class RequestRoutingModule {}