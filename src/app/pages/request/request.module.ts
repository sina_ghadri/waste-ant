import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RequestRoutingModule } from './request.routing';
import { RequestIndexComponent } from './index/index.component';
import { RequestEditComponent } from './edit/edit.component';
import { RequestDetailComponent } from './detail/detail.component';
import { RequestCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RequestRoutingModule,
    ],
    declarations: [
        RequestIndexComponent,
        RequestCreateComponent,
        RequestEditComponent,
        RequestDetailComponent,
    ]
})
export class RequestModule {}