import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RequestAddDto } from 'src/app/models';

@Component({
    selector: 'app-request-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RequestCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:RequestService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}