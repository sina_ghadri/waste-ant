import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RequestService } from "../../../services";
import { RequestGetDto, RequestAddDto, RequestChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-request-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RequestIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: RequestService;

  constructor(service: RequestService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
