import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ElementService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ElementChangeDto } from 'src/app/models';
import { Uploader } from 'src/app/libs/drive/components/uploader/models/uploader';
import { UploaderService } from 'src/app/libs/drive/components/uploader/services/uploader.service';

@Component({
    selector: 'app-element-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ElementEditComponent extends BaseEditComponent implements OnInit {

    uploader: Uploader = new Uploader;
    chooseFlag: boolean = false;

    constructor(private currentService: ElementService, formBuilder: RxFormBuilder, private uploderService: UploaderService) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.uploader.service = this.uploderService;
    }
    choose() {
        this.chooseFlag = true;
    }
    chooseComplete(e) {
        if (e) {
            this.form.get('Image').setValue(e);
        }
        this.chooseFlag = false;
    }
}