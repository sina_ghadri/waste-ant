import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ElementRoutingModule } from './element.routing';
import { ElementIndexComponent } from './index/index.component';
import { ElementEditComponent } from './edit/edit.component';
import { ElementDetailComponent } from './detail/detail.component';
import { ElementCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';
import { UploaderModule } from 'src/app/libs/drive/components/uploader/uploader.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ElementRoutingModule,
        LayoutTableModule,
        UploaderModule
    ],
    declarations: [
        ElementIndexComponent,
        ElementCreateComponent,
        ElementEditComponent,
        ElementDetailComponent,
    ],
    exports: [
        ElementIndexComponent,
        ElementCreateComponent,
        ElementEditComponent,
        ElementDetailComponent,
    ]
})
export class ElementModule {}