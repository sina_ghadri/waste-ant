import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ElementService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ElementAddDto } from 'src/app/models';
import { Uploader } from 'src/app/libs/drive/components/uploader/models/uploader';
import { UploaderService } from 'src/app/libs/drive/components/uploader/services/uploader.service';

@Component({
    selector: 'app-element-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ElementCreateComponent extends BaseCreateComponent implements OnInit {

    uploader: Uploader = new Uploader;
    chooseFlag: boolean = false;

    constructor(private currentService: ElementService, formBuilder: RxFormBuilder, private uploderService: UploaderService) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.uploader.service = this.uploderService;
    }
    choose() {
        this.chooseFlag = true;
    }
    chooseComplete(e) {
        if (e) {
            this.form.get('Image').setValue(e);
        }
        this.chooseFlag = false;
    }
}