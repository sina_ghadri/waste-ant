import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ElementService } from "../../../services";
import { ElementGetDto, ElementAddDto, ElementChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-element-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ElementIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ElementService;

  constructor(service: ElementService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
