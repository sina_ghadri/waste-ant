import { RouterModule } from '@angular/router';
import { ElementIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ElementIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ElementRoutingModule {}