import { Component, OnInit, Input } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { StationService, UseTypeService, UserService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { StationAddDto, UseTypeGetDto, UserGetInterfaceDto, BaseGetDto, ODataQueryOptions } from 'src/app/models';
import { NzTreeNode, NzTreeNodeOptions } from 'ng-zorro-antd';
import { TreeExtension } from 'src/app/libs/bitspco/components/base/tree.extension';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-station-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class StationCreateComponent extends BaseCreateComponent implements OnInit {
    @Input() placeId: number;
    
    expandKeys: string[] = [];
    useTypes: UseTypeGetDto[];
    useTypeNodes: NzTreeNodeOptions[] = [];
    users: UserGetInterfaceDto[];
    treeExt: TreeExtension = new TreeExtension;

    isloadingUser: boolean = false;

    constructor(
        private currentService: StationService,
        private userService: UserService,
        private useTypeService: UseTypeService,
        formBuilder: RxFormBuilder
    ) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.useTypeService.getAll().subscribe(op => {
            this.useTypes = op.Data;
            this.useTypeNodes = this.treeExt.createTreeNodes(this.useTypes).map(x => this.treeExt.convertTreeNode2Options(x));
        });
        this.onSearchUser('');
    }
    makeFormGroup(obj: any): FormGroup {
        obj = new StationAddDto;
        obj.PlaceId = this.placeId;
        return super.makeFormGroup(obj);
    }
    onSearchUser(value: string) {
        const query = new ODataQueryOptions;
        query.orderby = 'Id';
        query.top = 10;
        if(value) query.filter = `Name like \"${value}\"`;
        this.isloadingUser = true;
        this.userService.select(query).subscribe(op => {
            this.users = op.Data;
            this.isloadingUser = false;
        }, e => this.isloadingUser = false);
    }
}