import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { StationService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { StationGetDto } from 'src/app/models';

@Component({
    selector: 'app-station-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class StationDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:StationService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}