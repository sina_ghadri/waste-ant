import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { StationRoutingModule } from './station.routing';
import { StationIndexComponent } from './index/index.component';
import { StationEditComponent } from './edit/edit.component';
import { StationDetailComponent } from './detail/detail.component';
import { StationCreateComponent } from './create/create.component';
import { StationComponent } from './station.component';
import { LayoutTreeModule } from 'src/app/layouts/tree/tree.module';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        StationRoutingModule,

        LayoutTreeModule,
        LayoutTableModule,
    ],
    declarations: [
        StationComponent,
        StationIndexComponent,
        StationCreateComponent,
        StationEditComponent,
        StationDetailComponent,
    ],
    exports: [
        StationIndexComponent,
        StationCreateComponent,
        StationEditComponent,
        StationDetailComponent,
    ]
})
export class StationModule {}