import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { StationComponent } from './station.component';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: StationComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class StationRoutingModule {}