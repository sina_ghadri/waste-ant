import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { StationService, UserService, UseTypeService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { StationChangeDto, UseTypeGetDto, UserGetInterfaceDto, ODataQueryOptions } from 'src/app/models';
import { NzTreeNodeOptions } from 'ng-zorro-antd';
import { TreeExtension } from 'src/app/libs/bitspco/components/base/tree.extension';

@Component({
    selector: 'app-station-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class StationEditComponent extends BaseEditComponent implements OnInit {
    expandKeys: string[] = [];
    useTypes: UseTypeGetDto[];
    useTypeNodes: NzTreeNodeOptions[] = [];
    users: UserGetInterfaceDto[];
    treeExt: TreeExtension = new TreeExtension;

    isloadingUser: boolean = false;

    constructor(
        private currentService: StationService,
        private userService: UserService,
        private useTypeService: UseTypeService,
        formBuilder: RxFormBuilder
    ) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.useTypeService.getAll().subscribe(op => {
            this.useTypes = op.Data;
            this.useTypeNodes = this.treeExt.createTreeNodes(this.useTypes).map(x => this.treeExt.convertTreeNode2Options(x));
        });
        this.form.get('UseTypeId').setValue(this.form.get('UseTypeId').value.toString());
        this.onSearchUser('');
    }
    onSearchUser(value: string) {
        const query = new ODataQueryOptions;
        query.orderby = 'Id';
        query.top = 10;
        if(value) query.filter = `Name like \"${value}\"`;
        this.isloadingUser = true;
        this.userService.select(query).subscribe(op => {
            this.users = op.Data;
            this.isloadingUser = false;
        }, e => this.isloadingUser = false);
    }
}