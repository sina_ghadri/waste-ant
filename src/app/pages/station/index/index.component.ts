import { Component, OnInit, TemplateRef, ViewChild, Input, OnChanges, SimpleChanges } from "@angular/core";
import { StationService } from "../../../services";
import { StationGetDto, StationAddDto, StationChangeDto, ODataQueryOptions } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-station-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class StationIndexComponent extends BaseTableLayoutComponent implements OnInit, OnChanges {
  @Input() placeId: number;
  private currentService: StationService;

  constructor(service: StationService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    super.ngOnInit();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.placeId) {
      this.load();
      this.pageIndex = 1;
    }
  }
  load(query?: ODataQueryOptions) {
    query = new ODataQueryOptions;
    if (this.placeId) query.filter = `PlaceId eq ${this.placeId}`;
    super.load(query);
  }
}
