import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RequestCleaningService } from "../../../services";
import { RequestCleaningGetDto, RequestCleaningAddDto, RequestCleaningChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-request-cleaning-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RequestCleaningIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: RequestCleaningService;

  constructor(service: RequestCleaningService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
