import { RouterModule } from '@angular/router';
import { RequestCleaningIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: RequestCleaningIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class RequestCleaningRoutingModule {}