import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestCleaningService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { RequestCleaningGetDto } from 'src/app/models';

@Component({
    selector: 'app-request-cleaning-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class RequestCleaningDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:RequestCleaningService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}