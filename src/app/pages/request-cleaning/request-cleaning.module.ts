import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RequestCleaningRoutingModule } from './request-cleaning.routing';
import { RequestCleaningIndexComponent } from './index/index.component';
import { RequestCleaningEditComponent } from './edit/edit.component';
import { RequestCleaningDetailComponent } from './detail/detail.component';
import { RequestCleaningCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RequestCleaningRoutingModule,
    ],
    declarations: [
        RequestCleaningIndexComponent,
        RequestCleaningCreateComponent,
        RequestCleaningEditComponent,
        RequestCleaningDetailComponent,
    ]
})
export class RequestCleaningModule {}