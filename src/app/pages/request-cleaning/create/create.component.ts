import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestCleaningService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RequestCleaningAddDto } from 'src/app/models';

@Component({
    selector: 'app-request-cleaning-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RequestCleaningCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:RequestCleaningService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}