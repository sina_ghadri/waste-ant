import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestCleaningService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { RequestCleaningChangeDto } from 'src/app/models';

@Component({
    selector: 'app-request-cleaning-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class RequestCleaningEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:RequestCleaningService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}