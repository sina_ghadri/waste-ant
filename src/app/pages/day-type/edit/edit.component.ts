import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayTypeService, DayPartNameService } from '../../../services';
import { Color, DayPartNameGetDto, DayPartAddDto } from 'src/app/models';
import { NzModalService } from 'ng-zorro-antd';
import { TimeSpliteEvent, TimeSplit } from 'src/app/libs/bitspco/components/time-splitter/models';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';

@Component({
    selector: 'app-day-type-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class DayTypeEditComponent extends BaseEditComponent implements OnInit {
    colors: Color[];
    timeSplits: TimeSplit[] = [];
    partName: DayPartNameGetDto;
    partNames: DayPartNameGetDto[];

    @ViewChild('split', { static: false }) splitTpl: TemplateRef<any>;

    constructor(
        private currentService: DayTypeService,
        private dayPartNameService: DayPartNameService,
        private formBuilder: RxFormBuilder,
        private modalService: NzModalService) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        this.currentService.getColors().subscribe(x => this.colors = x.Data);


        this.currentService.getParts(this.id).subscribe(op => {
            const data = op.Data;
            for (let i = 0; i < data.length; i++) {
                const part = data[i];
                const time = new TimeSplit;
                time.data = part.DayPartName;
                time.text = part.DayPartName.Name;
                time.color = part.DayPartName.Color;
                time.time = part.EndTime;
                this.timeSplits.push(time);
            }
        });
    }
    loadParts() {
        if (!this.partNames) {
            this.dayPartNameService.getAll().subscribe(op => {
                this.partNames = op.Data;
            })
        }
    }
    onSplit(e: TimeSpliteEvent) {
        this.loadParts();
        const modal = this.modalService.create({
            nzTitle: 'انتخاب نوع قسمت روز',
            nzContent: this.splitTpl,
            nzFooter: [
                {
                    label: 'انصراف',
                    type: 'default',
                    onClick: () => {
                        modal.destroy();
                    }
                },
                {
                    label: 'تایید',
                    type: 'primary',
                    onClick: () => {
                        const data = e.data;
                        data.text = this.partName.Name;
                        data.color = this.partName.Color;
                        data.data = this.partName;
                        e.callback(data);
                        modal.destroy();
                    }
                }
            ]
        })
    }

    submit() {
        const parts = [];
        let preItem: TimeSplit = null;
        for (let i = 0; i < this.timeSplits.length; i++) {
            const item = this.timeSplits[i];
            const part = new DayPartAddDto;
            part.DayPartNameId = item.data.Id;
            if (i == 0) part.StartTime = this.obj.StartTime;
            else part.StartTime = preItem.time;
            part.EndTime = item.time;
            this.f.Parts.push(part);
            preItem = item;
        }
        this.form.get('Parts').setValue(parts);
        super.submit();
    }
}