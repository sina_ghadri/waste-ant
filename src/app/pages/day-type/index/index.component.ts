import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { DayTypeService } from "../../../services";
import { DayTypeGetDto, DayTypeAddDto, DayTypeChangeDto, DateTime } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-role-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class DayTypeIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: DayTypeService;

  constructor(
    service: DayTypeService, 
    modalService: NzModalService, 
    formBuilder: RxFormBuilder,
    private rxfb:RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }
  create() {
    this.form = this.rxfb.formGroup(DayTypeAddDto, new DayTypeAddDto);
    super.create();
  }
}
