import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { DayTypeRoutingModule } from './day-type.routing';
import { DayTypeIndexComponent } from './index/index.component';
import { DayTypeEditComponent } from './edit/edit.component';
import { DayTypeDetailComponent } from './detail/detail.component';
import { DayTypeCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';
import { TimeSplitterModule } from 'src/app/libs/bitspco/components/time-splitter/time-splitter.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        DayTypeRoutingModule,
        LayoutTableModule,
        TimeSplitterModule
    ],
    declarations: [
        DayTypeIndexComponent,
        DayTypeCreateComponent,
        DayTypeEditComponent,
        DayTypeDetailComponent,
    ]
})
export class DayTypeModule {}