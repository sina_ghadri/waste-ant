import { RouterModule } from '@angular/router';
import { DayTypeIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: DayTypeIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class DayTypeRoutingModule {}