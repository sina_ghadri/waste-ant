import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayTypeService, DayPartNameService } from '../../../services';
import { DayPartNameGetDto, Color, DayPartAddDto } from 'src/app/models';
import { NzModalService } from 'ng-zorro-antd';
import { TimeSpliteEvent, TimeSplit } from 'src/app/libs/bitspco/components/time-splitter/models';
import { BaseCreateComponent } from '../../../libs/bitspco/components/base/create.component';

@Component({
    selector: 'app-day-type-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class DayTypeCreateComponent extends BaseCreateComponent implements OnInit {
    colors: Color[];
    timeSplits: TimeSplit[] = [];
    partName: DayPartNameGetDto;
    partNames: DayPartNameGetDto[];

    @ViewChild('split', { static: false }) splitTpl: TemplateRef<any>;

    constructor(
        private currentService: DayTypeService,
        private formBuilder: RxFormBuilder,
        private dayPartNameService: DayPartNameService,
        private modalService: NzModalService) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.currentService.getColors().subscribe(x => this.colors = x.Data);
    }
    loadParts() {
        if (!this.partNames) {
            this.dayPartNameService.getAll().subscribe(op => {
                this.partNames = op.Data;
            })
        }
    }
    onSplit(e: TimeSpliteEvent) {
        this.loadParts();
        const modal = this.modalService.create({
            nzTitle: 'انتخاب نوع قسمت روز',
            nzContent: this.splitTpl,
            nzFooter: [
                {
                    label: 'انصراف',
                    type: 'default',
                    onClick: () => {
                        modal.destroy();
                    }
                },
                {
                    label: 'تایید',
                    type: 'primary',
                    onClick: () => {
                        const data = e.data;
                        data.text = this.partName.Name;
                        data.color = this.partName.Color;
                        data.data = this.partName;
                        e.callback(data);
                        modal.destroy();
                    }
                }
            ]
        })
    }

    submit() {
        const parts = [];
        let preItem: TimeSplit = null;
        for (let i = 0; i < this.timeSplits.length; i++) {
            const item = this.timeSplits[i];
            const part = new DayPartAddDto;
            part.DayPartNameId = item.data.Id;
            if(i == 0) part.StartTime = this.obj.StartTime;
            else part.StartTime = preItem.time;
            part.EndTime = item.time;
            parts.push(part);
            preItem = item;
        }
        this.form.get('Parts').setValue(parts);
        super.submit();
    }
}