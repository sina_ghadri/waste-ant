import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayTypeService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { DayTypeGetDto } from 'src/app/models';
import { TimeSplit } from 'src/app/libs/bitspco/components/time-splitter/models';

@Component({
    selector: 'app-day-type-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DayTypeDetailComponent extends BaseDetailComponent implements OnInit {
    timeSplits: TimeSplit[] = [];

    constructor(
        private currentService: DayTypeService,
        private formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        this.currentService.getParts(this.id).subscribe(op => {
            const data = op.Data;
            for (let i = 0; i < data.length; i++) {
                const part = data[i];
                const time = new TimeSplit;
                time.text = part.DayPartName.Name;
                time.color = part.DayPartName.Color;
                time.time = part.EndTime;
                this.timeSplits.push(time);
            }
        });
    }
}