import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ShiftPartNameRoutingModule } from './shift-part-name.routing';
import { ShiftPartNameIndexComponent } from './index/index.component';
import { ShiftPartNameEditComponent } from './edit/edit.component';
import { ShiftPartNameDetailComponent } from './detail/detail.component';
import { ShiftPartNameCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ShiftPartNameRoutingModule,
        LayoutTableModule,
    ],
    declarations: [
        ShiftPartNameIndexComponent,
        ShiftPartNameCreateComponent,
        ShiftPartNameEditComponent,
        ShiftPartNameDetailComponent,
    ]
})
export class ShiftPartNameModule {}