import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ShiftPartNameService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { DayPartChangeDto, Color } from 'src/app/models';

@Component({
    selector: 'app-shift-part-name-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ShiftPartNameEditComponent extends BaseEditComponent implements OnInit {
    colors: Color[];

    constructor(private currentService:ShiftPartNameService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        this.currentService.getColors().subscribe(op => this.colors = op.Data);
    }

}