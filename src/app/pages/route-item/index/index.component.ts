import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RouteItemService } from "../../../services";
import { RouteItemGetDto, RouteItemAddDto, RouteItemChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-route-item-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RouteItemIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: RouteItemService;

  constructor(service: RouteItemService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
