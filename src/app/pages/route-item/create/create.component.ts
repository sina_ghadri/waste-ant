import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RouteItemService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RouteItemAddDto } from 'src/app/models';

@Component({
    selector: 'app-route-item-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RouteItemCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:RouteItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}