import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RouteItemService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { RouteItemGetDto } from 'src/app/models';

@Component({
    selector: 'app-route-item-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class RouteItemDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:RouteItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}