import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RouteItemRoutingModule } from './route-item.routing';
import { RouteItemIndexComponent } from './index/index.component';
import { RouteItemEditComponent } from './edit/edit.component';
import { RouteItemDetailComponent } from './detail/detail.component';
import { RouteItemCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RouteItemRoutingModule,
    ],
    declarations: [
        RouteItemIndexComponent,
        RouteItemCreateComponent,
        RouteItemEditComponent,
        RouteItemDetailComponent,
    ]
})
export class RouteItemModule {}