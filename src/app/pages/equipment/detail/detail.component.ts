import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { EquipmentService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { EquipmentGetDto } from 'src/app/models';

@Component({
    selector: 'app-equipment-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class EquipmentDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:EquipmentService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}