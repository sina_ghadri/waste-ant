import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { EquipmentService } from "../../../services";
import { EquipmentGetDto, EquipmentAddDto, EquipmentChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-equipment-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class EquipmentIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: EquipmentService;

  constructor(service: EquipmentService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
