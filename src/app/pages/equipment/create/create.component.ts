import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { EquipmentService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { EquipmentAddDto } from 'src/app/models';

@Component({
    selector: 'app-equipment-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class EquipmentCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:EquipmentService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}