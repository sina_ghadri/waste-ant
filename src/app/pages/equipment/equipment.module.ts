import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { EquipmentRoutingModule } from './equipment.routing';
import { EquipmentIndexComponent } from './index/index.component';
import { EquipmentEditComponent } from './edit/edit.component';
import { EquipmentDetailComponent } from './detail/detail.component';
import { EquipmentCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        EquipmentRoutingModule,
        LayoutTableModule,
    ],
    declarations: [
        EquipmentIndexComponent,
        EquipmentCreateComponent,
        EquipmentEditComponent,
        EquipmentDetailComponent,
    ]
})
export class EquipmentModule {}