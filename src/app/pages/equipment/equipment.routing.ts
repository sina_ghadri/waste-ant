import { RouterModule } from '@angular/router';
import { EquipmentIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: EquipmentIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class EquipmentRoutingModule {}