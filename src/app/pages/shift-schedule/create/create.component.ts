import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ShiftScheduleService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ShiftScheduleAddDto } from 'src/app/models';

@Component({
    selector: 'app-shift-schedule-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ShiftScheduleCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:ShiftScheduleService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}