import { Component, OnInit } from "@angular/core";
import { BaseTreeLayoutComponent } from 'src/app/libs/bitspco/components/base/tree.layout.component';
import { PlaceService } from 'src/app/services';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
    selector: 'app-shift-schedule',
    templateUrl: './shift-schedule.component.html',
    styleUrls: ['./shift-schedule.component.scss']
})
export class ShiftScheduleComponent extends BaseTreeLayoutComponent implements OnInit {
    private currentService: PlaceService;
  
    constructor(service: PlaceService, formBuilder: RxFormBuilder) {
      super(service, formBuilder);
      this.currentService = service;
    }
  
    ngOnInit(): void {
      this.load();
    }
}