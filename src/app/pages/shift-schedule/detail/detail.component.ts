import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ShiftScheduleService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { ShiftScheduleGetDto } from 'src/app/models';

@Component({
    selector: 'app-shift-schedule-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class ShiftScheduleDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:ShiftScheduleService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}