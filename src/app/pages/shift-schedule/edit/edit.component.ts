import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ShiftScheduleService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ShiftScheduleChangeDto } from 'src/app/models';

@Component({
    selector: 'app-shift-schedule-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ShiftScheduleEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:ShiftScheduleService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}