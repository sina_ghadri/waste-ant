import { Component, OnInit, Input, ViewChild, TemplateRef, OnChanges, SimpleChanges } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { ShiftScheduleService, RoleService, CalendarService, ShiftService } from "../../../services";
import { RoleGetDto, JDate, CalendarGetDto, ShiftScheduleAddDto, ShiftGetDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { Calendar, CalendarDay, CalendarDayItem, Time, CalendarLine } from '../models';

@Component({
  selector: "app-shift-schedule-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ShiftScheduleIndexComponent extends BaseTableLayoutComponent implements OnInit, OnChanges {

  @Input() placeId: number;

  calendars: CalendarGetDto[];
  roles: RoleGetDto[] = [];
  shifts: ShiftGetDto[];

  roleId: number;
  shiftId: number;
  year: number;
  month: number;

  jcalendar: Calendar;
  lines: CalendarLine[] = [];

  selectedItem: CalendarDayItem;
  selectedDay: CalendarDay;
  isStartTime: boolean;

  shiftModal: NzModalRef;
  @ViewChild('shiftModalTpl', { static: false }) shiftModalTpl: TemplateRef<any>;

  constructor(
    private roleService: RoleService,
    private calendarService: CalendarService,
    private shiftService: ShiftService,
    private shiftScheduleService: ShiftScheduleService,
    modalService: NzModalService,
    formBuilder: RxFormBuilder
  ) {
    super(shiftScheduleService, formBuilder);
    this.modalService = modalService;
  }

  ngOnInit(): void {
    this.load();

    this.lines.push(new CalendarLine(new Time(3)));
    this.lines.push(new CalendarLine(new Time(6)));
    this.lines.push(new CalendarLine(new Time(9)));
    this.lines.push(new CalendarLine(new Time(12)));
    this.lines.push(new CalendarLine(new Time(15)));
    this.lines.push(new CalendarLine(new Time(18)));
    this.lines.push(new CalendarLine(new Time(21)));

    this.shiftService.getAll().subscribe(op => this.shifts = op.Data);
    this.roleService.getAll().subscribe(op => this.roles = op.Data);

    const jdate = JDate.now;
    this.year = jdate.getYear();
    this.month = jdate.getMonth();

    this.loadDates();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.placeId) {
      this.onchange();
    }
  }
  getShiftsByDay(): ShiftGetDto[] {
    const calendar = this.calendars.find(x => x.Date.substr(0, 10) == this.selectedDay.value);
    return (calendar && calendar.DayTypeId ? this.shifts.filter(x => x.DayTypeId == calendar.DayTypeId) : []);
  }
  loadDates(callbak?: () => void) {
    if (!this.year || !this.month) return;

    const date = JDate.parseJDate(`${this.year}/${this.month}/01`, 'YYYY/MM/DD');
    const from = JDate.format(date.toDate(), 'YYYY-MM-DD');
    const to = JDate.format(date.addMonth(1).addDays(-1).toDate(), 'YYYY-MM-DD');

    this.calendarService.getAllByTimeRange(from, to).subscribe(op => {
      this.calendars = op.Data.sort((x1, x2) => (x1.Date > x2.Date ? 1 : (x1.Date < x2.Date ? -1 : 0)));
      let jcalendar = new Calendar;
      for (let i = 0; i < this.calendars.length; i++) {
        const calendar = this.calendars[i];
        let day = new CalendarDay;
        day.calendarId = calendar.Id;
        day.value = calendar.Date.substr(0, 10);
        day.text = JDate.toJDate(JDate.parseDate(calendar.Date, 'YYYY/MM/DD')).format('YYYY/MM/DD');
        jcalendar.days.push(day);
      }
      this.jcalendar = jcalendar;
      if (callbak) callbak();
    });
  }
  loadShifts() {
    if (!this.roleId || !this.placeId || !this.year || !this.month) return;

    const date = JDate.parseJDate(`${this.year}/${this.month}/01`, 'YYYY/MM/DD');
    const from = JDate.format(date.toDate(), 'YYYY-MM-DD');
    const to = JDate.format(date.addMonth(1).addDays(-1).toDate(), 'YYYY-MM-DD');

    this.shiftScheduleService.getAllByRange(this.roleId, this.placeId, from, to).subscribe(op => {
      for (let i = 0; i < this.jcalendar.days.length; i++) {
        const day = this.jcalendar.days[i];
        let items = op.Data.filter(x => x.CalendarId == day.calendarId).sort((x1, x2) => (x1.Calendar.Date > x2.Calendar.Date ? 1 : (x1.Calendar.Date < x2.Calendar.Date ? -1 : 0)));
        for (let i = 0; i < items.length; i++) {
          const item = items[i];
          const dayItem = new CalendarDayItem(Time.getFromString(item.StartTime), Time.getFromString(item.EndTime), item.Shift.Name, item.Shift.DayType.Color);
          dayItem.shiftScheduleId = item.Id;
          if (day) day.items.push(dayItem);
        }
      }
    })
  }
  onchange() {
    this.loadDates(() => this.loadShifts());
  }
  newShiftStart(day: CalendarDay) {
    this.selectedDay = day;
    this.shiftModal = this.modalService.create({
      nzWidth: '544px',
      nzTitle: 'افزودن شیفت جدید',
      nzContent: this.shiftModalTpl,
      nzFooter: null
    });
  }
  newShift() {
    const calendar = this.calendars.find(x => x.Id == this.selectedDay.calendarId);
    if (calendar) {
      const schedule = new ShiftScheduleAddDto;
      schedule.CalendarId = calendar.Id;
      schedule.ShiftId = this.shiftId;
      schedule.RoleId = this.roleId;
      schedule.PlaceId = this.placeId;
      schedule.StartTime = '09:00:00';
      schedule.EndTime = '12:00:00';
      this.shiftScheduleService.create(schedule).subscribe(op => {
        if (op.Success) {
          const dayItem = new CalendarDayItem(Time.getFromString(schedule.StartTime), Time.getFromString(schedule.EndTime), op.Data.Shift.Name, calendar.DayType.Color);
          dayItem.shiftScheduleId = op.Data.Id;
          this.selectedDay.items.push(dayItem);
          this.shiftModal.destroy();
        }
      });
    }
  }
  getPrecent(element: HTMLElement, e: any): number {
    var rect = element.getBoundingClientRect();
    var x = element.offsetWidth - (e.clientX - rect.left);
    return x * 100 / element.offsetWidth;
  }
  resizeStart(item: CalendarDayItem, isStartTime: boolean) {
    this.selectedItem = item;
    this.isStartTime = isStartTime;
  }
  resizing(item: CalendarDayItem, element: HTMLElement, e: MouseEvent) {
    if (this.selectedItem == item) {
      let time = Time.getFromPercent(this.getPrecent(element, e));
      if (this.isStartTime) {
        if (time.getPercent() < item.endTime.getPercent()) item.startTime = time;
      }
      else {
        if (time.getPercent() > item.startTime.getPercent()) item.endTime = time;
      }
    }
  }
  resizeEnd() {
    if (this.selectedItem) {
      if (this.isStartTime) {
        this.shiftScheduleService.setStartTime(this.selectedItem.shiftScheduleId, this.selectedItem.startTime.toString('hh:mm:ss')).subscribe(op => {

        });
      }
      else {
        this.shiftScheduleService.setEndTime(this.selectedItem.shiftScheduleId, this.selectedItem.endTime.toString('hh:mm:ss')).subscribe(op => {

        });
      }
    }
    this.selectedItem = null;
  }
}
