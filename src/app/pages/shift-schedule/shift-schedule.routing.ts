import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ShiftScheduleComponent } from './shift-schedule.component';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ShiftScheduleComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ShiftScheduleRoutingModule {}