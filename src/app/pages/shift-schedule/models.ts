export class Calendar {
    days: CalendarDay[] = [];
}

export class CalendarDay {
    calendarId: number;
    text: string;
    color: string;
    value: string;
    items: CalendarDayItem[] = [];

    constructor(text: string = '', color: string = '', value: string = '') {
        this.text = text;
        this.color = color;
        this.value = value;
    }
}

export class CalendarDayItem {
    shiftScheduleId: number;
    startTime: Time;
    endTime: Time;
    text: string;
    color: string;

    constructor(startTime: Time, endTime: Time, text: string = '', color: string = '') {
        this.startTime = startTime;
        this.endTime = endTime;
        this.text = text;
        this.color = color;
    }

    getStartPercent(): number {
        return this.startTime.getPercent();
    }
    getEndPercent(): number {
        return this.endTime.getPercent();
    }
    getWidthPercent(): number {
        return this.getEndPercent() - this.getStartPercent();
    }
}

export class CalendarLine {
    time: Time;

    constructor(time: Time) {
        this.time = time;
    }

    getPercent(): number {
        return this.time.getPercent();
    }
}

export class Time {
    hour: number = 0;
    minute: number = 0;
    second: number = 0;

    constructor(hour: number = 0, minute: number = 0, second: number = 0) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    getPercent(): number {
        return ((this.hour * 60 + this.minute) * 60 + this.second) * 100 / 86400;
    }
    toString(format: string = 'hh:mm:ss'): string {
        format = format.replace('hh', (this.hour > 9 ? '' : '0') + this.hour);
        format = format.replace('mm', (this.minute > 9 ? '' : '0') + this.minute);
        format = format.replace('ss', (this.second > 9 ? '' : '0') + this.second);
        return format;
    }

    public static getFromPercent(percent: number): Time {
        let value = percent * 86400 / 100;
        let time = new Time;
        time.second = Math.floor(value % 60);
        time.minute = Math.floor((value / 60) % 60);
        time.hour = Math.floor((value /3600) % 60);
        return time;
    }
    public static getFromString(value: string): Time {
        const split = value.split(':');
        const time = new Time;
        if(split.length > 0) time.hour = parseInt(split[0]);
        if(split.length > 1) time.minute = parseInt(split[1]);
        if(split.length > 2) time.second = parseInt(split[2]);
        return time;
    }
}