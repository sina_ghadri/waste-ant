import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ShiftScheduleRoutingModule } from './shift-schedule.routing';
import { ShiftScheduleIndexComponent } from './index/index.component';
import { ShiftScheduleEditComponent } from './edit/edit.component';
import { ShiftScheduleDetailComponent } from './detail/detail.component';
import { ShiftScheduleCreateComponent } from './create/create.component';
import { ShiftScheduleComponent } from './shift-schedule.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';
import { LayoutTreeModule } from 'src/app/layouts/tree/tree.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ShiftScheduleRoutingModule,

        LayoutTableModule,
        LayoutTreeModule,
    ],
    declarations: [
        ShiftScheduleComponent,
        ShiftScheduleIndexComponent,
        ShiftScheduleCreateComponent,
        ShiftScheduleEditComponent,
        ShiftScheduleDetailComponent,
    ]
})
export class ShiftScheduleModule {}