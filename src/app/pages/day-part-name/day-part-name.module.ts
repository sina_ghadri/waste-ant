import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { DayPartNameRoutingModule } from './day-part-name.routing';
import { DayPartNameIndexComponent } from './index/index.component';
import { DayPartNameEditComponent } from './edit/edit.component';
import { DayPartNameDetailComponent } from './detail/detail.component';
import { DayPartNameCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        DayPartNameRoutingModule,
        LayoutTableModule,
    ],
    declarations: [
        DayPartNameIndexComponent,
        DayPartNameCreateComponent,
        DayPartNameEditComponent,
        DayPartNameDetailComponent,
    ]
})
export class DayPartNameModule {}