import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { DayPartNameService } from "../../../services";
import { DayPartNameGetDto, DayPartNameAddDto, DayPartNameChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-day-part-name-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class DayPartNameIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: DayPartNameService;

  constructor(service: DayPartNameService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }
}
