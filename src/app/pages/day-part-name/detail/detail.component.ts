import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartNameService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { DayPartGetDto } from 'src/app/models';

@Component({
    selector: 'app-day-part-name-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DayPartNameDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:DayPartNameService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}