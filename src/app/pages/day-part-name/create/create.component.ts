import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartNameService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { DayPartAddDto, Color } from 'src/app/models';

@Component({
    selector: 'app-day-part-name-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class DayPartNameCreateComponent extends BaseCreateComponent implements OnInit {
    colors: Color[];

    constructor(private currentService:DayPartNameService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.currentService.getColors().subscribe(op => this.colors = op.Data);
    }

}