import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartNameService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { DayPartChangeDto, Color } from 'src/app/models';

@Component({
    selector: 'app-day-part-name-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class DayPartNameEditComponent extends BaseEditComponent implements OnInit {
    colors: Color[];

    constructor(private currentService:DayPartNameService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        this.currentService.getColors().subscribe(op => this.colors = op.Data);
    }

}