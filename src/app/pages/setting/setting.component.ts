import { Component } from "@angular/core";

@Component({
    selector: 'app-setting',
    template: '<router-outlet></router-outlet>'
})
export class SettingComponent {
    
}