import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SettingComponent } from './setting.component';
import { SettingGeneralComponent } from './general/general.component';
import { SettingSecurityComponent } from './security/security.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '', component: SettingComponent, children: [
                    { path: '', pathMatch: 'full', redirectTo: 'general' },
                    { path: 'general', component: SettingGeneralComponent },
                    { path: 'security', component: SettingSecurityComponent },
                ]
            }
        ])
    ],
    exports: [RouterModule],
    declarations: []
})
export class SettingRoutingModule { }