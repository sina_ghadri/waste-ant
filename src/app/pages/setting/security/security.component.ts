import { Component, OnInit } from "@angular/core";
import { SettingService } from "../../../services";
import { BaseFormComponent } from 'src/app/libs/bitspco/components/base/form.component';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { SettingChangeDto } from 'src/app/models';

@Component({
  selector: "app-setting-security",
  templateUrl: "./security.component.html",
  styleUrls: ["./security.component.scss"]
})
export class SettingSecurityComponent extends BaseFormComponent implements OnInit {

  constructor(private service: SettingService, formBuilder: RxFormBuilder) {
    super(formBuilder);
  }

  ngOnInit(): void {
    this.form = this.makeFormGroup(new SettingChangeDto)
  }
}
