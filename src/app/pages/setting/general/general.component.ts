import { Component, OnInit } from "@angular/core";
import { SettingService } from "../../../services";

@Component({
  selector: "app-setting-general",
  templateUrl: "./general.component.html",
  styleUrls: ["./general.component.scss"]
})
export class SettingGeneralComponent implements OnInit {

  constructor(private service: SettingService) {
  }

  ngOnInit(): void {

  }
}
