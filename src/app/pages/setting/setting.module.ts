import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { SettingRoutingModule } from './setting.routing';
import { SettingGeneralComponent } from './general/general.component';
import { SettingSecurityComponent } from './security/security.component';
import { SettingComponent } from './setting.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        SettingRoutingModule,
    ],
    declarations: [
        SettingComponent,
        SettingGeneralComponent,
        SettingSecurityComponent
    ]
})
export class SettingModule {}