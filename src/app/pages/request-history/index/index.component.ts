import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RequestHistoryService } from "../../../services";
import { RequestHistoryGetDto, RequestHistoryAddDto, RequestHistoryChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-request-history-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RequestHistoryIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: RequestHistoryService;

  constructor(service: RequestHistoryService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
