import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RequestHistoryRoutingModule } from './request-history.routing';
import { RequestHistoryIndexComponent } from './index/index.component';
import { RequestHistoryEditComponent } from './edit/edit.component';
import { RequestHistoryDetailComponent } from './detail/detail.component';
import { RequestHistoryCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RequestHistoryRoutingModule,
    ],
    declarations: [
        RequestHistoryIndexComponent,
        RequestHistoryCreateComponent,
        RequestHistoryEditComponent,
        RequestHistoryDetailComponent,
    ]
})
export class RequestHistoryModule {}