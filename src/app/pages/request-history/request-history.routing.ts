import { RouterModule } from '@angular/router';
import { RequestHistoryIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: RequestHistoryIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class RequestHistoryRoutingModule {}