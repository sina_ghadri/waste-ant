import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestHistoryService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { RequestHistoryGetDto } from 'src/app/models';

@Component({
    selector: 'app-request-history-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class RequestHistoryDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:RequestHistoryService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}