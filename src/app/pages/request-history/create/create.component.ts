import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestHistoryService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RequestHistoryAddDto } from 'src/app/models';

@Component({
    selector: 'app-request-history-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RequestHistoryCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:RequestHistoryService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}