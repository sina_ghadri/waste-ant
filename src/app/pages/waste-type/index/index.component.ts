import { Component, OnInit } from "@angular/core";
import { WasteTypeService } from "../../../services";
import { WasteTypeGetDto } from "../../../models";
import { NzModalService, NzTreeNode } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { BaseTreeLayoutComponent } from '../../../libs/bitspco/components/base/tree.layout.component';

@Component({
  selector: "app-waste-type-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class WasteTypeIndexComponent extends BaseTreeLayoutComponent implements OnInit {
  private currentService: WasteTypeService;

  constructor(service: WasteTypeService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

  createTreeNode(item: WasteTypeGetDto): NzTreeNode {
    return new NzTreeNode({
      key: item.Id.toString(),
      title: item.Name,
    });
  }
}
