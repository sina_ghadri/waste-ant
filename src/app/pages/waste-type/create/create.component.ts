import { Component, OnInit, Input } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { WasteTypeService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { WasteTypeAddDto } from 'src/app/models';

@Component({
    selector: 'app-waste-type-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class WasteTypeCreateComponent extends BaseCreateComponent implements OnInit {
    @Input() parentId: number;

    constructor(private currentService:WasteTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    makeFormGroup() {
        return super.makeFormGroup({ ParentId: this.parentId });
    }
}