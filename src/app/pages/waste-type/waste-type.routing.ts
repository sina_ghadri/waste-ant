import { RouterModule } from '@angular/router';
import { WasteTypeIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: WasteTypeIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class WasteTypeRoutingModule {}