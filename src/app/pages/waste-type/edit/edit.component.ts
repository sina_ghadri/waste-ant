import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { WasteTypeService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { WasteTypeChangeDto } from 'src/app/models';

@Component({
    selector: 'app-waste-type-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class WasteTypeEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:WasteTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}