import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { WasteTypeRoutingModule } from './waste-type.routing';
import { WasteTypeIndexComponent } from './index/index.component';
import { WasteTypeEditComponent } from './edit/edit.component';
import { WasteTypeDetailComponent } from './detail/detail.component';
import { WasteTypeCreateComponent } from './create/create.component';
import { LayoutTreeModule } from 'src/app/layouts/tree/tree.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        WasteTypeRoutingModule,
        LayoutTreeModule,
    ],
    declarations: [
        WasteTypeIndexComponent,
        WasteTypeCreateComponent,
        WasteTypeEditComponent,
        WasteTypeDetailComponent,
    ]
})
export class WasteTypeModule {}