import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { WasteTypeService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { WasteTypeGetDto } from 'src/app/models';

@Component({
    selector: 'app-waste-type-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class WasteTypeDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:WasteTypeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}