import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptSwapItemService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ReceiptSwapItemAddDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-swap-item-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ReceiptSwapItemCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:ReceiptSwapItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}