import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ReceiptSwapItemService } from "../../../services";
import { ReceiptSwapItemGetDto, ReceiptSwapItemAddDto, ReceiptSwapItemChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-receipt-swap-item-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ReceiptSwapItemIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ReceiptSwapItemService;

  constructor(service: ReceiptSwapItemService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
