import { RouterModule } from '@angular/router';
import { ReceiptSwapItemIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ReceiptSwapItemIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ReceiptSwapItemRoutingModule {}