import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ReceiptSwapItemRoutingModule } from './receipt-swap-item.routing';
import { ReceiptSwapItemIndexComponent } from './index/index.component';
import { ReceiptSwapItemEditComponent } from './edit/edit.component';
import { ReceiptSwapItemDetailComponent } from './detail/detail.component';
import { ReceiptSwapItemCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ReceiptSwapItemRoutingModule,
    ],
    declarations: [
        ReceiptSwapItemIndexComponent,
        ReceiptSwapItemCreateComponent,
        ReceiptSwapItemEditComponent,
        ReceiptSwapItemDetailComponent,
    ]
})
export class ReceiptSwapItemModule {}