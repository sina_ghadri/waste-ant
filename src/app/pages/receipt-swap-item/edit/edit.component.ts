import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptSwapItemService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ReceiptSwapItemChangeDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-swap-item-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ReceiptSwapItemEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:ReceiptSwapItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}