import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { CleaningAreaEquipmentService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { CleaningAreaEquipmentChangeDto } from 'src/app/models';

@Component({
    selector: 'app-cleaning-area-equipment-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class CleaningAreaEquipmentEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:CleaningAreaEquipmentService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}