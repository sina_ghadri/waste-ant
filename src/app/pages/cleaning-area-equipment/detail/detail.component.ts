import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { CleaningAreaEquipmentService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { CleaningAreaEquipmentGetDto } from 'src/app/models';

@Component({
    selector: 'app-cleaning-area-equipment-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class CleaningAreaEquipmentDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:CleaningAreaEquipmentService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}