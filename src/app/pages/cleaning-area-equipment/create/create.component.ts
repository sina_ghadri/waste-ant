import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { CleaningAreaEquipmentService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { CleaningAreaEquipmentAddDto } from 'src/app/models';

@Component({
    selector: 'app-cleaning-area-equipment-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CleaningAreaEquipmentCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:CleaningAreaEquipmentService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}