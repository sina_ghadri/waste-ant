import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { CleaningAreaEquipmentRoutingModule } from './cleaning-area-equipment.routing';
import { CleaningAreaEquipmentIndexComponent } from './index/index.component';
import { CleaningAreaEquipmentEditComponent } from './edit/edit.component';
import { CleaningAreaEquipmentDetailComponent } from './detail/detail.component';
import { CleaningAreaEquipmentCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        CleaningAreaEquipmentRoutingModule,

        LayoutTableModule,
    ],
    declarations: [
        CleaningAreaEquipmentIndexComponent,
        CleaningAreaEquipmentCreateComponent,
        CleaningAreaEquipmentEditComponent,
        CleaningAreaEquipmentDetailComponent,
    ]
})
export class CleaningAreaEquipmentModule {}