import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { CleaningAreaEquipmentService } from "../../../services";
import { CleaningAreaEquipmentGetDto, CleaningAreaEquipmentAddDto, CleaningAreaEquipmentChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-cleaning-area-equipment-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class CleaningAreaEquipmentIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: CleaningAreaEquipmentService;

  constructor(service: CleaningAreaEquipmentService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
