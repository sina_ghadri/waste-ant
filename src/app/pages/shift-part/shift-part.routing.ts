import { RouterModule } from '@angular/router';
import { ShiftPartIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ShiftPartIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ShiftPartRoutingModule {}