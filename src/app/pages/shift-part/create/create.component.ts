import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ShiftPartService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ShiftPartAddDto } from 'src/app/models';

@Component({
    selector: 'app-shift-part-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ShiftPartCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:ShiftPartService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}