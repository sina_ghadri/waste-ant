import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ShiftPartRoutingModule } from './shift-part.routing';
import { ShiftPartIndexComponent } from './index/index.component';
import { ShiftPartEditComponent } from './edit/edit.component';
import { ShiftPartDetailComponent } from './detail/detail.component';
import { ShiftPartCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ShiftPartRoutingModule,
    ],
    declarations: [
        ShiftPartIndexComponent,
        ShiftPartCreateComponent,
        ShiftPartEditComponent,
        ShiftPartDetailComponent,
    ]
})
export class ShiftPartModule {}