import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ShiftPartService } from "../../../services";
import { ShiftPartGetDto, ShiftPartAddDto, ShiftPartChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-shift-part-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ShiftPartIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ShiftPartService;

  constructor(service: ShiftPartService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
