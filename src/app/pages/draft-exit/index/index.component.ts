import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { DraftExitService } from "../../../services";
import { DraftExitGetDto, DraftExitAddDto, DraftExitChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-draft-exit-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class DraftExitIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: DraftExitService;

  constructor(service: DraftExitService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
