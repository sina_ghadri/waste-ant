import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DraftExitService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { DraftExitAddDto } from 'src/app/models';

@Component({
    selector: 'app-draft-exit-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class DraftExitCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:DraftExitService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}