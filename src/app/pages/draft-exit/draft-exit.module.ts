import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { DraftExitRoutingModule } from './draft-exit.routing';
import { DraftExitIndexComponent } from './index/index.component';
import { DraftExitEditComponent } from './edit/edit.component';
import { DraftExitDetailComponent } from './detail/detail.component';
import { DraftExitCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        DraftExitRoutingModule,
    ],
    declarations: [
        DraftExitIndexComponent,
        DraftExitCreateComponent,
        DraftExitEditComponent,
        DraftExitDetailComponent,
    ]
})
export class DraftExitModule {}