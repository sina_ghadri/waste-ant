import { RouterModule } from '@angular/router';
import { DraftExitIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: DraftExitIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class DraftExitRoutingModule {}