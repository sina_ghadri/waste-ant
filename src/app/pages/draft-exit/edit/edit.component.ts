import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DraftExitService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { DraftExitChangeDto } from 'src/app/models';

@Component({
    selector: 'app-draft-exit-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class DraftExitEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:DraftExitService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}