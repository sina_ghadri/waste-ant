import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { DayPartScheduleItemService } from "../../../services";
import { DayPartScheduleItemGetDto, DayPartScheduleItemAddDto, DayPartScheduleItemChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-role-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class DayPartScheduleItemIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: DayPartScheduleItemService;

  constructor(service: DayPartScheduleItemService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
