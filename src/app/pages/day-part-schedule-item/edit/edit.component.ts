import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartScheduleItemService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { DayPartScheduleItemChangeDto } from 'src/app/models';

@Component({
    selector: 'app-part-schedule-item-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class DayPartScheduleItemEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:DayPartScheduleItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}