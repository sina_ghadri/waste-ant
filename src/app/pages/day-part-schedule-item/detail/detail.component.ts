import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartScheduleItemService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { DayPartScheduleItemGetDto } from 'src/app/models';

@Component({
    selector: 'app-part-schedule-item-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DayPartScheduleItemDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:DayPartScheduleItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}