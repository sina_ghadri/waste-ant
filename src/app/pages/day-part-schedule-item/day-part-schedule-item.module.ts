import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { DayPartScheduleItemRoutingModule } from './day-part-schedule-item.routing';
import { DayPartScheduleItemIndexComponent } from './index/index.component';
import { DayPartScheduleItemEditComponent } from './edit/edit.component';
import { DayPartScheduleItemDetailComponent } from './detail/detail.component';
import { DayPartScheduleItemCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        DayPartScheduleItemRoutingModule,
    ],
    declarations: [
        DayPartScheduleItemIndexComponent,
        DayPartScheduleItemCreateComponent,
        DayPartScheduleItemEditComponent,
        DayPartScheduleItemDetailComponent,
    ]
})
export class DayPartScheduleItemModule {}