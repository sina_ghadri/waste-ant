import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { DayPartScheduleItemService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { DayPartScheduleItemAddDto } from 'src/app/models';

@Component({
    selector: 'app-part-schedule-item-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class DayPartScheduleItemCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:DayPartScheduleItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}