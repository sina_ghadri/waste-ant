import { RouterModule } from '@angular/router';
import { DayPartScheduleItemIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: DayPartScheduleItemIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class DayPartScheduleItemRoutingModule {}