import { RouterModule } from '@angular/router';
import { ReceiptDischargeItemIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ReceiptDischargeItemIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ReceiptDischargeItemRoutingModule {}