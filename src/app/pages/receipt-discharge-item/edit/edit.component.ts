import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptDischargeItemService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ReceiptDischargeItemChangeDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-discharge-item-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ReceiptDischargeItemEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:ReceiptDischargeItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}