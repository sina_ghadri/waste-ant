import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptDischargeItemService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { ReceiptDischargeItemGetDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-discharge-item-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class ReceiptDischargeItemDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:ReceiptDischargeItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}