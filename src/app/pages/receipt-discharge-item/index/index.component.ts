import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ReceiptDischargeItemService } from "../../../services";
import { ReceiptDischargeItemGetDto, ReceiptDischargeItemAddDto, ReceiptDischargeItemChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-receipt-discharge-item-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ReceiptDischargeItemIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ReceiptDischargeItemService;

  constructor(service: ReceiptDischargeItemService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
