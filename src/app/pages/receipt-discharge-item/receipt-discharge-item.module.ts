import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ReceiptDischargeItemRoutingModule } from './receipt-discharge-item.routing';
import { ReceiptDischargeItemIndexComponent } from './index/index.component';
import { ReceiptDischargeItemEditComponent } from './edit/edit.component';
import { ReceiptDischargeItemDetailComponent } from './detail/detail.component';
import { ReceiptDischargeItemCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ReceiptDischargeItemRoutingModule,
    ],
    declarations: [
        ReceiptDischargeItemIndexComponent,
        ReceiptDischargeItemCreateComponent,
        ReceiptDischargeItemEditComponent,
        ReceiptDischargeItemDetailComponent,
    ]
})
export class ReceiptDischargeItemModule {}