import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptDischargeItemService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ReceiptDischargeItemAddDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-discharge-item-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ReceiptDischargeItemCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:ReceiptDischargeItemService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}