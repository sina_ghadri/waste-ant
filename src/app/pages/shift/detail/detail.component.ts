import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ShiftService, ShiftPartService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { ShiftGetDto } from 'src/app/models';
import { TimeSplit } from 'src/app/libs/bitspco/components/time-splitter/models';

@Component({
    selector: 'app-shift-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class ShiftDetailComponent extends BaseDetailComponent implements OnInit {

    timeSplits: TimeSplit[] = [];
    
    constructor(
        private currentService:ShiftService, 
        formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.currentService.getAllPart(this.obj.Id).subscribe(op => {
            const data = op.Data;
            for (let i = 0; i < data.length; i++) {
                const part = data[i];
                const time = new TimeSplit;
                time.text = part.ShiftPartName.Name;
                time.color = part.ShiftPartName.Color;
                time.time = part.EndTime;
                this.timeSplits.push(time);
            }
        })
    }
}