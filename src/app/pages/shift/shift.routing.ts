import { RouterModule } from '@angular/router';
import { ShiftIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ShiftIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ShiftRoutingModule {}