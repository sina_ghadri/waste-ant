import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ShiftRoutingModule } from './shift.routing';
import { ShiftIndexComponent } from './index/index.component';
import { ShiftEditComponent } from './edit/edit.component';
import { ShiftDetailComponent } from './detail/detail.component';
import { ShiftCreateComponent } from './create/create.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';
import { TimeSplitterModule } from 'src/app/libs/bitspco/components/time-splitter/time-splitter.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ShiftRoutingModule,
        LayoutTableModule,
        TimeSplitterModule,
    ],
    declarations: [
        ShiftIndexComponent,
        ShiftCreateComponent,
        ShiftEditComponent,
        ShiftDetailComponent,
    ]
})
export class ShiftModule {}