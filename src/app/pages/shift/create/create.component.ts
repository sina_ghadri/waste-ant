import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { RxFormBuilder, RxFormArray } from '@rxweb/reactive-form-validators';
import { ShiftService, RoleService, DayTypeService, ShiftPartNameService, ConfigurationService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RoleGetDto, DayTypeGetDto, ShiftPartNameGetDto, ShiftPartAddDto, ShiftRoleAddDto } from 'src/app/models';
import { TimeSplit, TimeSpliteEvent } from 'src/app/libs/bitspco/components/time-splitter/models';
import { NzModalService } from 'ng-zorro-antd';
import { FormArray } from '@angular/forms';

@Component({
    selector: 'app-shift-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ShiftCreateComponent extends BaseCreateComponent implements OnInit {
    roles: RoleGetDto[];
    dayTypes: DayTypeGetDto[];

    partName: ShiftPartNameGetDto;
    partNames: ShiftPartNameGetDto[];
    timeSplits: TimeSplit[] = [];

    @ViewChild('split', { static: false }) splitTpl: TemplateRef<any>;

    constructor(
        private currentService: ShiftService,
        private roleService: RoleService,
        private dayTypeService: DayTypeService,
        private modalService: NzModalService,
        private shiftPartNameService: ShiftPartNameService,
        private configurationService: ConfigurationService,
        private rxfb: RxFormBuilder) {
        super(currentService, rxfb);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.roleService.getAll().subscribe(op => this.roles = op.Data.filter(x => x.Module.Symbol == this.configurationService.configuration.symbol));
        this.dayTypeService.getAll().subscribe(op => this.dayTypes = op.Data);
    }

    loadParts() {
        if (!this.partNames) {
            this.shiftPartNameService.getAll().subscribe(op => {
                this.partNames = op.Data;
            })
        }
    }
    onSplit(e: TimeSpliteEvent) {
        this.loadParts();
        const modal = this.modalService.create({
            nzTitle: 'انتخاب نوع قسمت روز',
            nzContent: this.splitTpl,
            nzFooter: [
                {
                    label: 'انصراف',
                    type: 'default',
                    onClick: () => {
                        modal.destroy();
                    }
                },
                {
                    label: 'تایید',
                    type: 'primary',
                    onClick: () => {
                        const data = e.data;
                        data.text = this.partName.Name;
                        data.color = this.partName.Color;
                        data.data = this.partName;
                        e.callback(data);
                        modal.destroy();
                    }
                }
            ]
        })
    }

    submit() {
        const roles = [];
        const parts = [];
        for (let i = 0; i < this.f.RoleIds.length; i++) {
            const id = this.f.RoleIds[i];
            const item = new ShiftRoleAddDto;
            item.RoleId = id;
            roles.push(item);
        }
        const dayType = this.dayTypes.find(x => x.Id == this.f.DayTypeId);
        let preItem: TimeSplit = null;
        for (let i = 0; i < this.timeSplits.length; i++) {
            const item = this.timeSplits[i];
            const part = new ShiftPartAddDto;
            part.ShiftPartNameId = item.data.Id;
            if (i == 0) part.StartTime = dayType.StartTime;
            else part.StartTime = preItem.time;
            part.EndTime = item.time;
            parts.push(part);
            preItem = item;
        }
        this.form.get('Roles').setValue(roles);
        this.form.get('Parts').setValue(parts);
        super.submit();
    }
}