import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UseTypeIndexService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { UseTypeIndexChangeDto } from 'src/app/models';

@Component({
    selector: 'app-use-type-index-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class UseTypeIndexEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:UseTypeIndexService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}