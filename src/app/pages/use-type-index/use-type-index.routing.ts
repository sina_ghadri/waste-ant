import { RouterModule } from '@angular/router';
import { UseTypeIndexIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: UseTypeIndexIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class UseTypeIndexRoutingModule {}