import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UseTypeIndexService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { UseTypeIndexGetDto } from 'src/app/models';

@Component({
    selector: 'app-use-type-index-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class UseTypeIndexDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:UseTypeIndexService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}