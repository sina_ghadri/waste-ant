import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { UseTypeIndexService } from "../../../services";
import { UseTypeIndexGetDto, UseTypeIndexAddDto, UseTypeIndexChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-use-type-index-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class UseTypeIndexIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: UseTypeIndexService;

  constructor(service: UseTypeIndexService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
