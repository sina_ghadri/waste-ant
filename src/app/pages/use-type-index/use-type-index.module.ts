import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { UseTypeIndexRoutingModule } from './use-type-index.routing';
import { UseTypeIndexIndexComponent } from './index/index.component';
import { UseTypeIndexEditComponent } from './edit/edit.component';
import { UseTypeIndexDetailComponent } from './detail/detail.component';
import { UseTypeIndexCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        UseTypeIndexRoutingModule,
    ],
    declarations: [
        UseTypeIndexIndexComponent,
        UseTypeIndexCreateComponent,
        UseTypeIndexEditComponent,
        UseTypeIndexDetailComponent,
    ]
})
export class UseTypeIndexModule {}