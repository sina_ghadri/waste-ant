import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UseTypeIndexService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { UseTypeIndexAddDto } from 'src/app/models';

@Component({
    selector: 'app-use-type-index-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class UseTypeIndexCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:UseTypeIndexService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}