import { RouterModule } from '@angular/router';
import { RequestElevatorIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: RequestElevatorIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class RequestElevatorRoutingModule {}