import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestElevatorService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { RequestElevatorGetDto } from 'src/app/models';

@Component({
    selector: 'app-request-elevator-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class RequestElevatorDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:RequestElevatorService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}