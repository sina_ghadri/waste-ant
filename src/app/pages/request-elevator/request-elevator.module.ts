import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { RequestElevatorRoutingModule } from './request-elevator.routing';
import { RequestElevatorIndexComponent } from './index/index.component';
import { RequestElevatorEditComponent } from './edit/edit.component';
import { RequestElevatorDetailComponent } from './detail/detail.component';
import { RequestElevatorCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        RequestElevatorRoutingModule,
    ],
    declarations: [
        RequestElevatorIndexComponent,
        RequestElevatorCreateComponent,
        RequestElevatorEditComponent,
        RequestElevatorDetailComponent,
    ]
})
export class RequestElevatorModule {}