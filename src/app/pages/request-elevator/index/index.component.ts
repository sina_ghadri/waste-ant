import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RequestElevatorService } from "../../../services";
import { RequestElevatorGetDto, RequestElevatorAddDto, RequestElevatorChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-request-elevator-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class RequestElevatorIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: RequestElevatorService;

  constructor(service: RequestElevatorService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
