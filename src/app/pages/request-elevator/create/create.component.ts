import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { RequestElevatorService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { RequestElevatorAddDto } from 'src/app/models';

@Component({
    selector: 'app-request-elevator-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class RequestElevatorCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:RequestElevatorService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}