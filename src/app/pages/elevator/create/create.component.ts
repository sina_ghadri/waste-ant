import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ElevatorService, PlaceService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ElevatorAddDto, PlaceGetDto } from 'src/app/models';
import { NzModalService, NzModalRef, NzTreeNode } from 'ng-zorro-antd';

@Component({
    selector: 'app-elevator-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ElevatorCreateComponent extends BaseCreateComponent implements OnInit {

    modal: NzModalRef;

    @ViewChild('chooseTpl', { static: false }) chooseTpl: TemplateRef<any>;

    constructor(
        private currentService: ElevatorService,
        private placeService: PlaceService,
        formBuilder: RxFormBuilder, private modalService: NzModalService) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    choose() {
        this.modal = this.modalService.create({
            nzTitle: 'انتخاب محل',
            nzContent: this.chooseTpl,
            nzFooter: null
        })
    }
    chooseComplete(e) {
        if(e) {
            const place: PlaceGetDto = e.data;
            this.form.get('PlaceId').setValue(place.Id);
            this.form.get('Place').get('Id').setValue(place.Id);
            this.form.get('Place').get('Name').setValue(place.Name);
        }
        this.modal.destroy();
    }
}