import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ElevatorService, PlaceService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ElevatorChangeDto, PlaceGetDto } from 'src/app/models';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';

@Component({
    selector: 'app-elevator-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ElevatorEditComponent extends BaseEditComponent implements OnInit {

    modal: NzModalRef;

    @ViewChild('chooseTpl', { static: false }) chooseTpl: TemplateRef<any>;

    constructor(
        private currentService: ElevatorService,
        private placeService: PlaceService,
        formBuilder: RxFormBuilder, private modalService: NzModalService) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    choose() {
        this.modal = this.modalService.create({
            nzTitle: 'انتخاب محل',
            nzContent: this.chooseTpl,
            nzFooter: null
        })
    }
    chooseComplete(e) {
        if(e) {
            const place: PlaceGetDto = e.data;
            this.form.get('PlaceId').setValue(place.Id);
            this.form.get('Place').get('Id').setValue(place.Id);
            this.form.get('Place').get('Name').setValue(place.Name);
        }
        this.modal.destroy();
    }
}