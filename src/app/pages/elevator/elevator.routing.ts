import { RouterModule } from '@angular/router';
import { ElevatorIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ElevatorIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ElevatorRoutingModule {}