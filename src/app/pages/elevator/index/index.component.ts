import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ElevatorService } from "../../../services";
import { ElevatorGetDto, ElevatorAddDto, ElevatorChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-elevator-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ElevatorIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ElevatorService;

  constructor(service: ElevatorService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
