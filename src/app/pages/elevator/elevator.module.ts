import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ElevatorRoutingModule } from './elevator.routing';
import { ElevatorIndexComponent } from './index/index.component';
import { ElevatorEditComponent } from './edit/edit.component';
import { ElevatorDetailComponent } from './detail/detail.component';
import { ElevatorCreateComponent } from './create/create.component';
import { ChoosePlaceModule } from 'src/app/components/choose-place/choose-place.module';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ElevatorRoutingModule,
        LayoutTableModule,
        ChoosePlaceModule,
    ],
    declarations: [
        ElevatorIndexComponent,
        ElevatorCreateComponent,
        ElevatorEditComponent,
        ElevatorDetailComponent,
    ]
})
export class ElevatorModule {}