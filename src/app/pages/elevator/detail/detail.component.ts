import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ElevatorService, PlaceService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { ElevatorGetDto } from 'src/app/models';

@Component({
    selector: 'app-elevator-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class ElevatorDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:ElevatorService,
        private placeService: PlaceService, 
        formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}