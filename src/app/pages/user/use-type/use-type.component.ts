import { Component, OnInit, Input } from "@angular/core";
import { UseTypeService, UserExtendedService } from '../../../services';
import { UseTypeGetDto, UserGetDto, UserUseTypeAddDto, UserUseTypeGetDto } from 'src/app/models';
import { NzTreeNode } from 'ng-zorro-antd';
import { TreeExtension } from 'src/app/libs/bitspco/components/base/tree.extension';

@Component({
  selector: 'app-user-use-type',
  templateUrl: './use-type.component.html',
  styleUrls: ['./use-type.component.scss']
})
export class UserUseTypeComponent implements OnInit {
  @Input() obj: UserGetDto;

  progress: boolean;
  useTypes: UseTypeGetDto[];
  useTypeNodes: NzTreeNode[];

  userUseTypes: UserUseTypeGetDto[];

  treeExt: TreeExtension = new TreeExtension;

  constructor(
    private userService: UserExtendedService,
    private useTypeService: UseTypeService,
  ) {

  }

  ngOnInit(): void {
    this.progress = true;
    this.useTypeService.getAll().subscribe(op => {
      this.useTypes = op.Data;
      this.useTypeNodes = this.treeExt.createTreeNodes(this.useTypes);
      this.userService.getAllUseType(this.obj.Id).subscribe(op => {
        this.userUseTypes = op.Data;
        this.render();
        this.progress = false;
      }, e => this.progress = false);
    }, e => this.progress = false);
  }
  render() {
    const useTypeIds = this.userUseTypes.map(x => x.UseTypeId);
    this.treeExt.foreach(this.useTypeNodes,
      x => {
        x.isChecked = (x.parentNode ? x.parentNode.isChecked : false) || useTypeIds.indexOf(+x.origin.key) > -1;
        x.isDisabled = (x.parentNode ? x.parentNode.isChecked : false);
      },
      x => {
        if (!x.isChecked) x.isHalfChecked = x.children.find(y => y.isChecked || y.isHalfChecked) != null;
      })
  }
  onCheckUseTypeChanged(node: NzTreeNode) {
    if (this.obj) {
      if (node.isChecked) this.addUseType(node);
      else this.removeUseType(node);
    }
  }
  addUseType(node: NzTreeNode) {
    const item = new UserUseTypeAddDto;
    item.UserId = this.obj.Id;
    item.UseTypeId = +node.origin.key;
    this.userService.addUseType(this.obj.Id, item).subscribe(op => {
      if (op.Success) this.userUseTypes.push(op.Data);
      this.render();
    });
  }
  removeUseType(node: NzTreeNode) {
    this.userService.removeUseType(this.obj.Id, +node.origin.key).subscribe(op => {
      if (op.Success) this.userUseTypes.splice(this.userUseTypes.indexOf(this.userUseTypes.find(x => x.Id == op.Data.Id)), 1);
      this.render();
    });
  }
  getUseTypeNodes(): NzTreeNode[] {
    return this.useTypeNodes;
  }
}