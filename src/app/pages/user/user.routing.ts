import { RouterModule } from '@angular/router';
import { UserComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: UserComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class UserRoutingModule {}