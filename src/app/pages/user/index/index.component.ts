import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { UserService } from "../../../services";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { UserGetDto } from 'src/app/models';

@Component({
  selector: "app-use-type-index-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class UserComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: UserService;

  @ViewChild('userPlaceTpl', {static: false}) userPlaceTpl: TemplateRef<any>;

  userUseTypeModal: NzModalRef;
  @ViewChild('userUseTypeTpl', {static: false}) userUseTypeTpl: TemplateRef<any>;

  constructor(service: UserService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.modalService = modalService;
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

  showUserPlace(item: UserGetDto) {
    this.selectedItem = item;
    this.userUseTypeModal = this.modalService.create({
      nzTitle: 'محل فعالیت',
      nzContent: this.userPlaceTpl,
      nzFooter: null
    });
  }

  showUserUseType(item: UserGetDto) {
    this.selectedItem = item;
    this.userUseTypeModal = this.modalService.create({
      nzTitle: 'نوع کاربری',
      nzContent: this.userUseTypeTpl,
      nzFooter: null
    });
  }
}
