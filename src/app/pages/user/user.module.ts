import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { UserRoutingModule } from './user.routing';
import { UserComponent } from './index/index.component';
import { LayoutTableModule } from 'src/app/layouts/table/table.module';
import { UserPlaceComponent } from './place/place.component';
import { UserUseTypeComponent } from './use-type/use-type.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        UserRoutingModule,

        LayoutTableModule,
    ],
    declarations: [
        UserComponent,
        UserPlaceComponent,
        UserUseTypeComponent,
    ]
})
export class UserModule {}