import { Component, OnInit, Input } from "@angular/core";
import { PlaceService, UserExtendedService } from '../../../services';
import { PlaceGetDto, UserGetDto, UserPlaceAddDto, UserPlaceGetDto } from 'src/app/models';
import { NzTreeNode } from 'ng-zorro-antd';
import { TreeExtension } from 'src/app/libs/bitspco/components/base/tree.extension';

@Component({
  selector: 'app-user-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.scss']
})
export class UserPlaceComponent implements OnInit {
  @Input() obj: UserGetDto;

  progress: boolean;
  places: PlaceGetDto[];
  placeNodes: NzTreeNode[];

  userPlaces: UserPlaceGetDto[];

  treeExt: TreeExtension = new TreeExtension;

  constructor(
    private userService: UserExtendedService,
    private placeService: PlaceService,
  ) {

  }

  ngOnInit(): void {
    this.progress = true;
    this.placeService.getAll().subscribe(op => {
      this.places = op.Data;
      this.placeNodes = this.treeExt.createTreeNodes(this.places);
      this.userService.getAllPlace(this.obj.Id).subscribe(op => {
        this.userPlaces = op.Data;
        this.render();
        this.progress = false;
      }, e => this.progress = false);
    }, e => this.progress = false);
  }
  render() {
    const placeIds = this.userPlaces.map(x => x.PlaceId);
    this.treeExt.foreach(this.placeNodes,
      x => {
        x.isChecked = (x.parentNode ? x.parentNode.isChecked : false) || placeIds.indexOf(+x.origin.key) > -1;
        x.isDisabled = (x.parentNode ? x.parentNode.isChecked : false);
      },
      x => {
        if (!x.isChecked) x.isHalfChecked = x.children.find(y => y.isChecked || y.isHalfChecked) != null;
      })
  }
  onCheckPlaceChanged(node: NzTreeNode) {
    if (this.obj) {
      if (node.isChecked) this.addPlace(node);
      else this.removePlace(node);
    }
  }
  addPlace(node: NzTreeNode) {
    const item = new UserPlaceAddDto;
    item.UserId = this.obj.Id;
    item.PlaceId = +node.origin.key;
    this.userService.addPlace(this.obj.Id, item).subscribe(op => {
      if (op.Success) this.userPlaces.push(op.Data);
      this.render();
    });
  }
  removePlace(node: NzTreeNode) {
    this.userService.removePlace(this.obj.Id, +node.origin.key).subscribe(op => {
      if (op.Success) this.userPlaces.splice(this.userPlaces.indexOf(this.userPlaces.find(x => x.Id == op.Data.Id)), 1);
      this.render();
    });
  }
  getPlaceNodes(): NzTreeNode[] {
    return this.placeNodes;
  }
}