import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { PlaceElementService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { PlaceElementAddDto } from 'src/app/models';

@Component({
    selector: 'app-place-element-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class PlaceElementCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:PlaceElementService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}