import { RouterModule } from '@angular/router';
import { PlaceElementIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: PlaceElementIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class PlaceElementRoutingModule {}