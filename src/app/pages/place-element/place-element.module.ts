import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { PlaceElementRoutingModule } from './place-element.routing';
import { PlaceElementIndexComponent } from './index/index.component';
import { PlaceElementEditComponent } from './edit/edit.component';
import { PlaceElementDetailComponent } from './detail/detail.component';
import { PlaceElementCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        PlaceElementRoutingModule,
    ],
    declarations: [
        PlaceElementIndexComponent,
        PlaceElementCreateComponent,
        PlaceElementEditComponent,
        PlaceElementDetailComponent,
    ]
})
export class PlaceElementModule {}