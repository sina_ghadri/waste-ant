import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { PlaceElementService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { PlaceElementGetDto } from 'src/app/models';

@Component({
    selector: 'app-place-element-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class PlaceElementDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:PlaceElementService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}