import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { PlaceElementService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { PlaceElementChangeDto } from 'src/app/models';

@Component({
    selector: 'app-place-element-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class PlaceElementEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:PlaceElementService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}