import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptDischargeService } from '../../../services';
import { BaseEditComponent } from '../../../libs/bitspco/components/base/edit.component';
import { ReceiptDischargeChangeDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-discharge-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class ReceiptDischargeEditComponent extends BaseEditComponent implements OnInit {

    constructor(private currentService:ReceiptDischargeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}