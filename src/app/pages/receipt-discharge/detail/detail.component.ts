import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptDischargeService } from '../../../services';
import { BaseDetailComponent } from '../../../libs/bitspco/components/base/detail.component';
import { ReceiptDischargeGetDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-discharge-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class ReceiptDischargeDetailComponent extends BaseDetailComponent implements OnInit {

    constructor(private currentService:ReceiptDischargeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}