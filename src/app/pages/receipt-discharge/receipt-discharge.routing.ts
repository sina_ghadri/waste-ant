import { RouterModule } from '@angular/router';
import { ReceiptDischargeIndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: ReceiptDischargeIndexComponent }])
    ],
    exports: [RouterModule],
    declarations: []
})
export class ReceiptDischargeRoutingModule {}