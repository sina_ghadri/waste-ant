import { Component, OnInit } from "@angular/core";
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ReceiptDischargeService } from '../../../services';
import { BaseCreateComponent } from "../../../libs/bitspco/components/base/create.component";
import { ReceiptDischargeAddDto } from 'src/app/models';

@Component({
    selector: 'app-receipt-discharge-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class ReceiptDischargeCreateComponent extends BaseCreateComponent implements OnInit {

    constructor(private currentService:ReceiptDischargeService, formBuilder: RxFormBuilder) {
        super(currentService, formBuilder);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

}