import { NgModule } from "@angular/core";
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule } from '@angular/common/http';
import { ReceiptDischargeRoutingModule } from './receipt-discharge.routing';
import { ReceiptDischargeIndexComponent } from './index/index.component';
import { ReceiptDischargeEditComponent } from './edit/edit.component';
import { ReceiptDischargeDetailComponent } from './detail/detail.component';
import { ReceiptDischargeCreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        ReceiptDischargeRoutingModule,
    ],
    declarations: [
        ReceiptDischargeIndexComponent,
        ReceiptDischargeCreateComponent,
        ReceiptDischargeEditComponent,
        ReceiptDischargeDetailComponent,
    ]
})
export class ReceiptDischargeModule {}