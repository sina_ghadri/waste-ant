import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ReceiptDischargeService } from "../../../services";
import { ReceiptDischargeGetDto, ReceiptDischargeAddDto, ReceiptDischargeChangeDto } from "../../../models";
import { BaseTableLayoutComponent } from '../../../libs/bitspco/components/base/table.layout.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-receipt-discharge-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class ReceiptDischargeIndexComponent extends BaseTableLayoutComponent implements OnInit {
  private currentService: ReceiptDischargeService;

  constructor(service: ReceiptDischargeService, modalService: NzModalService, formBuilder: RxFormBuilder) {
    super(service, formBuilder);
    this.currentService = service;
  }

  ngOnInit(): void {
    this.load();
  }

}
