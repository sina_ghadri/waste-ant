import { CanActivate, CanActivateChild, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, Route, UrlSegment, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService, ConfigurationService } from '../../../services';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

    constructor(
        private router: Router,
        private authService: AuthService,
        private configuration: ConfigurationService,
    ) { }

    checkToken(): Observable<boolean> {
        return new Observable(ob => {
            this.authService.getLoginInfo().subscribe(loginInfo => {
                if (loginInfo) ob.next(true);
                else {
                    this.configuration.listen(config => {
                        location.href = config.login;
                    });
                }
            })
        });
    }

    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> {
        return this.checkToken();
    }
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.checkToken();
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.checkToken();
    }
}
