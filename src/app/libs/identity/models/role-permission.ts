import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../../bitspco/models/base';
import { RoleGetDto } from './role';
import { PermissionGetDto } from './permission';

export class RolePermissionGetDto extends BaseGetDto {
    RoleId: number = 0;
    PermissionId: number = 0;

    Role: RoleGetDto;
    Permission: PermissionGetDto;
}
export class RolePermissionAddDto extends BaseAddDto {
    RoleId: number = 0;
    PermissionId: number = 0;
}
export class RolePermissionChangeDto extends BaseChangeDto {
    RoleId: number = 0;
    PermissionId: number = 0;
}