import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../../bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';
import { JDate, DateTime } from '../../bitspco/models/datetime';
import { Gender } from '../../../models/enums';

export class UserGetDto extends BaseGetDto {
    Name: string = '';
    Username: string = '';
    Thumbnail: string = '';

    IsAppAuthenticatorEnable: boolean = false;
    IsContactAuthenticatorEnable: boolean = false;
    IsChangePasswordRequired: boolean = false;
    IsLock: boolean = false;
    IsActive: boolean = false;
    IsOnline: boolean = false;
    IsOnlineManually: boolean = true;

    ExpireTime: string = '';
    LastPasswordChange: string = '';
    Timeout: number = 0;
    MaxTokenCount: number = 0;

    More: string;
    
    @prop()
    get JExpireTime(): string { return this.ExpireTime ? JDate.toJDate(JDate.parseDate(this.ExpireTime, 'YYYY-MM-DD')).format('YYYY-MM-DD') : ''; }
    set JExpireTime(value: string) { this.ExpireTime = DateTime.format(JDate.parse(value, 'YYYY-MM-DD').toDate(), 'YYYY-MM-DD'); }
}
export class UserGetInterfaceDto extends BaseGetDto {
    Name: string = '';
    NationalCode?: string = '';
    Telephone?: string = '';
    Mobile?: string = '';
    Picture?: string = '';
    BrandName?: string = '';
    BrandLogo?: string = '';
}
export class UserAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    Username: string = '';
    @prop()
    Password: string = '';
    @prop()
    RePassword: string = '';
    @prop()
    Thumbnail: string = '';
    @prop()
    ExpireTime: string = '';
    @prop()
    Timeout: number = 30;
    @prop()
    MaxTokenCount: number = 1;
    @prop()
    IsChangePasswordRequired: boolean = true;

    @prop()
    get JExpireTime(): string { return this.ExpireTime ? JDate.toJDate(JDate.parseDate(this.ExpireTime, 'YYYY-MM-DD')).format('YYYY-MM-DD') : ''; }
    set JExpireTime(value: string) { this.ExpireTime = DateTime.format(JDate.parse(value, 'YYYY-MM-DD').toDate(), 'YYYY-MM-DD'); }
}
export class UserChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    Username: string = '';
    @prop()
    Thumbnail: string = '';
    @prop()
    ExpireTime: string = '';
    @prop()
    Timeout: number = 30;
    @prop()
    MaxTokenCount: number = 1;
    @prop()
    IsChangePasswordRequired: boolean = true;

    @prop()
    get JExpireTime(): string { return this.ExpireTime ? JDate.toJDate(JDate.parseDate(this.ExpireTime, 'YYYY-MM-DD')).format('YYYY-MM-DD') : ''; }
    set JExpireTime(value: string) { this.ExpireTime = DateTime.format(JDate.parse(value, 'YYYY-MM-DD').toDate(), 'YYYY-MM-DD'); }
}
export class UserChangeProfileDto extends BaseChangeDto {
    Name: string;
    Thumbnail: string;

    More: string;
}