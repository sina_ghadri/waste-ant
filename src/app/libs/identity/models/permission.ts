import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../../bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';

export class PermissionGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    Symbol: string = '';
    @prop()
    IsActive: boolean = true;
    @prop()
    ParentId?: number = 0;

    Parent: PermissionGetDto;
}
export class PermissionAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    Symbol: string = '';
    @prop()
    IsActive: boolean = true;
    @prop()
    ParentId?: number = 0;
}
export class PermissionChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    Symbol: string = '';
    @prop()
    IsActive: boolean = true;
    @prop()
    ParentId?: number = 0;
}