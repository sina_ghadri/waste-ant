import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../../bitspco/models/base';
import { UserGetDto } from './user';
import { RoleGetDto } from './role';

export class UserRoleGetDto extends BaseGetDto {
    UserId: number = 0;
    RoleId: number = 0;

    User: UserGetDto;
    Role: RoleGetDto;
}
export class UserRoleAddDto extends BaseAddDto {
    UserId: number = 0;
    RoleId: number = 0;
}
export class UserRoleChangeDto extends BaseChangeDto {
    UserId: number = 0;
    RoleId: number = 0;
}