import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../../bitspco/models/base';
import { TokenStatus } from '../../../models/enums';
import { UserGetDto } from './user';
import { JDate } from '../../bitspco/models';

export class TokenGetDto extends BaseGetDto {
    UserId: number = 0;

    Ip: string = '';
    Url: string = '';
    Method: string = '';
    Host: string = '';
    Origin: string = '';
    Referer: string = '';
    CacheControl: string = '';
    RequestTime: string = '';
    ISP: string = '';
    Organization: string = '';
    ZipCode: string = '';
    TimeZone: string = '';
    Longitude: string = '';
    Latitude: string = '';
    Currency: string = '';
    CountryFlag: string = '';
    CountryName: string = '';
    CountryCode: string = '';
    ContinentName: string = '';
    ContinentCode: string = '';
    ConnectionType: string = '';
    City: string = '';
    Agent: string = '';
    UserAgentFamily: string = '';
    UserAgentMajor: string = '';
    UserAgentMinor: string = '';
    UserAgentPatch: string = '';
    DeviceBrand: string = '';
    DeviceFamily: string = '';
    DeviceModel: string = '';
    OSFamily: string = '';
    OSMajor: string = '';
    OSMinor: string = '';
    OSPatch: string = '';
    OSPatchMinor: string = '';

    ExpireDescription: string = '';
    ExpireTime: string = '';
    IsNeedVerfication: boolean = false;
    ValidationCode?: number = 0;
    VerficationTime: string = '';
    LastRequestTime: string = '';
    Status: TokenStatus;

    User: UserGetDto;

    get JCreationTime(): string { return this.CreationTime ? JDate.toJDate(JDate.parseDate(this.CreationTime, 'YYYY/MM/DD hh:mm:ss')).format('YYYY/MM/DD hh:mm:ss') : ''; }
    get JExpireTime(): string { return this.ExpireTime ? JDate.toJDate(JDate.parseDate(this.ExpireTime, 'YYYY/MM/DD hh:mm:ss')).format('YYYY/MM/DD hh:mm:ss') : ''; }
    get JLastRequestTime(): string { return this.LastRequestTime ? JDate.toJDate(JDate.parseDate(this.LastRequestTime, 'YYYY/MM/DD hh:mm:ss')).format('YYYY/MM/DD hh:mm:ss') : ''; }

    getStatusHtml(): string {
        if (this.Status == TokenStatus.Valid) return '<span class="text-green">فعال</span>';
        if (this.Status == TokenStatus.Refresh) return '<span class="text-blue">نیازمند بازسازی</span>';
        if (this.Status == TokenStatus.Expired) return '<span class="text-red">منقضی شده</span>';
        return '<span class="text-gray">نامشخص</span>'
    }
}
export class TokenAddDto extends BaseAddDto {

}
export class TokenChangeDto extends BaseChangeDto {

}