import { BaseGetDto } from '../../bitspco/models/base';

export class ModuleGetDto extends BaseGetDto {
    Name: string;
    Symbol: string;
    Icon: string;
    Link: string;

    Roles: string[];
    Permissions: string[];
    Claims: any;
}