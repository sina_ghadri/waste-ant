import { UserGetDto } from './user';
import { ModuleGetDto } from './module';

export class LoginRequestDto {
    Username: string = '';
    Password: string = '';
}

export class LoginResponseDto {
    User: UserGetDto;
    Modules: ModuleGetDto[];

    CreationTime: string;
}