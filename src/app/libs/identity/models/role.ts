import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../../bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';
import { ModuleGetDto } from './module';

export class RoleGetDto extends BaseGetDto {
    @prop()
    ModuleId: number;
    @prop()
    Name: string = '';
    @prop()
    Symbol: string = '';
    @prop()
    IsActive: boolean = false;
    @prop()
    ParentId?: number = 0;

    Parent: RoleGetDto;
    Module: ModuleGetDto;
}
export class RoleAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    Symbol: string = '';
    @prop()
    IsActive: boolean = false;
    @prop()
    ParentId?: number = null;
}
export class RoleChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    Symbol: string = '';
    @prop()
    IsActive: boolean = false;
    @prop()
    ParentId?: number = 0;
}