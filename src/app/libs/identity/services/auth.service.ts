import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginRequestDto, LoginResponseDto } from '../models/auth';
import { Observable } from 'rxjs';
import { ConfigurationService } from '../../bitspco/services/configuration.service';
import { OperationResultType, OperationResult } from '../../bitspco/models';
import { CookieService } from 'ngx-cookie-service';
import { HttpConfigService } from '../../bitspco/services/http.service';
import { Md5 } from 'ts-md5/dist/md5';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public get Token(): string { return this.cookieService.get('Token')  }
  public set Token(value: string) {
    if (value) this.cookieService.set('Token', value);
    else {
      localStorage.removeItem("LoginInfo")
      localStorage.removeItem("Token_Hash");
      this.cookieService.delete('Token', '/', '.' + location.hostname);
    }
  }

  constructor(
    private http: HttpClient,
    private configurationService: ConfigurationService,
    private cookieService: CookieService,
    private httpConfig: HttpConfigService
  ) {
    this.httpConfig.token = this.Token;
  }

  getLoginInfo(reload: boolean = false): Observable<LoginResponseDto> {
    if (!reload) {
      const value =  '{"User":{"Name":"سجاد سلیم زاده","Username":"admin","Thumbnail":"http://94.182.191.77/api/drive/Files/d3548c76-5eb6-4c10-8357-d30aa00301a3/Download","More":"{\\"Firstname\\":\\"سجاد\\",\\"Lastname\\":\\"سلیم زاده\\",\\"Thumbnail\\":\\"http://94.182.191.77/api/drive/Files/d3548c76-5eb6-4c10-8357-d30aa00301a3/Download\\",\\"StartTime\\":\\"1398/09/06\\",\\"EndTime\\":\\"\\",\\"NationalCode\\":\\"0019158483\\",\\"Telephone\\":\\"02144962184\\",\\"Cellphone\\":\\"09192070538\\",\\"BrandName\\":\\"البرز\\",\\"BrandLogo\\":\\"http://94.182.191.77/api/drive/Files/6d49412d-8eae-4f56-8a56-698af57e0025/Download\\"}","IsAppAuthenticatorEnable":false,"IsContactAuthenticatorEnable":false,"IsChangePasswordRequired":true,"IsLock":false,"IsActive":true,"IsOnline":true,"IsOnlineManually":true,"ExpireTime":null,"LastPasswordChange":null,"Timeout":500,"MaxTokenCount":1,"Id":1,"CreatorUserId":null,"CreationTime":"2019-11-27T01:02:32.6370394","LastModifierUserId":1,"LastModificationTime":"2019-12-10T19:21:15.1824515"},"Modules":[{"Id":1,"Name":"مدیریت کاربران","Symbol":"Identity","Icon":"http://94.182.191.77/identity/assets/images/logo.png","Link":"/identity","Permissions":["Auth","Auth_Get","Auth_Remove","UserContact_Get","UserClaim_Get","UserApp_Get","User_Get","Token_Get","ThirdPartyAppAccess_Get","ThirdPartyApp_Get","ThirdPartyAccess_Get","UserQuestion_Get","Setting_Get","Role_Get","Question_Get","Permission_Get","Module_Get","Event_Get","Claim_Get","AuthenticatorApp_Get","RolePermission_Get","UserRole_Get"],"Roles":["Admin","Module"],"Claims":{"dfgdg":"10"}},{"Id":2,"Name":"مدیریت پسماند","Symbol":"WasteManagement","Icon":"http://94.182.191.77/wastemanagement/assets/images/logo.png","Link":"/wastemanagement","Permissions":[],"Roles":["Admin"],"Claims":{}},{"Id":3,"Name":"مدیریت فایل ها","Symbol":"Drive","Icon":"http://icons.iconarchive.com/icons/marcus-roberto/google-play/512/Google-Drive-icon.png","Link":null,"Permissions":["Label_Get","File_Get_Self","File_Remove_Self","Directory_Add_Self","File_Rename","File_Move","Label","File_Star","File_Upload"],"Roles":["Admin","Uploader"],"Claims":{"StorageId":"1"}}],"CreationTime":"2019-12-14T23:54:03.345236+03:30"}';
      let md5 = new Md5();
      if (value) {
        let token_hash =  'e0b6dd96fa536f3a1a70c499a1b34bc0';
        if (md5.appendStr(this.Token).end() == token_hash)
          return new Observable(ob => ob.next(JSON.parse(value)));
      }
    }
    return new Observable(ob => {
      this.configurationService.listen(config => {
        this.http.get<OperationResultType<LoginResponseDto>>(config.identity.auth).subscribe(op => {
          let md5 = new Md5();
          localStorage.setItem("LoginInfo", JSON.stringify(op.Data));
          localStorage.setItem("Token_Hash", md5.appendStr(this.Token).end() as string)
          ob.next(op.Data);
        });
      })
    });
  }
  login(obj: LoginRequestDto): Observable<OperationResultType<LoginResponseDto>> {
    return this.http.post<OperationResultType<LoginResponseDto>>(this.configurationService.configuration.identity.auth, obj);
  }
  logout(): Observable<OperationResult> {
    return new Observable(ob => {
      const op = new OperationResult;
      this.Token = null;
      op.Success = true;
      ob.next(op);
      return;
      this.http.delete<OperationResult>(this.configurationService.configuration.identity.Auth).subscribe(op => {
        if (op.Success) ob.next(op);
      })
    })
  }
}
