import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../../bitspco/services/configuration.service';
import { TokenGetDto, TokenAddDto, TokenChangeDto, OperationResultType } from '../../../models';
import { ICrudService } from '../../bitspco/interfaces/crud-service';
import { BaseCrudService } from '../../bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class TokenService extends BaseCrudService<TokenGetDto, TokenAddDto, TokenChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.identity.token);
        this.configureMakers(TokenGetDto, TokenAddDto, TokenChangeDto);
    }
    getAllLast10(): Observable<OperationResultType<TokenGetDto[]>> {
        return this.http.get<OperationResultType<TokenGetDto[]>>(this.baseUrl + `Last10`).map(x => this.mapper(x));
    }
}