import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { OperationResultType } from '../../bitspco/models';
import { ConfigurationService } from '../../bitspco/services/configuration.service';
import { UserGetDto, UserGetInterfaceDto, UserAddDto, UserChangeDto, UserChangeProfileDto } from '../models';
import { ICrudService } from '../../bitspco/interfaces/crud-service';
import { BaseCrudService } from '../../bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class UserService extends BaseCrudService<UserGetDto, UserAddDto, UserChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.identity.user);
        this.configureMakers(UserGetDto, UserAddDto, UserChangeDto);
    }
    changeProfile(obj: UserChangeProfileDto): Observable<OperationResultType<UserGetDto>> {
        return this.http.patch<OperationResultType<UserGetDto>>(this.baseUrl, obj);
    }
    active(id: number): Observable<OperationResultType<UserGetDto>> {
        return this.http.put<OperationResultType<UserGetDto>>(this.baseUrl + id + '/Active', {});
    }
    deactive(id: number): Observable<OperationResultType<UserGetDto>> {
        return this.http.put<OperationResultType<UserGetDto>>(this.baseUrl + id + '/Deactive', {});
    }
}