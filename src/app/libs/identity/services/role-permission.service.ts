import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../../bitspco/services/configuration.service';
import { RolePermissionGetDto, RolePermissionAddDto, RolePermissionChangeDto } from '../models';
import { ICrudService } from '../../bitspco/interfaces/crud-service';
import { BaseCrudService } from '../../bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class RolePermissionService extends BaseCrudService<RolePermissionGetDto, RolePermissionAddDto, RolePermissionChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.identity.rolePermission);
        this.configureMakers(RolePermissionGetDto, RolePermissionAddDto, RolePermissionChangeDto);
    }
}