import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { OperationResultType } from '../../bitspco/models';
import { ConfigurationService } from '../../bitspco/services/configuration.service';
import { RoleGetDto, RoleAddDto, RoleChangeDto, RolePermissionGetDto, RolePermissionAddDto } from '../models';
import { ICrudService } from '../../bitspco/interfaces/crud-service';
import { BaseCrudService } from '../../bitspco/services/base-crud.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class RoleService extends BaseCrudService<RoleGetDto, RoleAddDto, RoleChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.identity.role);
        this.configureMakers(RoleGetDto, RoleAddDto, RoleChangeDto);
    }
    active(id: number): Observable<OperationResultType<RoleGetDto>> {
        return this.http.put<OperationResultType<RoleGetDto>>(this.baseUrl + id + '/Active', {});
    }
    deactive(id: number): Observable<OperationResultType<RoleGetDto>> {
        return this.http.put<OperationResultType<RoleGetDto>>(this.baseUrl + id + '/Deactive', {});
    }

    getPermissions(id: number): Observable<OperationResultType<RolePermissionGetDto[]>> {
        return this.http.get<OperationResultType<RolePermissionGetDto[]>>(this.baseUrl + `${id}/Permissions`);
    }
    addPermission(id: number, role_permission: RolePermissionAddDto): Observable<OperationResultType<RolePermissionGetDto[]>> {
        return this.http.post<OperationResultType<RolePermissionGetDto[]>>(this.baseUrl + `${id}/Permissions`, role_permission);
    }
    removePermission(id: number, permissionId: number): Observable<OperationResultType<RolePermissionGetDto[]>> {
        return this.http.delete<OperationResultType<RolePermissionGetDto[]>>(this.baseUrl + `${id}/Permissions/${permissionId}`);
    }
}