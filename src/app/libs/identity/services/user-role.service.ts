import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../../bitspco/services/configuration.service';
import { UserRoleGetDto, UserRoleAddDto, UserRoleChangeDto } from '../../../models';
import { ICrudService } from '../../bitspco/interfaces/crud-service';
import { BaseCrudService } from '../../bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class UserRoleService extends BaseCrudService<UserRoleGetDto, UserRoleAddDto, UserRoleChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.identity.userRole);
        this.configureMakers(UserRoleGetDto, UserRoleAddDto, UserRoleChangeDto);
    }
}