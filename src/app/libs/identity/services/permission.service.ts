import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../../bitspco/services/configuration.service';
import { PermissionGetDto, PermissionAddDto, PermissionChangeDto } from '../models';
import { ICrudService } from '../../bitspco/interfaces/crud-service';
import { BaseCrudService } from '../../bitspco/services/base-crud.service';

@Injectable({providedIn: 'root'})
export class PermissionService extends BaseCrudService<PermissionGetDto, PermissionAddDto, PermissionChangeDto> implements ICrudService {

    constructor(http: HttpClient, configService: ConfigurationService) { 
        super(http, configService.configuration.identity.permission);
        this.configureMakers(PermissionGetDto, PermissionAddDto, PermissionChangeDto);
    }
}