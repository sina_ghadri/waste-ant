import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ExplorerComponent } from './components/explorer/explorer.component';
import { SharedComponent } from './components/shared/shared.component';
import { RecentComponent } from './components/recent/recent.component';
import { UploadComponent } from './components/upload/upload.component';
import { UploaderComponent } from './uploader.component';
import { UploaderService } from './services/uploader.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule
    ],
    declarations: [
        UploaderComponent,
        ExplorerComponent,
        SharedComponent,
        RecentComponent,
        UploadComponent,
    ],
    exports: [
        UploaderComponent
    ],
    providers: [
    ],
})
export class UploaderModule {}