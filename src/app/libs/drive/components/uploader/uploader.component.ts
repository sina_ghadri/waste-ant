import { Component, OnInit, Input, EventEmitter, Output, ViewEncapsulation } from "@angular/core";
import { UploaderService } from "./services/uploader.service";
import { Uploader } from "./models/uploader";

@Component({
  selector: "app-uploader",
  templateUrl: "./uploader.component.html",
  styleUrls: ["./uploader.component.scss"],
})
export class UploaderComponent implements OnInit {
  @Input() uploader: Uploader;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter

  get tab(): string {
    return this.uploader.tab;
  }
  set tab(value: string) {
    this.uploader.tab = value;
  }

  ngOnInit(): void { }

  oninserting() {
    if (this.uploader.selectedFile) {
      this.oncomplete.emit(this.uploader.service.getDownloadLink(this.uploader.selectedFile));
    }
  }
  cancel() {
    this.oncomplete.emit();
  }
}
