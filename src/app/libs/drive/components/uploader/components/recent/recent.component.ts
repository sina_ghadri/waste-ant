import { Component, OnInit, Input } from "@angular/core";
import { UploaderService } from '../../services/uploader.service';
import { Uploader } from '../../models/uploader';

@Component({
    selector: 'app-recent',
    templateUrl: './recent.component.html',
    styleUrls: ['./recent.component.scss']
})
export class RecentComponent implements OnInit {
  @Input() uploader: Uploader;

    ngOnInit(): void {
    }
}