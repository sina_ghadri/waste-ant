import { Component, OnInit, Input } from "@angular/core";
import { UploaderService } from "../../services/uploader.service";
import { Uploader } from "../../models/uploader";
import { FileType } from "../../models/file";

class FileInfo {
  progress: number = 0;
  speed: number = 0;
  status: string = "Pending";
  items: number[] = [];
  lastAmount: number = 0;
}

@Component({
  selector: "app-upload",
  templateUrl: "./upload.component.html",
  styleUrls: ["./upload.component.scss"]
})
export class UploadComponent implements OnInit {
  @Input() uploader: Uploader;

  files: FileList;
  fileInfos: FileInfo[] = [];

  ngOnInit(): void {
  }
  getFileTypeClass(file: File): string {
    const items = [
      { key: 'image', value: 'image' },
      { key: 'video', value: 'video' },
      { key: 'doc', value: 'doc' },
      { key: 'excel', value: 'excel' },
      { key: 'slide', value: 'slide' },
      { key: 'html', value: 'html' },
      { key: 'css', value: 'css' },
      { key: 'xml', value: 'xml' },
      { key: 'json', value: 'json' },
      { key: 'typescript', value: 'typescript' },
      { key: 'java', value: 'java' },
      { key: 'python', value: 'python' },
      { key: 'php', value: 'php' },
      { key: 'csharp', value: 'csharp' },
      { key: 'javascript', value: 'javascript' },
      { key: 'text', value: 'text' },
      { key: 'photoshop', value: 'photoshop' },
      { key: 'pdf', value: 'pdf' },
      { key: 'audio', value: 'audio' },
      { key: 'zip', value: 'zip' },
      { key: 'unknown', value: 'unknown' },
    ];
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (file.type.indexOf(item.key) > -1) return item.value;
    }
    return "unknown";
  }
  getFileSizeFormatted(size: number): string {
    const mb = Math.floor(size / (1024 * 1024));
    const kb = Math.floor(size / 1024) % 1024;
    const b = size % 1024;
    if (mb > 0) return (mb + kb / 1024).toFixed(2) + " Mb";
    if (kb > 0) return (kb + b / 1024).toFixed(2) + " Kb";
    return b.toFixed(2) + " Byte";
  }
  getFileInfo(file: File): FileInfo {
    const filename = file.name;
    return this.fileInfos[filename]
      ? this.fileInfos[filename]
      : (this.fileInfos[filename] = new FileInfo());
  }
  handleFileInput(files: FileList) {
    this.files = files;
  }
  startAll() {
    this.startUploadFileByIndex(0, true);
  }
  startUploadFileByIndex(index: number, withContinue: boolean = false) {
    const file = this.files.length > index ? this.files[index] : null;
    if (!file) return;
    if (this.getFileInfo(file).status == "Complete") return;
    if (this.getFileInfo(file).status == "Uploading") return;

    let timer = setInterval(() => {
      const maxCount = 10;
      let items = this.getFileInfo(file).items.map(x => x);
      if (items.length > maxCount) items.shift();
      items.push(this.getFileInfo(file).lastAmount);
      if (items.length > 0) {
        const speed = ((items[items.length - 1] - items[0]) / items.length);
        this.getFileInfo(file).speed = speed;
      }
      this.getFileInfo(file).items = items;
    }, 500);

    this.getFileInfo(file).status = "Uploading";
    this.uploader.service.getCurrentDirectory().subscribe(dir => {
      this.uploader.service
        .uploadFile(dir.Id, file, (done, total, progress) => {
          this.getFileInfo(file).lastAmount = done;
          if (progress !== Infinity) this.getFileInfo(file).progress = progress;
        })
        .subscribe(
          op => {
            clearInterval(timer);
            this.getFileInfo(file).status = "Complete";
            if (withContinue)
              this.startUploadFileByIndex(index + 1, withContinue);
          },
          er => {
            clearInterval(timer);
            this.getFileInfo(file).status = "Failed";
            if (withContinue)
              this.startUploadFileByIndex(index + 1, withContinue);
          }
        );
    });

  }
  stopAll() { }
  removeAll() {
    this.files = undefined;
  }
}
