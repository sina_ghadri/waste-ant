import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from "@angular/core";
import { FileNode, FileType, FileAccessibility } from "../../models/file";
import { UploaderService } from "../../services/uploader.service";
import { Uploader } from '../../models/uploader';
import { Label } from '../../models/label';

@Component({
  selector: "app-explorer",
  templateUrl: "./explorer.component.html",
  styleUrls: ["./explorer.component.scss"]
})
export class ExplorerComponent implements OnInit {
  @Input() uploader: Uploader;

  items: FileNode[];

  get service(): UploaderService { return this.uploader.service; }

  get mode(): string { return this.uploader.explorer.mode; }
  set mode(value: string) { this.uploader.explorer.mode = value; }

  get sort(): string { return this.uploader.explorer.sort; }
  set sort(value: string) { this.uploader.explorer.sort = value; }

  get selectedItem(): FileNode { return this.uploader.selectedItem; }
  set selectedItem(value: FileNode) { this.uploader.selectedItem = value; }

  labels: Label[];

  modal: string;
  breadcrumb: FileNode[] = [];

  mainContextMenuElement: HTMLElement;
  fileContextMenuElement: HTMLElement;
  filesElement: HTMLElement;

  get currentDirectory(): FileNode { if (this.breadcrumb.length > 0) return this.breadcrumb[this.breadcrumb.length - 1]; return null; }

  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    this.filesElement = this.element.nativeElement.querySelector('.files');
    this.fileContextMenuElement = this.element.nativeElement.querySelector('.files .file-contextmenu');
    this.mainContextMenuElement = this.element.nativeElement.querySelector('.files .main-contextmenu');

    this.service.getAllLabels().subscribe(op => this.labels = op.Data);

    this.loadItems();
    this.loadBreadcrumb();
  }
  getPoint(e: MouseEvent, parent: HTMLElement) {
    let rect = parent.getBoundingClientRect();
    const x = e.clientX - rect.left + parent.scrollLeft;
    const y = e.clientY - rect.top + parent.scrollTop;
    return { x, y };
  }
  loadItems() {
    this.items = [];
    this.service.getCurrentDirectory().subscribe(dir => {
      this.service.getFilesByParentId(dir.Id).subscribe(op => {
        this.items = op.Data.sort((x1, x2) => {
          if (x1.Type == FileType.Directory && x2.Type != FileType.Directory) return -1;
          return (x1.Name > x2.Name ? 1 : (x1.Name < x2.Name ? -1 : 0));
        })
      });
    })
  }
  loadBreadcrumb() {
    this.service.getCurrentDirectory().subscribe(dir => {
      this.service.getFileById(dir.Id).subscribe(op => {
        if (op.Success) {
          let file = op.Data.Parent;
          this.breadcrumb = [];
          while (file) {
            this.breadcrumb.unshift(file);
            file = file.Parent;
          }
        }
      })
    })

  }
  getFileTypeClass(file: FileNode): string {
    switch (file.Type) {
      case FileType.Directory: return 'directory';
      case FileType.Document: return 'doc';
      case FileType.Image: return 'image';
      case FileType.Slide: return 'slide';
      case FileType.SpreadSheet: return 'spreadsheet';
      case FileType.Video: return 'video';
      case FileType.Html: return 'html';
      case FileType.Css: return 'css';
      case FileType.Xml: return 'xml';
      case FileType.Json: return 'json';
      case FileType.Typescript: return 'typescript';
      case FileType.Java: return 'java';
      case FileType.Python: return 'python';
      case FileType.Php: return 'php';
      case FileType.CSharp: return 'csharp';
      case FileType.Javascript: return 'javascript';
      case FileType.Text: return 'text';
      case FileType.Photoshop: return 'photoshop';
      case FileType.Pdf: return 'pdf';
      case FileType.Audio: return 'audio';
      case FileType.Zip: return 'zip';
      case FileType.Unknown: return 'unknown';
    }
    return '';
  }
  getFileTypeName(file: FileNode): string {
    return this.getFileTypeClass(file);
  }
  getFileAccessibilityName(file: FileNode): string {
    switch (file.Accessibility) {
      case FileAccessibility.Public: return 'Public';
      case FileAccessibility.Private: return 'Private';
      case FileAccessibility.SpecialUser: return 'SpecialUser';
    }
    return '';
  }
  getFileSizeFormatted(file: FileNode): string {
    const mb = Math.floor(file.Size / (1024 * 1024));
    const kb = Math.floor(file.Size / (1024)) % 1024;
    const b = file.Size % 1024;
    if (mb > 0) return (mb + (kb / 1024)).toFixed(2) + ' Mb';
    if (kb > 0) return (kb + (b / 1024)).toFixed(2) + ' Kb';
    return b.toFixed(2) + " Byte";
  }
  getItems(): FileNode[] {
    if (!this.items) return [];
    let items = this.items.map(x => x);
    if (this.uploader.explorer.search) {
      const search = this.uploader.explorer.search.toLowerCase();
      items = items.filter(x => x.Name.toLowerCase().indexOf(search) > -1);
    }
    return items;
  }
  openContextMenu(e: MouseEvent, element: HTMLElement) {
    let point = this.getPoint(e, this.filesElement);
    element.style.top = point.y + 'px';
    if (point.x < 300) {
      element.classList.add('left');
      element.classList.remove('right');
      element.style.right = 'initial';
      element.style.left = point.x + 'px';
    }
    else {
      element.classList.add('right');
      element.classList.remove('left');
      element.style.right = (this.filesElement.offsetWidth - point.x - 20) + 'px';
      element.style.left = 'initial';
    }
    element.focus();
  }
  openFileContextMenu(e: MouseEvent, file: FileNode) {
    e.preventDefault();
    this.selectedItem = file;
    this.openContextMenu(e, this.fileContextMenuElement);
  }
  openMainContextMenu(e: MouseEvent) {
    e.preventDefault();
    this.openContextMenu(e, this.mainContextMenuElement);
  }
  gotoDirectory(dir: FileNode) {
    let index = this.breadcrumb.indexOf(this.breadcrumb.find(x => x.Id == dir.Id));
    this.breadcrumb.splice(index, this.breadcrumb.length - index);
    this.service.currentDirectory = dir;
    this.loadItems();
  }
  openDirectory(dir: FileNode) {
    this.service.getCurrentDirectory().subscribe(currentDirectory => {
      this.breadcrumb.push(currentDirectory);
      this.service.currentDirectory = dir;
      this.loadItems();
    })
  }
  dblclick(item: FileNode) {
    if (item.Type == FileType.Directory) {
      this.openDirectory(item);
    } else {
      this.preview();
    }
  }
  preview() {
    window.open(this.service.getPreviewLink(this.selectedItem), '_blank');
  }
  share() {
    //this.modal = 'share';
  }
  move() {
    //this.modal = 'move';

  }
  link() {
    //this.modal = 'link';

  }
  getShareLink() {

  }
  setLabel(item: Label) {
    this.service.setLabel(this.selectedItem.Id, item.Id).subscribe(op => {
      if (op.Success) {
        this.selectedItem.Label = op.Data.Label;
        this.selectedItem.LabelId = op.Data.LabelId;
      }
    });
  }
  removeLabel() {
    this.service.removeLabel(this.selectedItem.Id).subscribe(op => {
      if (op.Success) {
        this.selectedItem.Label = null;
        this.selectedItem.LabelId = null;
      }
    });
  }
  toggleStarred() {
    if (this.selectedItem.Starred) {
      this.service.removeStarred(this.selectedItem.Id).subscribe(op => {
        if (op.Success) this.selectedItem.Starred = false;
      })
    }
    else {
      this.service.setStarred(this.selectedItem.Id).subscribe(op => {
        if (op.Success) this.selectedItem.Starred = true;
      })
    }
  }
  rename() {
    var name = prompt('Enter new name : ', this.selectedItem.Name);
    if (name != this.selectedItem.Name) {
      this.service.renameFile(this.selectedItem.Id, name).subscribe(op => {
        if (op.Success) this.selectedItem.Name = op.Data.Name;
      })
    }
  }
  detail() {
    this.modal = 'detail';
  }
  getDownloadLink() {
    return this.service.getDownloadLink(this.selectedItem);
  }
  download() {
    window.open(this.getDownloadLink(), '_blank');
  }
  remove() {
    if(confirm(`آیا از حذف این فایل ((${this.selectedItem.Name})) اطمینان دارید؟`))
    {
      this.service.removeFile(this.selectedItem.Id).subscribe(op => {
        if(op.Success) {
          this.items.splice(this.items.indexOf(this.items.find(x => x.Id == op.Data.Id)),1);
        }
      })
    }
  }
  createDirectory() {
    var name = prompt('Please enter directory name : ');
    this.service.getCurrentDirectory().subscribe(dir => {
      this.service.makeDirectory(dir.Id, name).subscribe(op => {
        this.loadItems();
      })
    })

  }
}
