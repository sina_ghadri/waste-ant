import { Component, OnInit, Input } from "@angular/core";
import { UploaderService } from '../../services/uploader.service';
import { Uploader } from '../../models/uploader';

@Component({
    selector: 'app-shared',
    templateUrl: './shared.component.html',
    styleUrls: ['./shared.component.scss']
})
export class SharedComponent implements OnInit {
    @Input() uploader: Uploader;

    ngOnInit(): void {
    }
}