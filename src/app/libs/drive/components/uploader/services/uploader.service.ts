import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { OperationResult } from "../models/operation-result";
import { HttpClient } from "@angular/common/http";
import { FileNode } from "../models/file";
import { Label } from '../models/label';
import { AuthService } from 'src/app/services';
import { OperationResultType } from 'src/app/models';
import { SpaceGetDto } from '../models/space';

@Injectable({ providedIn: "root" })
export class UploaderService {
  private baseUrl: string;
  private space: SpaceGetDto;
  public currentDirectory: FileNode;

  constructor(private http: HttpClient, private authService: AuthService) { }

  configure(baseUrl: string) {
    this.baseUrl = baseUrl + (baseUrl.substr(baseUrl.length - 1, 1) == "/" ? "" : "/");
  }

  getPreviewLink(file: FileNode): string {
    return this.baseUrl + `Files/${file.Hash}/Preview`;
  }
  getDownloadLink(file: FileNode): string {
    return this.baseUrl + `Files/${file.Hash}/Download`;
  }
  getCurrentDirectory(): Observable<FileNode> {
    if (this.currentDirectory) return new Observable(ob => ob.next(this.currentDirectory));
    else if (this.space) return new Observable(ob => { this.currentDirectory = this.space.Root; ob.next(this.currentDirectory); });
    return new Observable(ob => {
      this.getOwnSpace().subscribe(op => {
        this.space = op.Data;
        this.currentDirectory = this.space.Root;
        ob.next(this.currentDirectory);
      }, err => ob.error(err));
    })
  }
  getOwnSpace(): Observable<OperationResultType<SpaceGetDto>> {
    return this.http.get<OperationResultType<SpaceGetDto>>(this.baseUrl + `Spaces/Own`)
  }
  getFileById(id: number): Observable<OperationResult<FileNode>> {
    return this.http.get<OperationResult<FileNode>>(this.baseUrl + `Files/${id}`);
  }
  getFilesByParentId(id: number): Observable<OperationResult<FileNode[]>> {
    return this.http.get<OperationResult<FileNode[]>>(this.baseUrl + `Files/${id}/Children`);
  }
  makeDirectory(id: number, name: string): Observable<OperationResultType<FileNode>> {
    return this.http.post<OperationResult<FileNode>>(this.baseUrl + `Files/${id}/Directory`, { Name: name });
  }
  getAllLabels(): Observable<OperationResult<Label[]>> {
    return this.http.get<OperationResult<Label[]>>(this.baseUrl + 'Labels');
  }
  setLabel(id: number, labelId: number): Observable<OperationResult<FileNode>> {
    return this.http.put<OperationResult<FileNode>>(this.baseUrl + `Files/${id}/Label/${labelId}`, {});
  }
  removeLabel(id: number): Observable<OperationResult<FileNode>> {
    return this.http.delete<OperationResult<FileNode>>(this.baseUrl + `Files/${id}/Label`);
  }
  setStarred(id: number): Observable<OperationResult<FileNode>> {
    return this.http.put<OperationResult<FileNode>>(this.baseUrl + `Files/${id}/Starred`, {});
  }
  removeStarred(id: number): Observable<OperationResult<FileNode>> {
    return this.http.delete<OperationResult<FileNode>>(this.baseUrl + `Files/${id}/Starred`);
  }
  moveFile(id: number, targetId: number): Observable<OperationResult<FileNode>> {
    return this.http.put<OperationResult<FileNode>>(this.baseUrl + `Files/${id}/Move/${targetId}`, {});
  }
  renameFile(id: number, name: string): Observable<OperationResult<FileNode>> {
    return this.http.put<OperationResult<FileNode>>(this.baseUrl + `Files/${id}/Rename`, { Name: name });
  }
  removeFile(id: number): Observable<OperationResult<FileNode>> {
    return this.http.delete<OperationResult<FileNode>>(this.baseUrl + `Files/${id}`);
  }
  uploadFile(id: number, file: File, onprogress: (done, total, progress) => void): Observable<OperationResult<FileNode>> {
    return new Observable(ob => {
      let data = new FormData();
      data.append("files", file);
      var xhr = new XMLHttpRequest();
      xhr.addEventListener(
        "progress",
        function (e: any) {
          var done = e.position || e.loaded,
            total = e.totalSize || e.total;
          if (onprogress)
            onprogress(done, total, Math.floor((done / total) * 1000) / 10);
        },
        false
      );
      if (xhr.upload) {
        xhr.upload.onprogress = function (e: any) {
          var done = e.position || e.loaded,
            total = e.totalSize || e.total;
          if (onprogress)
            onprogress(done, total, Math.floor((done / total) * 1000) / 10);
        };
      }
      xhr.onreadystatechange = function (e) {
        if (this.readyState == 4) {
          if (this.status == 200) ob.next(JSON.parse(this.responseText));
          else ob.error(this.responseText);
          ob.complete();
        }
      };
      xhr.open("POST", this.baseUrl + `Files/${id}/Upload`, true);
      xhr.setRequestHeader('Authorization', this.authService.Token);
      xhr.send(data);
    })
  }
}
