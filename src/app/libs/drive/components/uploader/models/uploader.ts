import { UploaderService } from "../services/uploader.service";
import { FileNode, FileType } from "./file";

export class Uploader {
  tab: string = 'explorer';
  selectedItem: FileNode;
  get selectedFile(): FileNode {
    if (this.selectedItem.Type == FileType.Directory) return null;
    return this.selectedItem;
  }
  service: UploaderService;

  explorer: UploaderExplorer = new UploaderExplorer();
  shared: UploaderShared = new UploaderShared();
  recent: UploaderRecent = new UploaderRecent();
  uploade: UploaderUpload = new UploaderUpload();
}

export class UploaderExplorer {
  folderId: number;
  sort: string = "Id";
  mode: string = "table";
  search: string;

  toggleMode() {
    if (this.mode == "grid") this.mode = "table";
    else if (this.mode == "table") this.mode = "grid";
  }
}

export class UploaderShared {

}

export class UploaderRecent {

}

export class UploaderUpload {
}