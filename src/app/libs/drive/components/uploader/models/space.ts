import { FileNode } from './file'

export class SpaceGetDto {
    StorageId: number;
    UserId: number;
    Name: string;
    Hash: string;
    Size: number;

    Root:FileNode;
}