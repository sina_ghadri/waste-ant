export class OperationResultCount<T> {
    Code: number;
    Success: boolean;
    Message: string;
    Count: number;
    Data: T;
}
export class OperationResult<T> {
    Code: number;
    Success: boolean;
    Message: string;
    Data: T;
}