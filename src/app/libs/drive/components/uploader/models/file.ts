import { Label } from './label';

export class FileNode {
  Id: number;
  Name: string;
  Symbol: string;
  Hash: string;
  Size: number;
  Starred: boolean;
  LabelId: number;
  Label: Label;
  Type: FileType;
  Accessibility: FileAccessibility;
  Status: FileStatus;
  ParentId?: number;
  Description: string;

  CreationTime: string;
  CreatorUserId?: number;
  LastModificationTime: string;
  ModifierUserId?: number;

  Parent: FileNode;
}
export enum FileAccessibility {
  Private = 1,
  Public = 2,
  SpecialUser = 3
}
export enum FileStatus {
  Active = 1,
  IsDeleted = 2
}
export enum FileType {
  Unknown = 0,
  Directory = 1,
  Video = 2,
  Image = 3,
  SpreadSheet = 4,
  Slide = 5,
  Document = 6,
  Audio = 7,
  Pdf = 8,
  Photoshop = 9,
  Text = 10,
  Html = 11,
  Css = 12,
  Javascript = 13,
  CSharp = 14,
  Php = 15,
  Python = 16,
  Java = 17,
  Typescript = 18,
  Json = 19,
  Xml = 20,
  Zip = 21,
}
