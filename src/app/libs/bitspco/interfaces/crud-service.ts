import { Observable } from "rxjs";
import { ODataQueryOptions } from "../models/odata-query-options";
import {
  BaseGetDto,
  BaseAddDto,
  BaseChangeDto,
  OperationResultCount,
  OperationResultType
} from "../../../models";

export interface IGetService {
  getAll(): Observable<OperationResultCount<BaseGetDto[]>>;
  select(query: ODataQueryOptions): Observable<OperationResultCount<BaseGetDto[]>>;
  getById(id: number): Observable<OperationResultType<BaseGetDto>>;
}
export interface ICreateService {
  create(obj: BaseAddDto): Observable<OperationResultType<BaseGetDto>>;
}
export interface IEditService {
  edit(id: number, obj: BaseAddDto): Observable<OperationResultType<BaseGetDto>>;
}
export interface IRemoveService {
  remove(id: number): Observable<OperationResultType<BaseGetDto>>;
}
export interface ICrudService
  extends IGetService,
  ICreateService,
  IEditService,
  IRemoveService {
  configureMakers(t?: new () => BaseGetDto, tc?: new () => BaseAddDto, te?: new () => BaseChangeDto);
  getGetGenericMaker(): new () => BaseGetDto;
  getAddGenericMaker(): new () => BaseAddDto;
  getChangeGenericMaker(): new () => BaseChangeDto;
}
