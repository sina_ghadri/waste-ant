import { ICrudService } from '../interfaces/crud-service';
import { OperationResultCount, ODataQueryOptions, OperationResultType, BaseGetDto, BaseAddDto, BaseChangeDto } from '../../../models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { isArray, isObject } from 'util';

export class Cache {
    private data: any = {};

    public getItem(name: string, callback: Observable<any>): Observable<any> {
        return new Observable(ob => {
            if(this.data[name]) ob.next(this.data[name]);
            return callback.subscribe(x => {
                this.data[name] = x;
                ob.next(x);
            })
        });
    }
    public removeItem(name: string) {
        this.data[name] = null;
    }
    public removeAll() {
        this.data = {};
    }
}

export class BaseCrudService<T extends BaseGetDto, TC extends BaseAddDto, TE extends BaseChangeDto> implements ICrudService {
    protected cache: Cache = new Cache;

    private static _genericMakers: any = {};
    static getGenericMaker(name: string): new () => any {
        return BaseCrudService._genericMakers[name];
    }

    private _tGenericMaker: new () => T;
    private _tcGenericMaker: new () => TC;
    private _teGenericMaker: new () => TE;

    protected baseUrl: string;
    protected http: HttpClient;

    constructor(http: HttpClient, baseUrl: string) {
        this.baseUrl = baseUrl;
        this.http = http;
    }

    configureMakers(t?: new () => T, tc?: new () => TC, te?: new () => TE) {
        BaseCrudService._genericMakers[Object.getPrototypeOf(new t()).constructor.name] = t;
        BaseCrudService._genericMakers[Object.getPrototypeOf(new tc()).constructor.name] = tc;
        BaseCrudService._genericMakers[Object.getPrototypeOf(new te()).constructor.name] = te;

        if (t) this._tGenericMaker = t;
        if (tc) this._tcGenericMaker = tc;
        if (te) this._teGenericMaker = te;
    }
    getGetGenericMaker(): new () => T { return this._tGenericMaker; }
    getAddGenericMaker(): new () => TC { return this._tcGenericMaker; }
    getChangeGenericMaker(): new () => TE { return this._teGenericMaker; }

    mapper(op: OperationResultCount<T[]>): OperationResultCount<T[]>;
    mapper(op: OperationResultType<T[]>): OperationResultType<T[]>;
    mapper(op: OperationResultType<T>): OperationResultType<T>;
    mapper(op: (OperationResultCount<T[]> | OperationResultType<T[]> | OperationResultType<T>)): OperationResultCount<T[]> | OperationResultType<T[]> | OperationResultType<T> {
        if (this._tGenericMaker) {
            if (isArray(op.Data)) {
                op.Data = (<T[]>op.Data).map(y => Convert.toObject(y, this._tGenericMaker).toClient());
            }
            else {
                op.Data = Convert.toObject(op.Data, this._tGenericMaker).toClient();
            }
        }
        return op;
    }

    getAll(): Observable<OperationResultCount<T[]>> {
        return this.cache.getItem('getAll', this.http.get<OperationResultCount<T[]>>(this.baseUrl).map(op => this.mapper(op)));
    }
    select(query: ODataQueryOptions): Observable<OperationResultCount<T[]>> {
        return this.cache.getItem('select' + query.toString(), this.http.get<OperationResultCount<T[]>>(this.baseUrl + query.toString()).map(op => this.mapper(op)));
    }
    getById(id: number): Observable<OperationResultType<T>> {
        return this.cache.getItem('getById/' + id, this.http.get<OperationResultType<T>>(this.baseUrl + id).map(op => this.mapper(op)));
    }
    create(obj: TC): Observable<OperationResultType<T>> {
        if (obj.toServer) obj = obj.toServer();
        else if (this._tcGenericMaker) obj = Convert.toObject(obj, this._tcGenericMaker).toServer();
        return this.http.post<OperationResultType<T>>(this.baseUrl, obj).map(op => {
            op = this.mapper(op);
            this.cache.removeAll();
            return op;
        });
    }
    edit(id: number, obj: TE): Observable<OperationResultType<T>> {
        if (obj.toServer) obj = obj.toServer();
        else if (this._teGenericMaker) obj = Convert.toObject(obj, this._teGenericMaker).toServer();
        return this.http.patch<OperationResultType<T>>(this.baseUrl + id, obj).map(op => {
            op = this.mapper(op);
            this.cache.removeAll();
            return op;
        });
    }
    remove(id: number): Observable<OperationResultType<T>> {
        return this.http.delete<OperationResultType<T>>(this.baseUrl + id).map(op => {
            op = this.mapper(op);
            this.cache.removeAll();
            return op;
        });
    }
}

export class Convert {
    static toObject<T>(value: any[], type?: new () => T): T[];
    static toObject<T>(value: any, type?: new () => T): T;

    static toObject<T>(value: any | any[], type?: new () => T): T | T[] {
        if (isArray(value)) {
            let array = [];
            for (let i = 0; i < value.length; i++) array.push(Convert.toObject(value[i], type));
            return array;
        } else {
            let obj;
            if (type) obj = new type;
            else obj = {};
            for (let k in value) {
                const val = value[k];
                if (isObject(val)) {
                    const tmp = obj[k];
                    if (tmp) {
                        const childTypeName = Object.getPrototypeOf(tmp).constructor.name;
                        const maker = BaseCrudService.getGenericMaker(childTypeName);
                        if (maker) {
                            obj[k] = Convert.toObject(val, maker)
                            continue;
                        }
                    }
                }
                obj[k] = val;
            }
            return obj;
        }
    }
}