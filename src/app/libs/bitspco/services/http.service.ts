import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import {
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpResponse,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { AuthorizationType } from '../../../models';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';

@Injectable()
export class HttpService implements HttpInterceptor {

  constructor(private service: HttpConfigService) {

  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let url = request.url;
    let token = this.service.token;
    let authorizationType = this.service.authorizationType;

    if (this.service.host) url = url.replace('[host]', this.service.host);
    if (this.service.port) url = url.replace('[port]', this.service.port.toString());

    if (!request.headers.get('Authorization')) {
      if (token && token.length > 0) {
        switch (authorizationType) {
          case AuthorizationType.Header:
            request = request.clone({
              url: url,
              setHeaders: { Authorization: token ? token : '' }
            });
            break;
          case AuthorizationType.QueryString:
            url = url + (url.indexOf('?') > -1 ? '&' : '?') + 'ApiKey=' + token;
            request = request.clone({ url: url });
            break;
        }
      } else request = request.clone({ url: url });
    }
    return next.handle(request).do(
      (response: HttpEvent<any>) => {
        if (response instanceof HttpResponse) {
          const context: HttpContext = new HttpContext;
          context.Request = request;
          context.Response = response;
          this.service.onsuccess.emit(context);
          if (response.status < 200) {
            this.service.on1xx.emit(context);
            if (response.status == 100) this.service.on100Continue.emit(context);
            else if (response.status == 101) this.service.on101SwitchingProtocols.emit(context);
            else if (response.status == 102) this.service.on102Processing.emit(context);
            else if (response.status == 103) this.service.on103EarlyHints.emit(context);
          } else if (response.status < 300) {
            this.service.on2xx.emit(context);
            if (response.status == 200) this.service.on200Ok.emit(context);
            else if (response.status == 201) this.service.on201Created.emit(context);
            else if (response.status == 202) this.service.on202Accepted.emit(context);
            else if (response.status == 203) this.service.on203NonAuthoritativeInformation.emit(context);
            else if (response.status == 204) this.service.on204NoContent.emit(context);
            else if (response.status == 205) this.service.on205ResetContent.emit(context);
            else if (response.status == 206) this.service.on206PartialContent.emit(context);
            else if (response.status == 207) this.service.on207MultiStatus.emit(context);
            else if (response.status == 208) this.service.on208AlreadyReported.emit(context);
            else if (response.status == 226) this.service.on226ImUsed.emit(context);
          } else if (response.status < 400) {
            this.service.on3xx.emit(context);
            if (response.status == 300) this.service.on300MultipleChoices.emit(context);
            else if (response.status == 301) this.service.on301MovedPermanently.emit(context);
            else if (response.status == 302) this.service.on302Found.emit(context);
            else if (response.status == 303) this.service.on303SeeOther.emit(context);
            else if (response.status == 304) this.service.on304NotModified.emit(context);
            else if (response.status == 305) this.service.on305UseProxy.emit(context);
            else if (response.status == 306) this.service.on306SwitchProxy.emit(context);
            else if (response.status == 307) this.service.on307TemporaryRedirect.emit(context);
            else if (response.status == 308) this.service.on308PermanentRedirect.emit(context);
          }
        }
      },
      (e: any) => {
        const context: HttpContext = new HttpContext;
        context.Request = request;
        context.Response = new HttpResponse({
          body: e.error,
          headers: e.headers,
          status: e.status,
          statusText: e.statusText,
          url: e.url
        });
        if (e instanceof HttpErrorResponse) {
          this.service.onerror.emit(context);
          if (e.status == 0) {
          } else if (e.status < 500) {
            this.service.on4xx.emit(context);
            if (e.status == 400) this.service.on400BadRequest.emit(context);
            else if (e.status == 401) this.service.on401UnAuthorize.emit(context);
            else if (e.status == 402) this.service.on402PaymentRequire.emit(context);
            else if (e.status == 403) this.service.on403Forbidden.emit(context);
            else if (e.status == 404) this.service.on404NotFound.emit(context);
            else if (e.status == 405) this.service.on405MethodNotAllowed.emit(context);
            else if (e.status == 406) this.service.on406NotAcceptable.emit(context);
            else if (e.status == 407) this.service.on407ProxyAuthenticationRquired.emit(context);
            else if (e.status == 408) this.service.on408RequestTimeout.emit(context);
            else if (e.status == 409) this.service.on409Confilict.emit(context);
            else if (e.status == 410) this.service.on410Gone.emit(context);
            else if (e.status == 411) this.service.on411LengthRequired.emit(context);
            else if (e.status == 412) this.service.on412PreconditionFailed.emit(context);
            else if (e.status == 413) this.service.on413PayloadTooLarg.emit(context);
            else if (e.status == 414) this.service.on414UrlTooLarg.emit(context);
            else if (e.status == 415) this.service.on415UnsupportedMediaType.emit(context);
            else if (e.status == 416) this.service.on416RangeNotSatisfiable.emit(context);
            else if (e.status == 417) this.service.on417ExpectationFailed.emit(context);
            else if (e.status == 418) this.service.on418ImTeapot.emit(context);
            else if (e.status == 421) this.service.on421MisdirectedRequest.emit(context);
            else if (e.status == 422) this.service.on422UnprocessableEntity.emit(context);
            else if (e.status == 423) this.service.on423Locked.emit(context);
            else if (e.status == 424) this.service.on424FailedDependency.emit(context);
            else if (e.status == 425) this.service.on425TooEarly.emit(context);
            else if (e.status == 426) this.service.on426UpgradeRequired.emit(context);
            else if (e.status == 428) this.service.on428PreconditionRequired.emit(context);
            else if (e.status == 429) this.service.on429TooManyRequest.emit(context);
            else if (e.status == 431) this.service.on431RequestHeaderFieldsTooLarg.emit(context);
          } else if (e.status < 600) {
            this.service.on5xx.emit(context);
            if (e.status == 500) this.service.on500InternalServerError.emit(context);
            else if (e.status == 501) this.service.on501NotImplemented.emit(context);
            else if (e.status == 502) this.service.on502BadGateway.emit(context);
            else if (e.status == 503) this.service.on503ServiceUnavailable.emit(context);
            else if (e.status == 504) this.service.on504GatewayTimeout.emit(context);
            else if (e.status == 505) this.service.on505HttpVersionNotSupported.emit(context);
            else if (e.status == 506) this.service.on506VariantAlsoNegotiates.emit(context);
            else if (e.status == 507) this.service.on507InsufficientStorage.emit(context);
            else if (e.status == 508) this.service.on508LoopDetected.emit(context);
            else if (e.status == 509) this.service.on509Unassigned.emit(context);
            else if (e.status == 510) this.service.on510NotExtended.emit(context);
            else if (e.status == 511) this.service.on511NetworkAuthenticationRequired.emit(context);
          }
        }
      },
      () => {
        const context: HttpContext = new HttpContext;
        context.Request = request;
        this.service.oncomplete.emit(context);
      }
    );
  }
}

export class HttpContext {
  Request: HttpRequest<any>;
  Response: HttpResponse<any>;
}

@Injectable({ providedIn: 'root' })
export class HttpConfigService {

  host: string;
  port: number;
  token: string;
  authorizationType: AuthorizationType = AuthorizationType.Header;

  onsuccess: EventEmitter<HttpContext> = new EventEmitter;
  onerror: EventEmitter<HttpContext> = new EventEmitter;
  oncomplete: EventEmitter<HttpContext> = new EventEmitter;

  on1xx: EventEmitter<HttpContext> = new EventEmitter;
  on100Continue: EventEmitter<HttpContext> = new EventEmitter;
  on101SwitchingProtocols: EventEmitter<HttpContext> = new EventEmitter;
  on102Processing: EventEmitter<HttpContext> = new EventEmitter;
  on103EarlyHints: EventEmitter<HttpContext> = new EventEmitter;

  on2xx: EventEmitter<HttpContext> = new EventEmitter;
  on200Ok: EventEmitter<HttpContext> = new EventEmitter;
  on201Created: EventEmitter<HttpContext> = new EventEmitter;
  on202Accepted: EventEmitter<HttpContext> = new EventEmitter;
  on203NonAuthoritativeInformation: EventEmitter<HttpContext> = new EventEmitter;
  on204NoContent: EventEmitter<HttpContext> = new EventEmitter;
  on205ResetContent: EventEmitter<HttpContext> = new EventEmitter;
  on206PartialContent: EventEmitter<HttpContext> = new EventEmitter;
  on207MultiStatus: EventEmitter<HttpContext> = new EventEmitter;
  on208AlreadyReported: EventEmitter<HttpContext> = new EventEmitter;
  on226ImUsed: EventEmitter<HttpContext> = new EventEmitter;

  on3xx: EventEmitter<HttpContext> = new EventEmitter;
  on300MultipleChoices: EventEmitter<HttpContext> = new EventEmitter;
  on301MovedPermanently: EventEmitter<HttpContext> = new EventEmitter;
  on302Found: EventEmitter<HttpContext> = new EventEmitter;
  on303SeeOther: EventEmitter<HttpContext> = new EventEmitter;
  on304NotModified: EventEmitter<HttpContext> = new EventEmitter;
  on305UseProxy: EventEmitter<HttpContext> = new EventEmitter;
  on306SwitchProxy: EventEmitter<HttpContext> = new EventEmitter;
  on307TemporaryRedirect: EventEmitter<HttpContext> = new EventEmitter;
  on308PermanentRedirect: EventEmitter<HttpContext> = new EventEmitter;

  on4xx: EventEmitter<HttpContext> = new EventEmitter;
  on400BadRequest: EventEmitter<HttpContext> = new EventEmitter;
  on401UnAuthorize: EventEmitter<HttpContext> = new EventEmitter;
  on402PaymentRequire: EventEmitter<HttpContext> = new EventEmitter;
  on403Forbidden: EventEmitter<HttpContext> = new EventEmitter;
  on404NotFound: EventEmitter<HttpContext> = new EventEmitter;
  on405MethodNotAllowed: EventEmitter<HttpContext> = new EventEmitter;
  on406NotAcceptable: EventEmitter<HttpContext> = new EventEmitter;
  on407ProxyAuthenticationRquired: EventEmitter<HttpContext> = new EventEmitter;
  on408RequestTimeout: EventEmitter<HttpContext> = new EventEmitter;
  on409Confilict: EventEmitter<HttpContext> = new EventEmitter;
  on410Gone: EventEmitter<HttpContext> = new EventEmitter;
  on411LengthRequired: EventEmitter<HttpContext> = new EventEmitter;
  on412PreconditionFailed: EventEmitter<HttpContext> = new EventEmitter;
  on413PayloadTooLarg: EventEmitter<HttpContext> = new EventEmitter;
  on414UrlTooLarg: EventEmitter<HttpContext> = new EventEmitter;
  on415UnsupportedMediaType: EventEmitter<HttpContext> = new EventEmitter;
  on416RangeNotSatisfiable: EventEmitter<HttpContext> = new EventEmitter;
  on417ExpectationFailed: EventEmitter<HttpContext> = new EventEmitter;
  on418ImTeapot: EventEmitter<HttpContext> = new EventEmitter;
  on421MisdirectedRequest: EventEmitter<HttpContext> = new EventEmitter;
  on422UnprocessableEntity: EventEmitter<HttpContext> = new EventEmitter;
  on423Locked: EventEmitter<HttpContext> = new EventEmitter;
  on424FailedDependency: EventEmitter<HttpContext> = new EventEmitter;
  on425TooEarly: EventEmitter<HttpContext> = new EventEmitter;
  on426UpgradeRequired: EventEmitter<HttpContext> = new EventEmitter;
  on428PreconditionRequired: EventEmitter<HttpContext> = new EventEmitter;
  on429TooManyRequest: EventEmitter<HttpContext> = new EventEmitter;
  on431RequestHeaderFieldsTooLarg: EventEmitter<HttpContext> = new EventEmitter;

  on5xx: EventEmitter<HttpContext> = new EventEmitter;
  on500InternalServerError: EventEmitter<HttpContext> = new EventEmitter;
  on501NotImplemented: EventEmitter<HttpContext> = new EventEmitter;
  on502BadGateway: EventEmitter<HttpContext> = new EventEmitter;
  on503ServiceUnavailable: EventEmitter<HttpContext> = new EventEmitter;
  on504GatewayTimeout: EventEmitter<HttpContext> = new EventEmitter;
  on505HttpVersionNotSupported: EventEmitter<HttpContext> = new EventEmitter;
  on506VariantAlsoNegotiates: EventEmitter<HttpContext> = new EventEmitter;
  on507InsufficientStorage: EventEmitter<HttpContext> = new EventEmitter;
  on508LoopDetected: EventEmitter<HttpContext> = new EventEmitter;
  on509Unassigned: EventEmitter<HttpContext> = new EventEmitter;
  on510NotExtended: EventEmitter<HttpContext> = new EventEmitter;
  on511NetworkAuthenticationRequired: EventEmitter<HttpContext> = new EventEmitter;


  constructor() { }

}

export enum HttpMethod {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  PUT = 'PUT',
  DELETE = 'DELETE',
}

export enum HttpStatus {
  NoResponse = 0,
  Continue = 100,
  SwitchingProtocols = 101,
  Processing = 102,
  EarlyHints = 103,
  Ok = 200,
  Created = 201,
  Accepted = 202,
  NonAuthoritativeInformation = 203,
  NoContent = 204,
  ResetContent = 205,
  PartialContent = 206,
  MultiStatus = 207,
  AlreadyReported = 208,
  ImUsed = 226,
  MultipleChoices = 300,
  MovedPermanently = 301,
  Found = 302,
  SeeOther = 303,
  NotModified = 304,
  UseProxy = 305,
  SwitchProxy = 306,
  TemporaryRedirect = 307,
  PermanentRedirect = 308,
  BadRequest = 400,
  UnAuthorize = 401,
  PaymentRequire = 402,
  Forbidden = 403,
  NotFound = 404,
  MethodNotAllowed = 405,
  NotAcceptable = 406,
  ProxyAuthenticationRquired = 407,
  RequestTimeout = 408,
  Confilict = 409,
  Gone = 410,
  LengthRequired = 411,
  PreconditionFailed = 412,
  PayloadTooLarg = 413,
  UrlTooLarg = 414,
  UnsupportedMediaType = 415,
  RangeNotSatisfiable = 416,
  ExpectationFailed = 417,
  ImTeapot = 418,
  MisdirectedRequest = 421,
  UnprocessableEntity = 422,
  Locked = 423,
  FailedDependency = 424,
  TooEarly = 425,
  UpgradeRequired = 426,
  PreconditionRequired = 428,
  TooManyRequest = 429,
  RequestHeaderFieldsTooLarg = 431,
  InternalServerError = 500,
  NotImplemented = 501,
  BadGateway = 502,
  ServiceUnavailable = 503,
  GatewayTimeout = 504,
  HttpVersionNotSupported = 505,
  VariantAlsoNegotiates = 506,
  InsufficientStorage = 507,
  LoopDetected = 508,
  Unassigned = 509,
  NotExtended = 510,
  NetworkAuthenticationRequired = 511,
}