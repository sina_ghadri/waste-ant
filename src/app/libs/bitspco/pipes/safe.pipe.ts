import { Pipe, NgModule } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({name: 'safe'})
export class SafeHtmlPipe {
  constructor(private sanitizer:DomSanitizer){}

  transform(html) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
}

@NgModule({
    declarations: [SafeHtmlPipe],
    exports: [SafeHtmlPipe]
})
export class SafeHtmlModule {}