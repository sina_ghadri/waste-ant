export class TimeSplit {
  text: string;
  time?: string;
  color: string;
  data?: any;
}

export class TimeSpliteEvent {
  data: TimeSplit;
  callback: (x: TimeSplit) => void;
}