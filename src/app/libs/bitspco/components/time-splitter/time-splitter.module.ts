import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { TimeSplitterComponent } from "./time-splitter.component";


@NgModule({
  imports: [
    FormsModule,
    CommonModule,
  ],
  declarations: [
    TimeSplitterComponent,
  ],
  exports: [
    TimeSplitterComponent,
  ],
  providers: [

  ]
})
export class TimeSplitterModule {}
