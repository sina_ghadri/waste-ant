import { Component, OnInit, Input, ElementRef, HostListener, Output, EventEmitter } from "@angular/core";
import { TimeSplit, TimeSpliteEvent } from "./models";

@Component({
  selector: 'app-time-splitter',
  templateUrl: './time-splitter.component.html',
  styleUrls: ['./time-splitter.component.scss']
})
export class TimeSplitterComponent implements OnInit {
  @Input() items: TimeSplit[] = [];
  @Input() model: TimeSplit = { text: '', color: 'red' };
  @Input() startTime: string;
  @Input() endTime: string;
  @Input() step: number = 5;

  private _disabled: boolean = false;
  @Input()
  get disabled(): any { return this._disabled; }
  set disabled(value: any) { this._disabled = (value == true || value == 1 || value == 'true' || value == '' || value == undefined); }

  @Output() onSplit: EventEmitter<any> = new EventEmitter;

  @HostListener('mouseup', ['$event'])
  onmouseup(e: MouseEvent) {
    if (this.disabled) return;
    if (this.selectedItem) {
      if (this.parseTime(this.selectedItem.time) - this.parseTime(this.startTime) < 0.2) {
        this.remove(this.selectedItem);
      }
    }
    this.selectedItem = null;
  }
  @HostListener('mousemove', ['$event'])
  onmouseMove(e: MouseEvent) {
    if (this.disabled) return;
    if (this.selectedItem) {
      const point = this.getPoint(e, this.element.nativeElement);
      const width = this.element.nativeElement.offsetWidth;
      let time = ((width - point.x) * (this.parseTime(this.endTime) - this.parseTime(this.startTime)) / width) + this.parseTime(this.startTime);
      let hour = Math.floor(time);
      let min = Math.round((time - Math.floor(time)) * 60);
      for (let i = 1; i < this.step; i++) {
        if ((min + i) % this.step == 0) {
          min = min + i;
          if (min > 60) { min -= 60; hour++; }
          break;
        }
        if ((min - i) % this.step == 0) {
          min = min - i;
          if (min < 0) { min = -min; hour--; }
          break;
        }
      }
      time = hour + (min / 60);
      if (time > this.parseTime(this.startTime) && time <= this.parseTime(this.endTime))
        this.selectedItem.time = this.toTime(time);
    }
  }

  selectedItem: TimeSplit = null;

  constructor(private element: ElementRef) { }
  ngOnInit(): void {
  }
  remove(item: TimeSplit) {
    this.items.splice(this.items.indexOf(item), 1);
  }
  getPoint(e: MouseEvent, parent: HTMLElement) {
    let rect = parent.getBoundingClientRect();
    const x = (e.clientX - rect.left) + parent.scrollLeft;
    const y = (e.clientY - rect.top) + parent.scrollTop;
    return { x, y };
  }
  parseTime(time: string): number {
    let splite = time.split(':');
    if (splite.length == 0) return 0;
    if (splite.length == 1) return parseInt(splite[0]);
    const hour = parseInt(splite[0]);
    const min = parseInt(splite[1]);
    return hour + (min / 60);
  }
  formatTime(time: string): string {
    if (time) {
      const split = time.split(':');
      if (split.length > 1) {
        return split[0] + ':' + split[1];
      }
    }
    return '-';
  }
  toTime(time: number): string {
    const hour = Math.floor(time);
    const min = Math.round((time - hour) * 60);
    return (hour > 9 ? '' : '0') + hour + ':' + (min > 9 ? '' : '0') + min;
  }
  getPercent(index: number): number {
    const value = this.parseTime(this.items[index].time) - (index > 0 ? this.parseTime(this.items[index - 1].time) : this.parseTime(this.startTime));
    return (value * 100 / (this.parseTime(this.endTime) - this.parseTime(this.startTime)));
  }
  getItems() {
    return this.items = this.items.sort((x1: TimeSplit, x2: TimeSplit) => {
      const time1 = this.parseTime(x1.time);
      const time2 = this.parseTime(x2.time);
      if (time1 > time2) return 1;
      if (time1 < time2) return -1;
      return 0;
    })
  }

  preventDefault(e: MouseEvent, obj: TimeSplit) {
    e.preventDefault();
    if (this.disabled) return;
    this.selectedItem = obj;
  }
  addSplit(e: MouseEvent, parent: HTMLElement) {
    if (this.disabled) return;
    if (this.selectedItem) return;
    const point = this.getPoint(e, parent);
    const width = parent.offsetWidth;
    const newNode = new TimeSplit;
    newNode.time = this.toTime(this.parseTime(this.startTime) + ((width - point.x) * (this.parseTime(this.endTime) - this.parseTime(this.startTime)) / width));
    const te: TimeSpliteEvent = new TimeSpliteEvent;
    te.data = newNode;
    te.callback = (x: TimeSplit) => {
      this.items.push(x);
    };
    this.onSplit.emit(te);
  }

  addSplitInFree(e: MouseEvent, item: TimeSplit) {
    if (this.disabled) return;
    const point = this.getPoint(e, this.element.nativeElement);
    const index = this.items.indexOf(item);
    const time = this.parseTime(item.time);
    const pretime = (index > 0 ? this.parseTime(this.items[index - 1].time) : this.parseTime(this.startTime));
    const dist = time - pretime;
    const width = this.element.nativeElement.offsetWidth;
    const newNode = new TimeSplit;
    newNode.time = this.toTime(pretime + ((width - point.x) * dist / width));
    newNode.color = this.model.color;
    newNode.text = this.model.text;
    this.items.splice(index, 0, newNode);
  }
}
