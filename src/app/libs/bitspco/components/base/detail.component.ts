import { SimpleChanges, OnChanges, Input, OnInit } from '@angular/core';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { BaseFormComponent } from './form.component';
import { ICrudService } from 'src/app/interfaces';
import { FormGroup } from '@angular/forms';

export class BaseDetailComponent extends BaseFormComponent implements OnInit, OnChanges {
    @Input() id: number;
    private _service: ICrudService;

    constructor(service: ICrudService, formBuilder: RxFormBuilder) {
        super(formBuilder);
        this._service = service;
    }
    ngOnInit() {

    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.obj) this.form = this.makeFormGroup(this.obj);
    }

    makeFormGroup(obj: any): FormGroup {
        const type = this._service.getGetGenericMaker();
        return super.makeFormGroup(Object.assign(new type, obj), type);
    }
}