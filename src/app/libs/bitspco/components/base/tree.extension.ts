import { NzTreeNode, NzTreeNodeOptions } from 'ng-zorro-antd';
import { BaseGetDto } from 'src/app/models';

export class TreeExtension {

  foreach(items: NzTreeNode[], before: (node: NzTreeNode) => void, after: (node: NzTreeNode) => void = null) {
    if (!items) return;
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      before(item);
      this.foreach(item.children, before, after);
      if(after) after(item);
    }
  }
  searchInTree(items: NzTreeNode[], value: string): string[] {
    let result: string[] = [];
    if (!items) return;
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (value && item.title.indexOf(value) > -1) result.push(item.key);
      result = result.concat(this.searchInTree(item.children, value));
    }
    return result;
  }
  getSelectedNode(items: NzTreeNode[]): NzTreeNode {
    if (!items) return;
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (item.isSelected) return item;
      const result = this.getSelectedNode(item.children);
      if (result) return result;
    }
    return null;
  }
  getSelectedNodeByKey(items: NzTreeNode[], key: string): NzTreeNode {
    if (!items) return;
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      item.isSelected = (item.key == key);
      if (item.isSelected) return item;
      const result = this.getSelectedNodeByKey(item.children, key);
      if (result) return result;
    }
    return null;
  }
  getExpandedKeys(items: NzTreeNode[]): string[] {
    if (!items) return [];
    const result: string[] = [];
    for (let i = 0; i < items.length; i++) {
      const node = items[i];
      if (node.isExpanded) result.push(node.key);
      result.concat(this.getExpandedKeys(node.children));
    }
    return result;
  }
  clearSelectedNode(items: NzTreeNode[]) {
    if (!items) return;
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      item.isSelected = false;
      this.clearSelectedNode(item.children);
    }
  }
  selectNodeByKey(items: NzTreeNode[], key: string) {
    if (!items) return;
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if(item.key == key) {
        item.isSelected = true;
        return true;
      }
      const result = this.selectNodeByKey(item.children, key);
      if(result) return result;
    }
    return false;
  }
  openNodeByKeys(items: NzTreeNode[], keys: string[]): boolean {
    if (!items) return;
    let result = false;
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      const child_result = this.openNodeByKeys(item.children, keys);
      if (child_result || keys.indexOf(item.key) > -1) {
        item.isExpanded = true;
        result = true;
      }
    }
    return result;
  }
  createTreeNode(item: BaseGetDto): NzTreeNode {
    const obj = <any>item;
    return new NzTreeNode({
      key: item.Id.toString(),
      title: (obj.Name ? obj.Name : (obj.Title ? obj.Title : (obj.Text ? obj.Text : ''))),
    });
  }
  createTreeNodes(items: any[], expandKeys?: string[], parent?: BaseGetDto): NzTreeNode[] {
    const result: NzTreeNode[] = [];
    const fitems = items.filter(x => (parent ? x.ParentId == parent.Id : !x.ParentId));
    for (let i = 0; i < fitems.length; i++) {
      const item = fitems[i];
      item.Parent = parent;
      const node = this.createTreeNode(item);
      node.children = this.createTreeNodes(items, expandKeys, item);
      node.children.map(x => { x.parentNode = node; return x; });
      if (expandKeys) node.isExpanded = expandKeys.indexOf(node.key) > -1;
      node.origin.data = item;
      if (node.children.length == 0) node.isLeaf = true;
      result.push(node);
    }
    return result.sort((x1, x2) => x1.isLeaf == x2.isLeaf ? 0 : (x1.isLeaf ? 1 : -1));
    // return result.sort((x1, x2) => x1.isLeaf == x2.isLeaf ? (x1.title == x2.title ? 0 : (x1.title > x2.title ? 1 : -1)) : (x1.isLeaf ? 1 : -1));
  }
  convertTreeNode2Options(node: NzTreeNode): NzTreeNodeOptions {
    return {
      checked: node.isChecked,
      disableCheckbox: node.isDisableCheckbox,
      children: node.children.map(x => this.convertTreeNode2Options(x)),
      disabled: node.isDisabled,
      expanded: node.isExpanded,
      icon: node.icon,
      isLeaf: node.isLeaf,
      key: node.key,
      selectable: node.isSelectable,
      selected: node.isSelected,
      title: node.title
    }
  }
}