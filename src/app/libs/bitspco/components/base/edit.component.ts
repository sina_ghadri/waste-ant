import { Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { BaseFormComponent } from './form.component';
import { ICrudService } from 'src/app/interfaces';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { FormGroup } from '@angular/forms';

export class BaseEditComponent extends BaseFormComponent implements OnInit, OnChanges {
    @Input() id: number;

    private _service: ICrudService;
    success: boolean;
    message: string;

    constructor(service: ICrudService, formBuilder: RxFormBuilder) {
        super(formBuilder);
        this._service = service;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    ngOnChanges(changes: SimpleChanges): void {
        if (changes.obj) this.form = this.makeFormGroup(this.obj);
    }

    makeFormGroup(obj: any): FormGroup {
        const type = this._service.getChangeGenericMaker();
        return super.makeFormGroup(Object.assign(new type, obj), type);
    }
    submit() {
        this.isLoading = true;
        this._service.edit(this.id, Object.assign(this.obj, this.form.value)).subscribe(op => {
            if (op.Success) {
                this.oncomplete.emit(op);
            }
            this.success = op.Success;
            this.message = op.Message;
            this.isLoading = false;
        }, e => this.isLoading = false);
    }
    cancel() {
        this.oncomplete.emit();
    }
}