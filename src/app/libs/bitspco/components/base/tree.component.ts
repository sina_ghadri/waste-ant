import { BaseComponent } from './base.component';
import { NzTreeNode } from 'ng-zorro-antd';
import { BaseGetDto } from 'src/app/models';
import { ICrudService } from 'src/app/interfaces';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { TreeExtension } from './tree.extension';

export class BaseTreeComponent extends BaseComponent {

    items: any[];
    nodes: NzTreeNode[];
    service: ICrudService;
    extension: TreeExtension = new TreeExtension;

    nzExpandedKeys: string[];

    constructor(service: ICrudService, formBuilder: RxFormBuilder) {
        super(service, formBuilder);
    }
    ngOnInit() {
        super.ngOnInit();
    }
    load() {
        this.selectedItem = null;
        this.isLoading = true;
        this.service.getAll().subscribe(op => {
            this.items = op.Data;
            this.loadNodes();
            this.isLoading = false;
        });
    }
    loadNodes() {
        const expandKeys = this.extension.getExpandedKeys(this.nodes);
        this.nodes = this.extension.createTreeNodes(this.items, expandKeys);
    }
    createTreeNode(item: BaseGetDto): NzTreeNode {
        return this.extension.createTreeNode(item);
    }
}