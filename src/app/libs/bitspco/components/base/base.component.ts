import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { isObject } from 'util';
import { FormGroup } from '@angular/forms';
import { BaseCrudService } from 'src/app/services';
import { ICrudService } from 'src/app/interfaces';

export class BaseComponent {
  id: number;
  form: FormGroup;
  formBuilder: RxFormBuilder;
  
  items: any[];
  service: ICrudService;

  isLoading: boolean = false;
  selectedItem: any;

  constructor(service: ICrudService, formBuilder: RxFormBuilder) {
    this.service = service;
    this.formBuilder = formBuilder;
  }
  ngOnInit() {
    
  }
  map(obj1, obj2) {
    for (const key in obj1) {
      if (obj1.hasOwnProperty(key) && obj2.hasOwnProperty(key)) {
        obj1[key] = obj2[key];
      }
    }
    return obj1;
  }
  created(op) {}
  edited(op) {}
  detailed() {}
}