import { ICrudService } from "src/app/libs/bitspco/interfaces/crud-service";
import { NzTreeNode } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { ViewChild } from '@angular/core';
import { LayoutTreeComponent } from 'src/app/layouts/tree/tree.component';
import { BaseComponent } from './base.component';
import { BaseGetDto } from 'src/app/models';
import { BaseTreeComponent } from './tree.component';

export class BaseTreeLayoutComponent extends BaseTreeComponent {
  id: number;

  @ViewChild(LayoutTreeComponent, { static: false }) layout: LayoutTreeComponent;

  constructor(service: ICrudService, formBuilder?: RxFormBuilder) {
    super(service, formBuilder);
  }
  ngOnInit() {
    super.ngOnInit();
  }
  detail(item: BaseGetDto) {
    this.id = item.Id;
    this.selectedItem = item;
  }
  create(item: BaseGetDto) {
    let obj: any = null;
    const type = this.service.getAddGenericMaker();
    if (type) obj = new type;
    if (item) this.map(obj, item);
    this.selectedItem = obj;
  }
  created(op) {
    if (op) {
      this.items.push(op.Data);
      this.loadNodes();
    }
    else this.layout.created(op, op ? op.Data.Id.toString() : null);
  }
  edit(item: BaseGetDto) {
    this.id = item.Id;
    const type = this.service.getChangeGenericMaker();
    if (type) item = this.map(new type, item);
    this.selectedItem = item;
  }
  edited(op) {
    if (op) {
      this.items.splice(this.items.indexOf(this.items.find(x => x.Id == op.Data.Id)), 1, op.Data);
      const node = this.createTreeNode(op.Data);
      this.layout.selectedNode.title = node.title;
      this.layout.selectedNode.origin.data = op.Data;
    }
    this.layout.edited(op);
  }
  remove() {
    this.isLoading = true;
    this.service.remove(this.id).subscribe(op => {
      this.layout.removed(op);
      this.isLoading = false;
      if (op.Success) this.load();
    }, e => this.isLoading = false)
  }
  selectedChange(e) {

  }
}
