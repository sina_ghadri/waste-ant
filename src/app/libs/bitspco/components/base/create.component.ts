import { BaseFormComponent } from './form.component';
import { ICrudService } from 'src/app/interfaces';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { SimpleChanges, OnChanges, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

export class BaseCreateComponent extends BaseFormComponent implements OnInit {
  private _service: ICrudService;

  success: boolean;
  message: string;

  constructor(service: ICrudService, formBuilder: RxFormBuilder) {
    super(formBuilder);
    this._service = service;
  }
  ngOnInit(): void {
    super.ngOnInit();
    this.form = this.makeFormGroup({});
  }
  makeFormGroup(obj: any): FormGroup {
    const type = this._service.getAddGenericMaker();
    this.obj = Object.assign(new type, obj);
    return super.makeFormGroup(this.obj, type);
  }
  submit() {
    for (const i in this.form.controls) {
      this.form.controls[i].markAsDirty();
      this.form.controls[i].updateValueAndValidity();
    }
    if (this.form.valid) {
      this.isLoading = true;
      this._service.create(Object.assign(this.obj, this.f)).subscribe(op => {
        if (op.Success) {
          this.oncomplete.emit(op);
        }
        this.success = op.Success;
        this.message = op.Message;
        this.isLoading = false;
      }, e => this.isLoading = false);
    }
  }
  cancel() {
    this.oncomplete.emit();
  }
}
