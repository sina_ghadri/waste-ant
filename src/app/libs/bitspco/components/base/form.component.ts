import { FormGroup, FormControl, AbstractControl, FormArray } from '@angular/forms';
import { Input, Output, EventEmitter } from '@angular/core';
import { RxFormBuilder, RxFormArray } from '@rxweb/reactive-form-validators';
import { isObject, isArray } from 'util';
import { BaseCrudService } from 'src/app/services';
import { ICrudService } from 'src/app/interfaces';

export class BaseFormComponent {
  @Input() obj;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter;

  form: FormGroup;
  fb: RxFormBuilder;

  isLoading: boolean = false;

  get f(): any { return this.form.value; }

  constructor(formBuilder: RxFormBuilder) {
    this.fb = formBuilder;
  }
  ngOnInit() {

  }
  private baseMakeFormGroup(obj: any, type?: new () => any, level: number = 1): FormGroup {
    let form: FormGroup = null;
    let tmp: any = null;
    if (type) {
      form = this.fb.formGroup(type, obj);
      tmp = new type;
    }
    else form = this.fb.formGroup(obj);
    const keys = Object.keys(obj);
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      const value = obj[key];
      if (tmp && tmp[key]) {
        if (isArray(value)) {
          // const array: AbstractControl[] = [];
          // for (let i = 0; i < value.length; i++) {
          //   const item = value[i];
          //   const childTypeName = Object.getPrototypeOf(item).constructor.name;
          //   const maker = BaseCrudService.getGenericMaker(childTypeName);
          //   const sub_form = this.baseMakeFormGroup(item, maker, level - 1);
          //   array.push(sub_form);
          //   array[i].setParent(undefined);
          // }
          // form.controls[key] = new RxFormArray(value, array);
        }
        else if (isObject(value)) {
          const childTypeName = Object.getPrototypeOf(tmp[key]).constructor.name;
          const maker = BaseCrudService.getGenericMaker(childTypeName);
          if (maker && level > 0) {
            form.controls[key] = this.baseMakeFormGroup(value, maker, level - 1);
          }
        }
      }
    }
    return form;
  }
  makeFormGroup(obj: any, type?: new () => any, level: number = 1): FormGroup {
    return this.baseMakeFormGroup(obj, type, level);
  }
}