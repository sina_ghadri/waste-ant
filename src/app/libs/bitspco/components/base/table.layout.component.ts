import { ODataQueryOptions } from "src/app/libs/bitspco/models/odata-query-options";
import { ICrudService } from "src/app/libs/bitspco/interfaces/crud-service";
import { BaseGetDto, BaseAddDto, BaseChangeDto } from 'src/app/libs/bitspco/models/base';
import { ViewChild, TemplateRef } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { BaseComponent } from './base.component';
import { BaseTableComponent } from './table.component';
import { LayoutTableComponent } from 'src/app/layouts/table/table.component';

export class BaseTableLayoutComponent extends BaseTableComponent {
  @ViewChild(LayoutTableComponent, { static: false }) layout: LayoutTableComponent;
  modalService: NzModalService;

  /*==============Ajax==============*/
  total = 1;
  pageIndex = 1;
  pageSize = 10;
  loading = false;
  sortValue: string | null = null;
  sortKey: string | null = null;
  /*================================*/

  /*============Checkbox============*/
  isAllDisplayDataChecked = false;
  isIndeterminate = false;
  mapOfCheckedId: { [key: string]: boolean } = {};
  numberOfChecked = 0;
  /*================================*/
  filters: any = {};
  filterChangeTimer: any;

  constructor(
    service: ICrudService,
    formBuilder: RxFormBuilder,
  ) {
    super(service, formBuilder);
  }
  ngOnInit() {
    super.ngOnInit();
  }
  load(query?: ODataQueryOptions) {
    if (!query) query = new ODataQueryOptions();
    if (!query.filter) query.filter = "";
    for (const key in this.filters) {
      if (this.filters.hasOwnProperty(key)) {
        const value = this.filters[key];
        if (value !== '' && value !== undefined && value !== null) {
          if (query.filter.length > 0) query.filter += ' and ';
          query.filter += `${key} like \"${value}\"`;
        }
      }
    }
    if (!query.orderby) query.orderby = this.sortKey + ' ' + (this.sortValue == 'ascend' ? 'asc' : 'desc');
    if (!query.skip) query.skip = (this.pageIndex - 1) * this.pageSize;
    if (!query.top) query.top = this.pageSize;
    this.loading = true;
    this.service.select(query ? query : new ODataQueryOptions()).subscribe(op => {
      this.items = op.Data;
      this.total = op.Count;
      this.loading = false;
    }, e => {
      this.loading = false;
    });
  }
  filterChange() {
    if (this.filterChangeTimer) clearTimeout(this.filterChangeTimer);
    this.filterChangeTimer = setTimeout(() => {
      this.load();
    }, 250);
  }
  getFooter(): string {
    const from = (this.total > 0 ? (this.pageIndex - 1) * this.pageSize + 1 : 0);
    const to = (this.total > 0 ? (from + this.pageSize > this.total ? this.total : (this.pageIndex * this.pageSize - 1)) : 0)
    return `نمایش ${from} الی ${to} از ${this.total} ردیف`;
  }
  sort(sort: { key: string; value: string }): void {
    this.sortKey = sort.key;
    this.sortValue = sort.value;
    this.searchData();
  }
  searchData(reset: boolean = false): void {
    if (reset) {
      this.pageIndex = 1;
    }
    this.load();
  }
  refreshStatus(): void {
    this.isAllDisplayDataChecked = this.items.every(
      item => this.mapOfCheckedId[item.Id]
    );
    this.isIndeterminate =
      this.items.some(item => this.mapOfCheckedId[item.Id]) &&
      !this.isAllDisplayDataChecked;
    this.numberOfChecked = this.items.filter(
      item => this.mapOfCheckedId[item.Id]
    ).length;
  }
  checkAll(value: boolean): void {
    this.items.forEach(item => (this.mapOfCheckedId[item.Id] = value));
    this.refreshStatus();
  }
  create() {
    this.layout.create();
  }
  created(op) {
    this.layout.created(op);
  }
  detail(item: BaseGetDto) {
    this.id = item.Id;
    this.selectedItem = item;
    this.layout.detail(item);
  }
  detailed() {
    this.layout.detailed();
  }
  edit(item: BaseGetDto) {
    this.id = item.Id;
    this.selectedItem = item;
    this.layout.edit(item);
  }
  edited(op) {
    this.layout.edited(op);
  }
  remove(item: BaseGetDto) {
    this.id = item.Id;
    this.selectedItem = item;
    this.service.remove(item.Id).subscribe(op => {
      this.load();
    })
  }
}
