export class OperationResult {
    Code: number = 0;
    Success: boolean = true;
    Message: string = '';
}

export class OperationResultType<T> {
    Code: number = 0;
    Success: boolean = true;
    Message: string = '';
    Data: T;

    constructor(data?:T) {
        this.Data = data;
    }
}

export class OperationResultCount<T> {
    Code: number = 0;
    Success: boolean = true;
    Message: string = '';
    Data: T;
    Count:number = 0;

    constructor(data?: T, count: number = 0) {
        this.Data = data;
        this.Count = count;
    }
}
