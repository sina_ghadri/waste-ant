import { HttpParams } from "@angular/common/http";

export class ODataQueryOptions {
  public filter?: string = '';
  public orderby?: string = '';
  public skip?: number = 0;
  public top?: number = 0;

  getParameters(): HttpParams {
    let params = new HttpParams();
    if (this.filter) params.append("$filter", this.filter);
    if (this.orderby) params.append("$orderby", this.orderby);
    if (this.top) params.append("$top", this.top.toString());
    if (this.skip) params.append("$skip", this.skip.toString());
    return params;
  }

  toString(): string {
    let params = "?";
    if (this.filter) params += `$filter=${this.filter}&`;
    if (this.orderby) params += `$orderby=${this.orderby}&`;
    if (this.top) params += `$top=${this.top}&`;
    if (this.skip) params += `$skip=${this.skip}&`;
    return params;
  }
}
