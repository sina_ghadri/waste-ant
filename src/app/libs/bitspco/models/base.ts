import { prop } from '@rxweb/reactive-form-validators';

export class BaseDto {
    
}

export class BaseGetDto extends BaseDto {
    @prop()
    Id: number = 0;

    CreatorUserId?: string = '';
    CreationTime: string = '';
    LastModifierUserId?: string = '';
    LastModificationTime?: string = '';

    toClient(): any {
        return this;
    }
}
export class BaseAddDto extends BaseDto {

    toServer(): any {
        return this;
    }
}
export class BaseChangeDto extends BaseDto {

    toServer(): any {
        return this;
    }
}