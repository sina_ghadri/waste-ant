import { CommonModule, registerLocaleData } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { ServiceWorkerModule } from "@angular/service-worker";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";


import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";
import { NgZorroAntdModule, NZ_I18N, fa_IR } from 'ng-zorro-antd';

import { environment } from "../environments/environment";
import { HotkeyModule } from 'angular2-hotkeys';
import { HttpService, HttpConfigService } from "./services";
import { LayoutPanelComponent } from "./layouts/panel/panel.component";
import { CookieService } from 'ngx-cookie-service';
import { AuthGuard } from "./libs/identity/guards/auth.guard";
import { IconsProviderModule } from './icons-provider.module';

import fa from '@angular/common/locales/fa';

registerLocaleData(fa);

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    BrowserModule,
    HttpClientModule,
    NgZorroAntdModule,
    IconsProviderModule,
    BrowserAnimationsModule,

    AppRoutingModule,
    HotkeyModule.forRoot(),

    ServiceWorkerModule.register("/ngsw-worker.js", {
      enabled: environment.production
    })
  ],
  declarations: [AppComponent, LayoutPanelComponent],
  providers: [
    HttpConfigService,
    AuthGuard,
    CookieService,
    { provide: NZ_I18N, useValue: fa_IR },
    { provide: HTTP_INTERCEPTORS, useClass: HttpService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
