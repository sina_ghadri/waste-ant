import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { PlaceService } from 'src/app/services';
import { TreeExtension } from '../../libs/bitspco/components/base/tree.extension';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { BaseTreeComponent } from '../../libs/bitspco/components/base/tree.component';
import { PlaceGetDto } from 'src/app/models';
import { NzTreeNode } from 'ng-zorro-antd';


@Component({
    selector: 'app-choose-place',
    templateUrl: './choose-place.component.html',
    styleUrls: ['./choose-place.component.scss']
})
export class ChoosePlaceComponent extends BaseTreeComponent implements OnInit {
    @Output() oncomplete: EventEmitter<any> = new EventEmitter;

    ext: TreeExtension = new TreeExtension;
    
    constructor(private placeService: PlaceService, formBuilder: RxFormBuilder) {
        super(placeService, formBuilder);
    }
    ngOnInit(): void {
        super.ngOnInit();
        this.load();
    }
    
    createTreeNode(item: PlaceGetDto): NzTreeNode {
        return new NzTreeNode({
            key: item.Id.toString(),
            title: item.Name,
        });;
    }
}