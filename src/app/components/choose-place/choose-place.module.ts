import { NgModule } from "@angular/core";
import { ChoosePlaceComponent } from './choose-place.component';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { ChooseTreeModule } from '../choose-tree/choose-tree.module';

@NgModule({
    imports: [
        CommonModule,
        NgZorroAntdModule,
        ChooseTreeModule
    ],
    exports: [ChoosePlaceComponent],
    declarations: [ChoosePlaceComponent]
})
export class ChoosePlaceModule {}