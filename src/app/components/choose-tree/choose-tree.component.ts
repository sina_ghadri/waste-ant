import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { NzFormatEmitEvent, NzTreeNode } from 'ng-zorro-antd';
import { TreeExtension } from '../../libs/bitspco/components/base/tree.extension';


@Component({
    selector: 'app-choose-tree',
    templateUrl: './choose-tree.component.html',
    styleUrls: ['./choose-tree.component.scss']
})
export class ChooseTreeComponent extends TreeExtension implements OnInit {
    @Input() searchValue = '';
    @Input() nodes: NzTreeNode[];
    
    @Output() oncomplete: EventEmitter<any> = new EventEmitter;
    
    selectedNode: NzTreeNode;

    constructor() {
        super();
    }
    ngOnInit(): void {
    }
    onClick(e: NzFormatEmitEvent) {
        this.selectedNode = e.node;
    }
    onDblClick(e: NzFormatEmitEvent) {
        this.oncomplete.emit(e.node.origin);
    }
    onDrop(e) {

    }
    onExpandChange(e) {

    }
    onSearchValueChange(e: NzFormatEmitEvent) {
        this.openNodeByKeys(this.nodes, this.searchInTree(this.nodes, this.searchValue));
    }
}