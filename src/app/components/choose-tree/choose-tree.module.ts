import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { ChooseTreeComponent } from './choose-tree.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgZorroAntdModule
    ],
    exports: [ChooseTreeComponent],
    declarations: [ChooseTreeComponent],
})
export class ChooseTreeModule {}