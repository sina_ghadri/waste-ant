import { Component, OnInit, Input, ElementRef, Output, EventEmitter, OnChanges, SimpleChanges } from "@angular/core";
import { IMapItem } from 'src/app/models';

@Component({
    selector: 'app-map-viewer',
    templateUrl: './map-viewer.component.html',
    styleUrls: ['./map-viewer.component.scss']
})
export class MapViewerComponent implements OnInit, OnChanges {
    @Input() isLoading: boolean;
    @Input() src: string;
    @Input() scale: number;
    @Input() items: IMapItem[];

    @Output() onloadstart: EventEmitter<any> = new EventEmitter;
    @Output() onloadend: EventEmitter<any> = new EventEmitter;

    get svg(): HTMLElement { return this.element.nativeElement.querySelector('.items-layer>svg'); }
    get svgWidth(): number { return this.svg ? this.svg.clientWidth : 0; }
    get svgHeight(): number { return this.svg ? this.svg.clientHeight : 0; }

    get image(): HTMLImageElement { return this.element.nativeElement.querySelector('.items-layer>img'); }
    get imageWidth(): number { return this.image ? this.image.naturalWidth : 0; }
    get imageHeight(): number { return this.image ? this.image.naturalHeight : 0; }

    constructor(
        private element: ElementRef
    ) { }

    ngOnInit(): void {

    }
    ngOnChanges(changes: SimpleChanges): void {
        if (changes.src) {
            if (this.src) this.onloadstart.emit();
        }
    }
}