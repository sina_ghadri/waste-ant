import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { MapViewerComponent } from './map-viewer.component';
import { SafeHtmlModule } from 'src/app/libs/bitspco/pipes/safe.pipe';
import { NgZorroAntdModule } from 'ng-zorro-antd';

@NgModule({
    imports: [
        CommonModule,
        NgZorroAntdModule,

        SafeHtmlModule,
    ],
    declarations: [
        MapViewerComponent
    ],
    exports: [
        MapViewerComponent
    ]
})
export class MapViewerModule {}