import { Component, Input, Output, EventEmitter, TemplateRef, ViewChild, OnChanges, SimpleChanges } from "@angular/core";
import { NzTreeNode, NzFormatEmitEvent, NzTreeComponent } from "ng-zorro-antd";
import { OperationResultType } from 'src/app/models';
import { TreeExtension } from 'src/app/libs/bitspco/components/base/tree.extension';

enum Modes {
  None = '',
  Creating = 'create',
  Editing = 'edit',
  Detailing = 'detail'
}

@Component({
  selector: "app-layout-tree",
  templateUrl: "./tree.component.html",
  styleUrls: ["./tree.component.scss"]
})
export class LayoutTreeComponent extends TreeExtension implements OnChanges {
  @Input() searchValue = '';
  @Input() nodes: NzTreeNode[];
  @Input() isLoading: boolean = true;

  @Input() createTpl: TemplateRef<any>;
  @Input() editTpl: TemplateRef<any>;
  @Input() detailTpl: TemplateRef<any>;
  @Input() removable: boolean = true;

  @Output() onCreate: EventEmitter<any> = new EventEmitter;
  @Output() onCreated: EventEmitter<any> = new EventEmitter;
  @Output() onDetail: EventEmitter<any> = new EventEmitter;
  @Output() onEdit: EventEmitter<any> = new EventEmitter;
  @Output() onEdited: EventEmitter<any> = new EventEmitter;
  @Output() onRemove: EventEmitter<any> = new EventEmitter;
  @Output() onReload: EventEmitter<any> = new EventEmitter;

  @Output() nzClick: EventEmitter<any> = new EventEmitter;
  @Output() nzOnDrop: EventEmitter<any> = new EventEmitter;
  @Output() nzExpandChange: EventEmitter<any> = new EventEmitter;
  @Output() nzSearchValueChange: EventEmitter<any> = new EventEmitter;

  @ViewChild(NzTreeComponent, { static: false }) tree: NzTreeComponent;


  mode: Modes = Modes.None;
  message: string;
  get selectedNode(): NzTreeNode { return (this.nodes ? this.getSelectedNode(this.nodes) : null); }
  get selectedValue(): any {
   return this.selectedNode ? this.selectedNode.origin.data : null; };

  constructor() {
    super();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.nodes) {
      this.clearSelectedNode(this.nodes);
      this.mode = Modes.None;
    }
  }
  reload() {
    this.onReload.emit();
  }

  create() {
    this.onCreate.emit(this.selectedValue);
    this.mode = Modes.Creating;
  }
  created(op: OperationResultType<any>, key: string) {
    if (op) {
      this.message = op.Message;
    }
    setTimeout(() => {
      if (key) {
        this.getSelectedNodeByKey(this.nodes, key);
        this.edit();
      }
      else this.detail();
    }, 1);
  }
  edit() {
    if (!this.selectedValue) return;
    this.onEdit.emit(this.selectedValue);
    this.mode = Modes.Editing;
  }
  edited(op: OperationResultType<any>) {
    if (op) {
      if (op.Success) {
        this.getSelectedNodeByKey(this.nodes, op.Data.Id);
      }
      this.message = op.Message;
    }
    this.detail();
  }
  detail() {
    if (!this.selectedValue) {
      this.mode = Modes.None;
      return;
    }
    this.onDetail.emit(this.selectedValue);
    this.mode = Modes.Detailing;
  }
  detailed() {

  }
  removed(op: OperationResultType<any>) {
    if (op) {
      if (op.Success) this.clearSelectedNode(this.nodes);
      this.message = op.Message;
    }
  }
  onClick(e: NzFormatEmitEvent) {
    this.clearSelectedNode(this.nodes);
    e.node.isSelected = true;
    this.detail();
    this.nzClick.emit(e);
  }
  onDblClick(e: NzFormatEmitEvent) {
    e.node.isExpanded = !e.node.isExpanded;
  }
  onDrop(e) {

  }
  onExpandChange(e) {

  }
  onSearchValueChange(e: NzFormatEmitEvent) {
    this.openNodeByKeys(this.nodes, this.searchInTree(this.nodes, this.searchValue));
  }
}
