import { NgModule } from "@angular/core";

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { LayoutTreeComponent } from './tree.component';

@NgModule({
    imports: [CommonModule, FormsModule, NgZorroAntdModule],
    exports: [LayoutTreeComponent],
    declarations: [LayoutTreeComponent]
})
export class LayoutTreeModule {
    
}