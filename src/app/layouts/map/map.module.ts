import { NgModule } from "@angular/core";

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { LayoutMapComponent } from './map.component';
import { MapViewerModule } from 'src/app/components/map-viewer/map-viewer.module';

@NgModule({
    imports: [CommonModule, FormsModule, NgZorroAntdModule, MapViewerModule],
    exports: [LayoutMapComponent],
    declarations: [LayoutMapComponent]
})
export class LayoutMapModule {

}