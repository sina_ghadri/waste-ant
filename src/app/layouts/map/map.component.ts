import { Component, OnChanges, SimpleChanges, OnInit, ElementRef, Input, ViewChild, Output, EventEmitter } from "@angular/core";
import { PlaceGetDto, StationGetDto, ElementGetDto, PlaceElementGetDto, RouteGetDto, RouteItemGetDto, CleaningAreaGetDto, ElevatorGetDto, IMapItem, Element, Place, Station, Point, CleaningArea, RouteItem, Route, Cordination, Elevator, Map, MapColors, MapItemEvent } from 'src/app/models';
import { HotkeysService } from 'angular2-hotkeys';
import { NzModalService } from 'ng-zorro-antd';
import { PlaceElementService, CleaningAreaService, RouteService, ElementService, StationService, PlaceService } from 'src/app/services';
import { MapViewerComponent } from 'src/app/components/map-viewer/map-viewer.component';

enum Modes {
  None = '',
  Creating = 'create',
  Editing = 'edit',
  Detailing = 'detail'
}

@Component({
  selector: "app-layout-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.scss"]
})
export class LayoutMapComponent implements OnInit, OnChanges {
  private _place: PlaceGetDto;
  @Input()
  get place(): PlaceGetDto { return this._place; }
  set place(value: PlaceGetDto) {
    this._place = value;
    if (value) {
      if (value.Image) this.src = value.Image;
      else if (value.Parent && value.Parent.Image) {
        this.src = this.placeService.getDynamicImageUrl(value.Parent.Id, value.Cordination);
      }
    }
  }

  @Input('place-class') placeClass: any = 'col-md-2 col-sm-3 col-xs-4 col-xxs-12';

  @Input() enablePlaces: boolean = false;
  @Input() enableStations: boolean = false;
  @Input() enableElements: boolean = false;
  @Input() enableRoutes: boolean = false;
  @Input() enableRoutItems: boolean = false;
  @Input() enableCleaningAreas: boolean = false;
  @Input() enableElevators: boolean = false;

  @Output() onplace: EventEmitter<Place> = new EventEmitter;
  @Output() onstation: EventEmitter<Station> = new EventEmitter;
  @Output() oncleaningArea: EventEmitter<CleaningArea> = new EventEmitter;
  @Output() onroute: EventEmitter<Route> = new EventEmitter;
  @Output() onelement: EventEmitter<Element> = new EventEmitter;
  @Output() onelevator: EventEmitter<Elevator> = new EventEmitter;

  @ViewChild(MapViewerComponent, { static: false }) map: MapViewerComponent;

  scale: number = 1;
  scales: number[] = [0.25, 0.50, 0.75, 1, 1.25, 1.50, 1.75, 2, 2.5, 3, 4];

  isLoading: boolean;
  src: string;
  items: IMapItem[];
  selectedItem: IMapItem;
  selectedPlace: PlaceGetDto;
  selectedPlaces: PlaceGetDto[] = [];

  private allPlaces: PlaceGetDto[];
  private places: PlaceGetDto[];
  private stations: StationGetDto[];
  private elements: ElementGetDto[];
  private placeElements: PlaceElementGetDto[];
  private routes: RouteGetDto[];
  private routeItems: RouteItemGetDto[];
  private cleaningAreas: CleaningAreaGetDto[];
  private elevators: ElevatorGetDto[];

  get LastSelectedPlace(): PlaceGetDto { return (this.selectedPlaces.length > 0 ? this.selectedPlaces[this.selectedPlaces.length - 1] : null); }

  get Places(): PlaceGetDto[] { return this.places ? this.places : []; }
  get PlacesCordinated(): PlaceGetDto[] { return this.Places.filter(x => x.Cordination); }
  get PlacesNotCordinate(): PlaceGetDto[] { return this.Places.filter(x => !x.Cordination); }
  set Places(value: PlaceGetDto[]) { this.places = value; this.items = this.getItems(); }

  get Stations(): StationGetDto[] { return this.stations ? this.stations : []; }
  get StationsCordinated(): StationGetDto[] { return this.Stations.filter(x => x.Cordination); }
  get StationsNotCordinate(): StationGetDto[] { return this.Stations.filter(x => !x.Cordination); }
  set Stations(value: StationGetDto[]) { this.stations = value; this.items = this.getItems(); }

  get Elements(): ElementGetDto[] { return this.elements ? this.elements : []; }
  get ElementsCordinated(): PlaceElementGetDto[] { return this.PlaceElementsCordinated.filter(x => x.Cordination); }
  set Elements(value: ElementGetDto[]) { this.elements = value; this.items = this.getItems(); }

  get PlaceElements(): PlaceElementGetDto[] { return this.placeElements ? this.placeElements.filter(x => x.Cordination) : []; }
  get PlaceElementsCordinated(): PlaceElementGetDto[] { return this.PlaceElements.filter(x => x.Cordination); }
  set PlaceElements(value: PlaceElementGetDto[]) { this.placeElements = value; this.items = this.getItems(); }

  get CleaningAreas(): CleaningAreaGetDto[] { return this.cleaningAreas ? this.cleaningAreas.filter(x => x.Cordination) : []; }
  get CleaningAreasCordinated(): CleaningAreaGetDto[] { return this.CleaningAreas.filter(x => x.Cordination); }
  get CleaningAreasNotCordinate(): CleaningAreaGetDto[] { return this.CleaningAreas.filter(x => !x.Cordination); }
  set CleaningAreas(value: CleaningAreaGetDto[]) { this.cleaningAreas = value; this.items = this.getItems(); }

  get Elevators(): ElevatorGetDto[] { return this.elevators ? this.elevators : []; }
  set Elevators(value: ElevatorGetDto[]) { this.elevators = value; this.items = this.getItems(); }

  get Routes(): RouteGetDto[] { return this.routes ? this.routes : []; }
  set Routes(value: RouteGetDto[]) { this.routes = value; this.items = this.getItems(); }

  get RouteItems(): RouteItemGetDto[] { return this.routeItems ? this.routeItems : []; }
  set RouteItems(value: RouteItemGetDto[]) { this.routeItems = value; this.items = this.getItems(); }

  constructor(
    private element: ElementRef,
    private placeService: PlaceService,
    private stationService: StationService,
    private elementService: ElementService,
    private routeService: RouteService,
    private cleaningAreaService: CleaningAreaService,
    private placeElementService: PlaceElementService,
    private modalService: NzModalService,
    private hotkeysService: HotkeysService
  ) { }

  ngOnInit(): void {

    this.placeService.getAll().subscribe(op => {
      this.allPlaces = op.Data;
      this.loadItems();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.place) {
      this.reset();
    }

    this.loadItems();
    this.items = this.getItems();
  }
  reset() {
    this.places = null;
    this.stations = null;
    this.placeElements = null;
    this.routes = null;
    this.routeItems = null;
    this.cleaningAreas = null;
    this.elevators = null;
  }
  onloadstart() {
    setTimeout(() => this.isLoading = true);
  }
  onloadend() {
    setTimeout(() => this.isLoading = false);
    this.items = this.getItems();
  }
  loadItems() {
    if (!this.places && this.allPlaces) {
      this.Places = this.allPlaces.filter(x => x.ParentId == (this.place && this.place.Id ? this.place.Id : null));
    }
    if (this.place) {
      if (this.enableStations && !this.stations) this.placeService.getStations(this.place.Id).subscribe(op => this.Stations = op.Data.filter(x => x.PlaceId == this.place.Id));
      if (this.enableElements && !this.placeElements) this.placeService.getElements(this.place.Id).subscribe(op => this.PlaceElements = op.Data.filter(x => x.PlaceId == this.place.Id));
      if (this.enableRoutes && !this.routes) {
        this.placeService.getRoutes(this.place.Id).subscribe(op => this.Routes = op.Data.filter(x => x.PlaceId == this.place.Id));
        this.placeService.getRoutItems(this.place.Id).subscribe(op => this.RouteItems = op.Data.filter(x => x.Station.PlaceId == this.place.Id));
      }
      if (this.enableCleaningAreas && !this.cleaningAreas) this.placeService.getCleaningAreas(this.place.Id).subscribe(op => this.CleaningAreas = op.Data.filter(x => x.PlaceId == this.place.Id));
      if (this.enableElevators && !this.elevators) this.placeService.getElevators(this.place.Id).subscribe(op => this.Elevators = op.Data.filter(x => x.PlaceId == this.place.Id));
    }
  }
  placeCompare(o1: PlaceGetDto, o2: PlaceGetDto) { return (o1 && o2 && o1.Id === o2.Id); }
  selectPlace(value?: PlaceGetDto) {
    this.place = value;

    this.reset();
    this.loadItems();

    if (this.place) {
      this.selectedPlaces.push(this.place);
    }
    setTimeout(() => this.selectedPlace = null);
  }
  selectPrePlace() {
    this.reset();
    this.selectedPlaces.pop();
    this.place = this.LastSelectedPlace;
    this.selectedPlace = null;
    this.loadItems();
  }
  initialItem(item: IMapItem) {
    if (this.selectedItem && item.getKey() == this.selectedItem.getKey()) item.isSelected = true;
    item.onclick.subscribe((e: MapItemEvent<Station>) => {
      this.selectedItem = e.item;
      this.items = this.getItems();
    });
  }
  getItems(): IMapItem[] {
    if (!this.isLoading) {
      const map = new Map();
      if (this.map) {
        map.width = this.map.svgWidth;
        map.height = this.map.svgHeight;

        const places = this.enablePlaces ? this.PlacesCordinated.map(x => {
          const item = new Place(x, Cordination.toRectangle(x.Cordination, this.map.imageWidth, this.map.imageHeight));
          item.map = map;
          this.initialItem(item);
          item.onclick.subscribe((e: MapItemEvent<Place>) => this.selectPlace(e.item.obj))
          this.onplace.emit(item);
          return item;
        }) : [];
        const stations = this.enableStations ? this.StationsCordinated.map(x => {
          const item = new Station(x, new Point(x.Cordination.X, x.Cordination.Y));
          item.map = map;
          this.initialItem(item);
          this.onstation.emit(item);
          return item;
        }) : [];
        const elements = this.enableElements ? this.PlaceElementsCordinated.map(x => {
          x.Element = this.Elements.find(y => y.Id == x.ElementId);
          x.Place = this.Places.find(y => y.Id == x.PlaceId);
          const item = new Element(x, new Point(x.Cordination.X, x.Cordination.Y));
          item.map = map;
          this.initialItem(item);
          this.onelement.emit(item);
          return item;
        }) : [];
        const cleaningAreas = this.enableCleaningAreas ? this.CleaningAreasCordinated.map(x => {
          const item = new CleaningArea(x, Cordination.toRectangle(x.Cordination, this.map.imageWidth, this.map.imageHeight));
          item.map = map;
          this.initialItem(item);
          this.oncleaningArea.emit(item);
          return item;
        }) : [];
        const routes = this.enableRoutes ? this.Routes.map((x, i) => {
          const item = new Route(x);
          item.color = MapColors[i % 11];
          item.map = map;
          this.initialItem(item);
          const routeItems = this.RouteItems.filter(y => y.RouteId == x.Id);
          for (let i = 0; i < routeItems.length; i++) {
            const routeItem = routeItems[i];
            const station = stations.find(y => y.obj.Id == routeItem.StationId);
            if (station) item.items.push(new RouteItem(item, station, i));
          }
          this.onroute.emit(item);
          return item;
        }) : [];

        return [].concat(cleaningAreas, routes, places, stations, elements);
      }
    }
    return [];
  }
  zoomIn() {
    let index = this.scales.indexOf(this.scale);
    if (index + 1 < this.scales.length) this.scale = this.scales[index + 1];
    this.zoomChange();
  }
  zoomOut() {
    let index = this.scales.indexOf(this.scale);
    if (index > 0) this.scale = this.scales[index - 1];
    this.zoomChange();
  }
  zoomChange() {
    setTimeout(() => this.items = this.getItems());
  }
  onload() {
    this.isLoading = false;
    this.items = this.getItems();
  }
}
