import { Component, OnInit, ElementRef } from "@angular/core";
import { ConfigurationService, AuthService } from "../../services";

import { SwUpdate } from "@angular/service-worker";
import { Router, NavigationEnd } from "@angular/router";
import { ModuleGetDto, UserGetDto } from 'src/app/models';

@Component({
  selector: "app-layout-panel",
  styleUrls: ["./panel.component.scss"],
  templateUrl: "./panel.component.html"
})
export class LayoutPanelComponent implements OnInit {
  update: boolean = false;

  title: string;
  logo: string;
  user: UserGetDto;
  menus: any[];
  isCollapsed: boolean;
  showModules: boolean;

  breadcrumbs: string[];
  modules: ModuleGetDto[];

  public constructor(
    private router: Router,
    private element: ElementRef,
    private authService: AuthService,
    private configurationService: ConfigurationService,
    updates: SwUpdate
  ) {
    updates.available.subscribe(event => {
      this.update = true;
      updates.activateUpdate().then(() => document.location.reload());
    });
  }
  ngOnInit(): void {
    const config = this.configurationService.configuration;
    this.title = config.title;
    this.logo = config.logo;
    this.menus = config.menus;

    this.authService.getLoginInfo().subscribe(loginInfo => {
      this.user = loginInfo.User;
      this.modules = loginInfo.Modules;
    });

    setTimeout(() => this.loadBreadcrumbs(), 250);

    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        setTimeout(() => this.loadBreadcrumbs(), 250);
      }
    })
  }
  loadBreadcrumbs() {
    let menu = this.element.nativeElement.querySelector('.ant-menu-item.ant-menu-item-selected>a') as HTMLElement;
    this.breadcrumbs = [];
    while (menu) {
      if (menu.tagName.toLowerCase() == 'li') {
        const span = menu.querySelector('span');
        const a = menu.querySelector('a');
        this.breadcrumbs.unshift(span ? span.innerText : a.innerText);
      }
      if (menu.tagName.toLowerCase() == 'nz-sider') break;
      menu = menu.parentNode as HTMLElement;
    }
  }
  openHandler(obj: any): void {
    for (let i = 0; i < this.menus.length; i++) {
      const menu = this.menus[i];
      menu.isOpen = false;
    }
    obj.isOpen = true;
  }
  toggleModule() {
    this.showModules = !this.showModules;
  }
  logout() {
    this.authService.logout().subscribe(op => {
      if (op.Success) location.href = this.configurationService.configuration.login;
    });
  }
}
