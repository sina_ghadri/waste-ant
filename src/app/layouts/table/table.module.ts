import { NgModule } from "@angular/core";

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { LayoutTableComponent } from './table.component';

@NgModule({
    imports: [CommonModule, FormsModule, NgZorroAntdModule],
    exports: [LayoutTableComponent],
    declarations: [LayoutTableComponent]
})
export class LayoutTableModule {
    
}