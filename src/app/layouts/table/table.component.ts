import { Component, Input, Output, EventEmitter, TemplateRef, OnInit } from "@angular/core";
import { FormGroup } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { BaseGetDto } from 'src/app/models';

enum States {
  None = '',
  Create = 'create',
  Edit = 'edit',
  Detail = 'detail',
}
enum Modes {
  None = '',
  Modal = 'modal'
}

@Component({
  selector: "app-layout-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.scss"]
})
export class LayoutTableComponent implements OnInit {
  @Input() form: FormGroup;

  @Input() createTpl: TemplateRef<any>;
  @Input() detailTpl: TemplateRef<any>;
  @Input() editTpl: TemplateRef<any>;
  @Input() modalWidth: string = '544px';

  @Output() onReload: EventEmitter<any> = new EventEmitter;
  @Output() onCreate: EventEmitter<any> = new EventEmitter;
  @Output() onCreated: EventEmitter<any> = new EventEmitter;
  @Output() onDetail: EventEmitter<any> = new EventEmitter;
  @Output() onDetailed: EventEmitter<any> = new EventEmitter;
  @Output() onEdit: EventEmitter<any> = new EventEmitter;
  @Output() onEdited: EventEmitter<any> = new EventEmitter;

  get createContext(): any { return { $implicit: this.selectedItem, id: this.id, onComplete: this.create } };
  get detailContext(): any { return { $implicit: this.selectedItem, id: this.id, onComplete: this.detail } };
  get editContext(): any { return { $implicit: this.selectedItem, id: this.id, onComplete: this.edit } };

  id: number;
  selectedItem: any;

  state: States;
  mode: Modes = Modes.None;

  createModal: NzModalRef;
  editModal: NzModalRef;
  detailModal: NzModalRef;

  constructor(private modalService: NzModalService) {

  }
  ngOnInit(): void {
    if(this.createTpl) this.state = States.Create;
  }
  showList() {
    this.state = States.None;
  }
  create() {
    this.state = States.Create;
    if (this.mode == Modes.Modal) {
      this.createModal = this.modalService.create({
        nzWidth: this.modalWidth,
        nzTitle: 'افزودن ردیف جدید',
        nzContent: this.createTpl,
        nzFooter: null,
      });
    }
  }
  created(op) {
    if (op) this.onReload.emit();
    if (this.mode == Modes.Modal) {
      this.createModal.destroy();
    }
    this.state = States.None;
  }
  detail(item: BaseGetDto) {
    this.id = item.Id;
    this.selectedItem = item;
    this.state = States.Detail;
    if (this.mode == Modes.Modal) {
      this.detailModal = this.modalService.create({
        nzWidth: this.modalWidth,
        nzTitle: 'جزئیات',
        nzContent: this.detailTpl,
        nzFooter: null
      });
    }
  }
  detailed() {
    if (this.mode == Modes.Modal) {
      this.detailModal.destroy();
    }
    this.state = States.None;
  }
  edit(item: BaseGetDto) {
    this.id = item.Id;
    this.selectedItem = item;
    this.state = States.Edit;
    if (this.mode == Modes.Modal) {
      this.editModal = this.modalService.create({
        nzWidth: this.modalWidth,
        nzTitle: 'ویرایش',
        nzContent: this.editTpl,
        nzFooter: null
      });
    }
  }
  edited(op) {
    if (op) this.onReload.emit();
    if (this.mode == Modes.Modal) {
      this.editModal.destroy();
    }
    this.state = States.None;
  }
}
