import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { LayoutPanelComponent } from './layouts/panel/panel.component';
import { AuthGuard } from './libs/identity/guards/auth.guard';

const routes: Route[] = [
  {
    path: '', component: LayoutPanelComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      { path: 'account', loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule) },

      { path: 'calendar', loadChildren: () => import('./pages/calendar/calendar.module').then(m => m.CalendarModule) },
      { path: 'cleaning-area', loadChildren: () => import('./pages/cleaning-area/cleaning-area.module').then(m => m.CleaningAreaModule) },
      { path: 'cleaning-area-equipment', loadChildren: () => import('./pages/cleaning-area-equipment/cleaning-area-equipment.module').then(m => m.CleaningAreaEquipmentModule) },
      { path: 'container', loadChildren: () => import('./pages/container/container.module').then(m => m.ContainerModule) },
      { path: 'container-type', loadChildren: () => import('./pages/container-type/container-type.module').then(m => m.ContainerTypeModule) },
      { path: 'dashboard', loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule) },
      { path: 'day-part', loadChildren: () => import('./pages/day-part/day-part.module').then(m => m.DayPartModule) },
      { path: 'day-part-name', loadChildren: () => import('./pages/day-part-name/day-part-name.module').then(m => m.DayPartNameModule) },
      { path: 'day-part-schedule', loadChildren: () => import('./pages/day-part-schedule/day-part-schedule.module').then(m => m.DayPartScheduleModule) },
      { path: 'day-part-schedule-item', loadChildren: () => import('./pages/day-part-schedule-item/day-part-schedule-item.module').then(m => m.DayPartScheduleItemModule) },
      { path: 'day-type', loadChildren: () => import('./pages/day-type/day-type.module').then(m => m.DayTypeModule) },
      { path: 'draft-exit', loadChildren: () => import('./pages/draft-exit/draft-exit.module').then(m => m.DraftExitModule) },
      { path: 'draft-exit-type', loadChildren: () => import('./pages/draft-exit-type/draft-exit-type.module').then(m => m.DraftExitTypeModule) },
      { path: 'element', loadChildren: () => import('./pages/element/element.module').then(m => m.ElementModule) },
      { path: 'elevator', loadChildren: () => import('./pages/elevator/elevator.module').then(m => m.ElevatorModule) },
      { path: 'equipment', loadChildren: () => import('./pages/equipment/equipment.module').then(m => m.EquipmentModule) },
      { path: 'index', loadChildren: () => import('./pages/index/index.module').then(m => m.IndexModule) },
      { path: 'inspection', loadChildren: () => import('./pages/inspection/inspection.module').then(m => m.InspectionModule) },
      { path: 'place', loadChildren: () => import('./pages/place/place.module').then(m => m.PlaceModule) },
      { path: 'place-editor', loadChildren: () => import('./pages/place-editor/place-editor.module').then(m => m.PlaceEditorModule) },
      { path: 'place-element', loadChildren: () => import('./pages/place-element/place-element.module').then(m => m.PlaceElementModule) },
      { path: 'receipt-discharge', loadChildren: () => import('./pages/receipt-discharge/receipt-discharge.module').then(m => m.ReceiptDischargeModule) },
      { path: 'receipt-discharge-item', loadChildren: () => import('./pages/receipt-discharge-item/receipt-discharge-item.module').then(m => m.ReceiptDischargeItemModule) },
      { path: 'receipt-swap', loadChildren: () => import('./pages/receipt-swap/receipt-swap.module').then(m => m.ReceiptSwapModule) },
      { path: 'receipt-swap-item', loadChildren: () => import('./pages/receipt-swap-item/receipt-swap-item.module').then(m => m.ReceiptSwapItemModule) },
      { path: 'request', loadChildren: () => import('./pages/request/request.module').then(m => m.RequestModule) },
      { path: 'request-cleaning', loadChildren: () => import('./pages/request-cleaning/request-cleaning.module').then(m => m.RequestCleaningModule) },
      { path: 'request-discharge', loadChildren: () => import('./pages/request-discharge/request-discharge.module').then(m => m.RequestDischargeModule) },
      { path: 'request-elevator', loadChildren: () => import('./pages/request-elevator/request-elevator.module').then(m => m.RequestElevatorModule) },
      { path: 'request-equipment', loadChildren: () => import('./pages/request-equipment/request-equipment.module').then(m => m.RequestEquipmentModule) },
      { path: 'request-history', loadChildren: () => import('./pages/request-history/request-history.module').then(m => m.RequestHistoryModule) },
      { path: 'request-leave', loadChildren: () => import('./pages/request-leave/request-leave.module').then(m => m.RequestLeaveModule) },
      { path: 'request-swap', loadChildren: () => import('./pages/request-swap/request-swap.module').then(m => m.RequestSwapModule) },
      { path: 'route', loadChildren: () => import('./pages/route/route.module').then(m => m.RouteModule) },
      { path: 'route-item', loadChildren: () => import('./pages/route-item/route-item.module').then(m => m.RouteItemModule) },
      { path: 'setting', loadChildren: () => import('./pages/setting/setting.module').then(m => m.SettingModule) },
      { path: 'shift', loadChildren: () => import('./pages/shift/shift.module').then(m => m.ShiftModule) },
      { path: 'shift-part', loadChildren: () => import('./pages/shift-part/shift-part.module').then(m => m.ShiftPartModule) },
      { path: 'shift-part-name', loadChildren: () => import('./pages/shift-part-name/shift-part-name.module').then(m => m.ShiftPartNameModule) },
      { path: 'shift-role', loadChildren: () => import('./pages/shift-role/shift-role.module').then(m => m.ShiftRoleModule) },
      { path: 'shift-schedule', loadChildren: () => import('./pages/shift-schedule/shift-schedule.module').then(m => m.ShiftScheduleModule) },
      { path: 'shift-schedule-item', loadChildren: () => import('./pages/shift-schedule-item/shift-schedule-item.module').then(m => m.ShiftScheduleItemModule) },
      { path: 'station', loadChildren: () => import('./pages/station/station.module').then(m => m.StationModule) },
      { path: 'use-type', loadChildren: () => import('./pages/use-type/use-type.module').then(m => m.UseTypeModule) },
      { path: 'use-type-index', loadChildren: () => import('./pages/use-type-index/use-type-index.module').then(m => m.UseTypeIndexModule) },
      { path: 'user', loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule) },
      { path: 'user-place', loadChildren: () => import('./pages/user-place/user-place.module').then(m => m.UserPlaceModule) },
      { path: 'user-use-type', loadChildren: () => import('./pages/user-use-type/user-use-type.module').then(m => m.UserUseTypeModule) },
      { path: 'waste-type', loadChildren: () => import('./pages/waste-type/waste-type.module').then(m => m.WasteTypeModule) },

    ]
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
