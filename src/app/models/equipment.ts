import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';

export class EquipmentGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
}
export class EquipmentAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
}
export class EquipmentChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
}