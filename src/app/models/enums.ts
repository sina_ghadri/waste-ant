export enum AuthorizationType {
  QueryString,
  Header
}
export enum CleaningType {
  Standard = 1,
  Minor = 2
}
export enum ContainerLocationType {
  None = 0,
  Station = 1,
  Place = 2,
  User = 3
}
export enum DayPartScheduleType {
  None = 0,
  NormalCleaning = 1,
  MinorCleaning = 2
}
export enum DocumentType {
  Receipt = 0,
  Draft = 1
}
export enum PlaceType {
  None = 0,
  Elevator = 1,
  Warehouse = 2
}
export enum ReceiptDischargeStatus {
  Draft = 1,
  InProcess = 2,
  InElevator = 3,
  Completed = 4
}
export enum ReceiptSwapStatus {
  Draft = 1,
  InProcess = 2,
  InElevator = 3,
  Completed = 4
}
export enum RequestGenerationType {
  Schedule = 1,
  User = 2
}
export enum RequestHistoryType {
  Confirm = 1,
  Reject = 2
}
export enum RequestLeaveType {
  None = 0
}
export enum RequestStatus {
  Draft = 1,
  Confirmed = 2,
  Done = 3,
  Canceled = 4,
  Failed = 5
}
export enum RequestType {
  Cleaning = 1,
  Discharge = 2,
  Elevator = 3,
  Equipment = 4,
  Leave = 5,
  Swap = 6
}
export enum RoleType {
  None = 0,
  User = 1,
  System = 2
}
export enum RouteStatus {
  Draft = 1,
  InProcess = 2,
  Completed = 3
}
export enum SystemCode {
  WasteManagement = 1
}
export enum TokenStatus {
  Valid = 1,
  Refresh = 2,
  Expired = 3
}
export enum Gender
{
    None = 0,
    Male = 1,
    Female = 2
}