import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { CleaningType } from './enums';
import { RequestGetDto } from './request';
import { CleaningAreaGetDto } from './cleaning-area';
import { DayPartScheduleGetDto } from './day-part-schedule';

export class RequestCleaningGetDto extends BaseGetDto {
    RequestId: number = 0;
    CleaningAreaId: number = 0;
    Type: CleaningType;
    DayPartScheduleId?: number = 0;

    Request: RequestGetDto;
    CleaningArea: CleaningAreaGetDto;
    DayPartSchedule: DayPartScheduleGetDto;
}
export class RequestCleaningAddDto extends BaseAddDto {
    RequestId: number = 0;
    Type: CleaningType;
}
export class RequestCleaningChangeDto extends BaseChangeDto {
    
}