import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { ReceiptDischargeStatus } from './enums';
import { UserGetDto } from '../libs/identity/models/user';
import { ContainerGetDto } from './container';
import { RouteGetDto } from './route';

export class ReceiptDischargeGetDto extends BaseGetDto {
    UserId: number = 0;
    ContainerId: number = 0;
    RouteId: number = 0;
    Weight: number = 0;
    Volume: number = 0;
    Time: string = '';
    StartCollectTime: string = '';
    EndCollectTime: string = '';
    PutNearElevatorTime: string = '';
    CompletedTime: string = '';
    Status: ReceiptDischargeStatus;

    User: UserGetDto;
    Container: ContainerGetDto;
    Route: RouteGetDto;
}
export class ReceiptDischargeAddDto extends BaseAddDto {
    UserId: number = 0;
    ContainerId: number = 0;
}
export class ReceiptDischargeChangeDto extends BaseChangeDto {
    ContainerId: number = 0;
    RouteId: number = 0;
    Weight: number = 0;
    Volume: number = 0;
    Time: string = '';
}