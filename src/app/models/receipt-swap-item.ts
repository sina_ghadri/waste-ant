import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { ReceiptSwapGetDto } from './receipt-swap';
import { StationGetDto } from './station';
import { ContainerGetDto } from './container';

export class ReceiptSwapItemGetDto extends BaseGetDto {
    ReceiptSwapId: number = 0;
    StationId: number = 0;
    GiveContainerId?: number = 0;
    TakeContainerId?: number = 0;

    ReceiptSwap: ReceiptSwapGetDto;
    Station: StationGetDto;
    GivContainer: ContainerGetDto;
    TakeContainer: ContainerGetDto;
}
export class ReceiptSwapItemAddDto extends BaseAddDto {
    ReceiptSwapId: number = 0;
    StationId: number = 0;
    GiveContainerId?: number = 0;
    TakeContainerId?: number = 0;
}
export class ReceiptSwapItemChangeDto extends BaseChangeDto {
    GiveContainerId?: number = 0;
    TakeContainerId?: number = 0;
}