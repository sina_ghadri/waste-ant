import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { ContainerLocationType } from './enums';
import { ContainerTypeGetDto } from './container-type';
import { PlaceGetDto } from './place';
import { StationGetDto } from './station';
import { UserGetDto } from '../libs/identity/models/user';

export class ContainerGetDto extends BaseGetDto {
    Code: number = 0;
    StartDate: string = '';
    EndDate: string = '';
    BuyTime?: string = '';
    ExpireTime?: string = '';
    ContainerTypeId: number = 0;
    PlaceId?: number = 0;
    StationId?: number = 0;
    DeliveredUserId?: number = 0;
    IsActive: boolean = false;

    LocationType: ContainerLocationType;

    ContainerType: ContainerTypeGetDto;
    Place: PlaceGetDto;
    Station: StationGetDto;
    DeliveredUser: UserGetDto;
}
export class ContainerAddDto extends BaseAddDto {
    Code: number = 0;
    StartDate: string = '';
    EndDate: string = '';
    BuyTime?: string = '';
    ExpireTime?: string = '';
    ContainerTypeId: number = 0;
    PlaceId?: number = 0;
    StationId?: number = 0;
    DeliveredUserId?: number = 0;
    IsActive: boolean = false;
}
export class ContainerChangeDto extends BaseChangeDto {
    Code: number = 0;
    StartDate: string = '';
    EndDate: string = '';
    BuyTime?: string = '';
    ExpireTime?: string = '';
    ContainerTypeId: number = 0;
    PlaceId?: number = 0;
    StationId?: number = 0;
    DeliveredUserId?: number = 0;
    IsActive: boolean = false;
}