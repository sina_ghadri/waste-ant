import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';

export class UseTypeGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    ParentId?: number = 0;

    Parent: UseTypeGetDto;
}
export class UseTypeAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    ParentId?: number = 0;
}
export class UseTypeChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    ParentId?: number = 0;
}