import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { UserUseTypeGetDto } from './user-use-type';
import { PlaceGetDto } from './place';
import { UserGetDto } from '../libs/identity/models/user';
import { prop } from '@rxweb/reactive-form-validators';
import { Cordination } from './map';

export class StationGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    UseTypeId: number = 0;
    @prop()
    PlaceId: number = 0;
    @prop()
    ConnectiveUserId?: number = null;
    @prop()
    Tag: string = '';
    @prop()
    Grade: number = 0;
    @prop()
    Space?: number = null;
    @prop()
    IsActive: boolean = false;

    @prop()
    Cordination?: Cordination = new Cordination;

    UserType: UserUseTypeGetDto;
    Place: PlaceGetDto;
    ConnectiveUser: UserGetDto;
}
export class StationAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    UseTypeId: number = 0;
    @prop()
    PlaceId: number = 0;
    @prop()
    ConnectiveUserId?: number = null;
    @prop()
    Tag: string = '';
    @prop()
    Space?: number = null;
    @prop()
    IsActive: boolean = false;

    @prop()
    Cordination?: Cordination;

    Place: PlaceGetDto = new PlaceGetDto;
}
export class StationChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    UseTypeId: number = 0;
    @prop()
    PlaceId: number = 0;
    @prop()
    ConnectiveUserId?: number = null;
    @prop()
    Tag: string = '';
    @prop()
    Space: number = null;
    @prop()
    IsActive: boolean = false;

    @prop()
    Cordination?: Cordination;

    Place: PlaceGetDto = new PlaceGetDto;
}