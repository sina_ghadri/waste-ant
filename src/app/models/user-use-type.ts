import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { UserGetDto } from '../libs/identity/models/user';
import { UseTypeGetDto } from './use-type';

export class UserUseTypeGetDto extends BaseGetDto {
    UserId: number = 0;
    UseTypeId: number = 0;

    User: UserGetDto;
    UseType: UseTypeGetDto;
}
export class UserUseTypeAddDto extends BaseAddDto {
    UserId: number = 0;
    UseTypeId: number = 0;
}
export class UserUseTypeChangeDto extends BaseChangeDto {
    UserId: number = 0;
    UseTypeId: number = 0;
}