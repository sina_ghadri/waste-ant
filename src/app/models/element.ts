import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';

export class ElementGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    Image: string = '';
    @prop()
    IsActive: boolean = false;
}
export class ElementAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    Image: string = '';
    @prop()
    IsActive: boolean = false;
}
export class ElementChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    Image: string = '';
    @prop()
    IsActive: boolean = false;
}