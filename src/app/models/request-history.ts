import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { RequestHistoryType } from './enums';
import { RequestGetDto } from './request';
import { RoleGetDto } from '../libs/identity/models/role';

export class RequestHistoryGetDto extends BaseGetDto {
    RequestId: number = 0;
    RoleId: number = 0;
    Order: number = 0;
    Type: RequestHistoryType;
    Description: string = '';

    Request: RequestGetDto;
    Role: RoleGetDto;
}
export class RequestHistoryAddDto extends BaseAddDto {
    RequestId: number = 0;
    RoleId: number = 0;
    Order: number = 0;
    Type: RequestHistoryType;
    Description: string = '';
}
export class RequestHistoryChangeDto extends BaseChangeDto {
    Order: number = 0;
    Type: RequestHistoryType;
    Description: string = '';
}