import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';

export class DraftExitTypeGetDto extends BaseGetDto {
    Name: string = '';
    IsNeedSalePrice: boolean = false;
}
export class DraftExitTypeAddDto extends BaseAddDto {
    Name: string = '';
    IsNeedSalePrice: boolean = false;
}
export class DraftExitTypeChangeDto extends BaseChangeDto {
    Name: string = '';
    IsNeedSalePrice: boolean = false;
}