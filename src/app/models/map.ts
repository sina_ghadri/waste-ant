import { PlaceGetDto } from './place';
import { ElevatorGetDto } from './elevator';
import { StationGetDto } from './station';
import { PlaceElementGetDto } from './place-element';
import { RouteGetDto } from './route';
import { CleaningAreaGetDto } from './cleaning-area';
import { EventEmitter } from '@angular/core';

export const MapColors = ['#61575c', '#ac1f2e', '#d02a4c', '#cf462b', '#cf642b', '#b7ae13', '#9ecf2b', '#6bc930', '#22a88c', '#18b26e', '#139041'];

export class Point {
    private _x: number = 0;
    private _y: number = 0;

    get x(): number { return this._x; }
    set x(value: number) { this._x = (!value || value === Infinity ? 0 : value); }

    get y(): number { return this._y; }
    set y(value: number) { this._y = (!value || value === Infinity ? 0 : value); }

    constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }

    add(x: number, y: number): Point {
        this.x += x;
        this.y += y;
        return this;
    }

    toCordination(): Cordination {
        const cord = new Cordination();
        cord.X = this.x;
        cord.Y = this.y;
        return cord;
    }

    toPercentage(width: number, height: number) {
        return new Point(this.x * 100 / width, this.y * 100 / height);
    }
}
export class Rectangle {
    private _x: number = 0;
    private _y: number = 0;
    private _width: number = 0;
    private _height: number = 0;


    get x(): number { return this._x; }
    set x(value: number) { this._x = (!value || value === Infinity ? 0 : value); }

    get y(): number { return this._y; }
    set y(value: number) { this._y = (!value || value === Infinity ? 0 : value); }

    get width(): number { return this._width; }
    set width(value: number) { this._width = (!value || value === Infinity ? 0 : value); }

    get height(): number { return this._height; }
    set height(value: number) { this._height = (!value || value === Infinity ? 0 : value); }

    constructor(x: number = 0, y: number = 0, width: number = 0, height: number = 0) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    toCordination(): Cordination {
        const cord = new Cordination();
        cord.X = this.x;
        cord.Y = this.y;
        cord.Width = this.width;
        cord.Height = this.height;
        return cord;
    }

    getCenter(): Point {
        return new Point(this.x + this.width / 2, this.y + this.height / 2);
    }

    static create(p1: Point, p2: Point): Rectangle {
        if (!p1 || !p2) return new Rectangle;
        const rect = new Rectangle;
        rect.x = (p1.x < p2.x ? p1.x : p2.x);
        rect.y = (p1.y < p2.y ? p1.y : p2.y);
        rect.width = Math.abs(p1.x - p2.x);
        rect.height = Math.abs(p1.y - p2.y);
        return rect;
    }
}
export class Cordination {
    X: number = 0;
    Y: number = 0;
    Width: number = 0;
    Height: number = 0;

    constructor() {

    }


    static toRectangle(cord: Cordination, width: number, height: number): Rectangle {
        if (!cord) return new Rectangle;
        if (cord.X + cord.Width > 100 || cord.Y + cord.Height > 100) {
            return Rectangle.create(new Point(cord.X, cord.Y).toPercentage(width, height), new Point(cord.X + cord.Width, cord.Y + cord.Height).toPercentage(width, height));
        }
        return Rectangle.create(new Point(cord.X, cord.Y), new Point(cord.X + cord.Width, cord.Y + cord.Height));
    }
}
export class Map {
    width: number;
    height: number;
}
export class MapItemEvent<T> {
    event: any;
    item: T;
}
export interface IMapItem {
    map: Map;
    scale: number;
    isSelected: boolean;

    onclick: EventEmitter<any>;
    ondblclick: EventEmitter<any>;

    getKey(): any;
    getName(): string;
    getClass(): any;
    render(): string;

    click(e);
    dblclick(e);
}
export class MapItem {
    map: Map;
    scale: number = 1;
    isSelected: boolean;

    onclick: EventEmitter<any> = new EventEmitter;
    ondblclick: EventEmitter<any> = new EventEmitter;

    getClass(): any {
        return {
            'selected': this.isSelected

        };
    }
    click(e) {
        this.onclick.emit({ event: e, item: this });
    }
    dblclick(e) {
        this.onclick.emit({ event: e, item: this });
    }
}
export class Place extends MapItem implements IMapItem {
    obj: PlaceGetDto;
    rect: Rectangle;

    constructor(place: PlaceGetDto, rect: Rectangle) {
        super();
        this.obj = place;
        this.rect = rect;
    }
    getKey(): any { return `place-${this.obj.Id}`; }
    getName(): string { return this.obj.Name; }
    getClass(): any { let classes = super.getClass(); classes['place'] = true; return classes; }

    render(): string {
        const center = this.rect.getCenter();
        if (center.x > 100) center.x = 50;
        if (center.y > 100) center.y = 50;
        const font = 25 * this.scale;
        return `
        <rect 
        fill="#044B94" fill-opacity="0.4" 
        stroke="#224c71" stroke-width="3" stroke-opacity="1"
        x="${this.rect.x}%" y="${this.rect.y}%" 
        width="${this.rect.width}%" height="${this.rect.height}%"
        ></rect>
        <text x="${center.x}%" y="${center.y}%" dominant-baseline="middle" text-anchor="middle" font-size="${font}">${this.obj.Name}</text>
        `;
    }
}
export class Elevator extends MapItem implements IMapItem {
    obj: ElevatorGetDto;
    location: Point;

    constructor(obj: ElevatorGetDto, location: Point) {
        super();
        this.obj = obj;
        this.location = location;
    }

    getKey(): any { return `elevator-${this.obj.Id}`; }
    getName(): string { return this.obj.Name; }
    getClass(): any { let classes = super.getClass(); classes['elevator'] = true; return classes; }
    render(): string {
        return '';
    }
}
export class Station extends MapItem implements IMapItem {
    obj: StationGetDto;
    location: Point;
    scale: number = 1;

    constructor(obj: StationGetDto, location: Point) {
        super();
        this.obj = obj;
        this.location = location;
    }

    getKey(): any { return `station-${this.obj.Id}`; }
    getName(): string { return this.obj.Name; }
    getClass(): any { let classes = super.getClass(); classes['station'] = true; return classes; }
    render(): string {
        const grade = Math.round(this.obj.Grade * 5);
        if (this.location.x > 100) this.location.x = 50;
        if (this.location.y > 100) this.location.y = 50;
        const radius = 20 * this.scale;
        const image = 25 * this.scale;
        return `
        <ellipse 
        class="stroke-animation" 
        cx="${this.location.x}%" cy="${this.location.y}%" 
        rx="${radius}" ry="${radius}"
        fill-opacity="1" fill="white"
        stroke="#61575c" stroke-width="2" stroke-opacity="1"/>
        <image xlink:href="assets/images/stations/${grade}.png" x="${this.location.x}%" y="${this.location.y}%" 
        width="${image}" height="${image}" transform="translate(-${image / 2},-${image / 2})"/>
        `;
    }
}
export class Element extends MapItem implements IMapItem {
    obj: PlaceElementGetDto;
    location: Point;

    constructor(obj: PlaceElementGetDto, location: Point) {
        super();
        this.obj = obj;
        this.location = location;
    }

    getKey(): any { return `element-${this.obj.Id}`; }
    getName(): string { return this.obj.Element.Name; }
    getClass(): any { let classes = super.getClass(); classes['element'] = true; return classes; }
    render(): string {
        if (this.location.x > 100) this.location.x = 50;
        if (this.location.y > 100) this.location.y = 50;
        if (!this.obj.Element) return;
        const size = 30 * this.scale;
        return `
        <image xlink:href="${this.obj.Element.Image}" x="${this.location.x}%" y="${this.location.y}%" 
        height="${size}" width="${size}" transform="translate(-${size / 2},-${size / 2})" />
        `;
    }
}
export class RouteItem extends MapItem {
    route: Route;
    station: Station;
    order: number;

    constructor(route: Route, station: Station, order: number) {
        super();
        this.route = route;
        this.station = station;
        this.order = order;
    }

    render(): string {
        const box = 45 * this.route.scale;
        const text_width = 80 * this.route.scale;
        const text_height = 20 * this.route.scale;
        const font = 15 * this.route.scale;
        return `
        <rect class="stroke-animation" 
        x="${this.station.location.x}%" y="${this.station.location.y}%" 
        width="${box}" height="${box}" 
        transform="translate(-${box / 2},-${box / 2})"
        fill-opacity=".5" fill="${this.route.color}"
        stroke="#61575c" stroke-width="2" stroke-opacity="1"/>
        <rect class="stroke-animation" x="${this.station.location.x}%" y="${this.station.location.y}%" width="${text_width}" height="${text_height}" transform="translate(-${text_width / 2},-${box - text_height / 4})"
        fill-opacity="1" fill="white"
        stroke="#61575c" stroke-width="2" stroke-opacity="1"/>
        <text x="${this.station.location.x}%" y="${this.station.location.y}%" transform="translate(0,-${(text_height + (text_height * 2)) / 2})" dominant-baseline="middle" text-anchor="middle" font-size="${font}">${this.route.obj.Code}-${this.order}</text>
        `;
    }
}
export class Route extends MapItem implements IMapItem {
    color: string;
    obj: RouteGetDto;
    items: RouteItem[] = [];

    constructor(obj: RouteGetDto) {
        super();
        this.obj = obj;
    }

    getKey(): any { return `route-${this.obj.Id}`; }
    getName(): string { return 'مسیر شماره ی' + this.obj.Code; }
    getClass(): any { let classes = super.getClass(); classes['route'] = true; return classes; }
    render(): string {
        let path = '';
        let str = '';
        if (this.items && this.items.length > 0) {
            const first = this.items[0];
            let x = (first.station.location.x / 100 * this.map.width);
            let y = (first.station.location.y / 100 * this.map.height);
            path = `<path d="M${x},${y} `;
            for (let i = 1; i < this.items.length; i++) {
                const item = this.items[i];
                x = (item.station.location.x / 100 * this.map.width);
                y = (item.station.location.y / 100 * this.map.height);
                path += `L${x},${y}`;
            }
            path += '" stroke="#660000" stroke-width="2" fill="none"/>';
        }
        for (let i = 0; i < this.items.length; i++) {
            const item = this.items[i];
            str += item.render();
        }
        return path + str;
    }
}
export class CleaningArea extends MapItem implements IMapItem {
    obj: CleaningAreaGetDto;
    rect: Rectangle;

    constructor(obj: CleaningAreaGetDto, rect: Rectangle) {
        super();
        this.obj = obj;
        this.rect = rect;
    }

    getKey(): any { return `cleaningarea-${this.obj.Id}`; }
    getName(): string { return 'محوطه شماره ی' + this.obj.Code; }
    getClass(): any { let classes = super.getClass(); classes['cleaning-area'] = true; return classes; }
    render(): string {
        const center = this.rect.getCenter();
        if (center.x > 100) center.x = 50;
        if (center.y > 100) center.y = 50;
        return `
        <rect 
        fill="#89c341" fill-opacity="0.4" 
        stroke="#407b41" stroke-width="3" stroke-opacity="1"
        x="${this.rect.x}%" y="${this.rect.y}%" 
        width="${this.rect.width}%" height="${this.rect.height}%"
        />
        <text x="${center.x}%" y="${center.y}%" dominant-baseline="middle" text-anchor="middle" font-size="20">${this.obj.Code}</text>
        `;
    }
}