import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';

export class ReceiptSwapGetDto extends BaseGetDto {
    UserId: number = 0;
    RouteId: number = 0;
    RequestSwapId: number = 0;
    DestinationPlaceId: number = 0;
    Time: string = '';
}
export class ReceiptSwapAddDto extends BaseAddDto {
    RouteId: number = 0;
    RequestSwapId: number = 0;
    DestinationPlaceId: number = 0;
    Time: string = '';
}
export class ReceiptSwapChangeDto extends BaseChangeDto {
    DestinationPlaceId: number = 0;
    Time: string = '';
}