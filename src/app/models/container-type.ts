import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { WasteTypeGetDto } from './waste-type';
import { prop, required } from '@rxweb/reactive-form-validators';

export class ContainerTypeGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    Color: string = '';
    @prop()
    Capacity: number = null;
    @prop()
    WasteTypeId: number = null;
    @prop()
    LongLife?: number = null;

    @prop()
    WasteType: WasteTypeGetDto = new WasteTypeGetDto;
}
export class ContainerTypeAddDto extends BaseAddDto {
    @prop()
    @required()
    Name: string = '';
    @prop()
    @required()
    Color: string = '';
    @prop()
    @required()
    Capacity: number = null;
    @prop()
    @required()
    WasteTypeId: number = null;
    @prop()
    @required()
    LongLife?: number = null;
}
export class ContainerTypeChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    Color: string = '';
    @prop()
    Capacity: number = null;
    @prop()
    WasteTypeId: number = null;
    @prop()
    LongLife?: number = null;
}