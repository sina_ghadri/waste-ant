import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { RequestGetDto } from './request';

export class RequestElevatorGetDto extends BaseGetDto {
    RequestId: number = 0;
    ElevatorId: number = 0;

    Request: RequestGetDto;
}
export class RequestElevatorAddDto extends BaseAddDto {
    RequestId: number = 0;
    ElevatorId: number = 0;
}
export class RequestElevatorChangeDto extends BaseChangeDto {
    
}