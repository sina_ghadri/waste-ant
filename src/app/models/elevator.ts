import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';
import { PlaceGetDto } from './place';

export class ElevatorGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    PlaceId: number = 0;
    @prop()
    IsActive: boolean = false;

    @prop()
    Place: PlaceGetDto = new PlaceGetDto;
}
export class ElevatorAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    PlaceId: number = 0;
    @prop()
    IsActive: boolean = false;

    @prop()
    Place: PlaceGetDto = new PlaceGetDto;
}
export class ElevatorChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    PlaceId: number = 0;
    @prop()
    IsActive: boolean = false;
    
    @prop()
    Place: PlaceGetDto = new PlaceGetDto;
}