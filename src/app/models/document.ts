import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { DocumentType } from './enums';
import { UserGetDto } from '../libs/identity/models/user';

export class DocumentGetDto extends BaseGetDto {
    Num: number = 0;
    Time: string = '';
    Type: DocumentType;
    UserId: number = 0;
    Volume?: number = 0;
    Weight?: number = 0;

    User: UserGetDto;
}
export class DocumentAddDto extends BaseAddDto {
    Num: number = 0;
    Time: string = '';
    Type: DocumentType;
    UserId: number = 0;
    Volume?: number = 0;
    Weight?: number = 0;
}
export class DocumentChangeDto extends BaseChangeDto {
    Num: number = 0;
    Time: string = '';
    Type: DocumentType;
    UserId: number = 0;
    Volume?: number = 0;
    Weight?: number = 0;
}