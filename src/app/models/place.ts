import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { PlaceType } from './enums';
import { prop } from '@rxweb/reactive-form-validators';
import { Cordination } from './map';

export class PlaceGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    Cordination: Cordination = new Cordination;
    @prop()
    Image: string = '';
    @prop()
    Type: PlaceType = PlaceType.None;
    @prop()
    IsActive: boolean = false;
    @prop()
    ParentId?: number = 0;

    Parent: PlaceGetDto;
}
export class PlaceAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    Cordination: Cordination;
    @prop()
    Image: string = '';
    @prop()
    Type: PlaceType = PlaceType.None;
    @prop()
    IsActive: boolean = false;
    @prop()
    ParentId?: number = 0;
}
export class PlaceChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    Cordination: Cordination;
    @prop()
    Image: string = '';
    @prop()
    Type: PlaceType = PlaceType.None;
    @prop()
    IsActive: boolean = false;
    @prop()
    ParentId?: number = 0;
}