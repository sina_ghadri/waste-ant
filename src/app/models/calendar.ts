import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { DayTypeGetDto } from './day-type';

export class CalendarGetDto extends BaseGetDto {
    Date: string = '';
    DayTypeId?: number = 0;

    DayType: DayTypeGetDto;
}
export class CalendarAddDto extends BaseAddDto {
    Date: string = '';
    DayPartId: number = 0;
}
export class CalendarChangeDto extends BaseChangeDto {
    DayPartId: number = 0;    
}

export class Day {
    text: string;
    value: string;
    color: string;
    tooltip: string;
    monthDistance: number = 0;
  }