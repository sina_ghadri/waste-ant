import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { ShiftGetDto } from './shift';
import { ShiftPartNameGetDto } from './shift-part-name';

export class ShiftPartGetDto extends BaseGetDto {
    ShiftId: number = 0;
    ShiftPartNameId: number = 0;
    StartTime: string = '';
    EndTime: string = '';

    Shift: ShiftGetDto;
    ShiftPartName: ShiftPartNameGetDto;
}
export class ShiftPartAddDto extends BaseAddDto {
    ShiftId: number = 0;
    ShiftPartNameId: number = 0;
    StartTime: string = '';
    EndTime: string = '';
}
export class ShiftPartChangeDto extends BaseChangeDto {
    ShiftId: number = 0;
    ShiftPartNameId: number = 0;
    StartTime: string = '';
    EndTime: string = '';
}