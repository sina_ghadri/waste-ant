import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { PlaceGetDto } from './place';
import { prop } from '@rxweb/reactive-form-validators';
import { Cordination } from './map';

export class CleaningAreaGetDto extends BaseGetDto {
    @prop()
    PlaceId: number = 0;
    @prop()
    Code: string = '';
    Cordination: Cordination = new Cordination;
    @prop()
    IsActive: boolean = false;

    Place: PlaceGetDto;
}
export class CleaningAreaAddDto extends BaseAddDto {
    @prop()
    PlaceId: number = 0;
    @prop()
    Code: string = '';
    Cordination: Cordination;
    @prop()
    IsActive: boolean = false;
}
export class CleaningAreaChangeDto extends BaseChangeDto {
    @prop()
    Code: string = '';
    Cordination: Cordination;
    @prop()
    IsActive: boolean = false;
}