import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { RequestGetDto } from './request';

export class RequestEquipmentGetDto extends BaseGetDto {
    RequestId: number = 0;
    EquipmentId: number = 0;
    Count: number = 0;

    Request: RequestGetDto;
}
export class RequestEquipmentAddDto extends BaseAddDto {
    RequestId: number = 0;
    EquipmentId: number = 0;
    Count: number = 0;
}
export class RequestEquipmentChangeDto extends BaseChangeDto {
    
}