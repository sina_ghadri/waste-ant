import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { RouteGetDto } from './route';
import { StationGetDto } from './station';

export class RouteItemGetDto extends BaseGetDto {
    RouteId: number;
    StationId: number;
    Order: number;

    Route: RouteGetDto;
    Station: StationGetDto;
}
export class RouteItemAddDto extends BaseAddDto {
    RouteId: number;
    StationId: number;
    Order: number;
}
export class RouteItemChangeDto extends BaseChangeDto {
    RouteId: number;
    StationId: number;
    Order: number;
}