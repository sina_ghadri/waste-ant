import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { ShiftGetDto } from './shift';
import { RoleGetDto } from '../libs/identity/models/role';

export class ShiftRoleGetDto extends BaseGetDto {
    ShiftId: number = 0;
    RoleId: number = 0;

    Shift: ShiftGetDto;
    Role: RoleGetDto;
}
export class ShiftRoleAddDto extends BaseAddDto {
    ShiftId: number = 0;
    RoleId: number = 0;
}
export class ShiftRoleChangeDto extends BaseChangeDto {
    ShiftId: number = 0;
    RoleId: number = 0;
}