import { required, prop } from "@rxweb/reactive-form-validators";
import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { DateTime } from '../libs/bitspco/models/datetime';
import { DayPartAddDto, DayPartGetDto } from './day-part';

export class DayTypeGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    Color: string = '';
    @prop()
    StartTime: string = '';
    @prop()
    EndTime: string = '';
    @prop()
    IsActive: boolean = false;
    
    @prop()
    Parts: DayPartGetDto[] = [];

    @prop()
    get StartDate(): Date { return DateTime.parse(this.StartTime, 'hh:mm:ss').toDate(); }
    set StartDate(value: Date) { this.StartTime = DateTime.toDateTime(value).format('hh:mm'); }

    @prop()
    get EndDate(): Date { return DateTime.parse(this.EndTime, 'hh:mm:ss').toDate(); }
    set EndDate(value: Date) { this.EndTime = DateTime.toDateTime(value).format('hh:mm'); }
}
export class DayTypeAddDto extends BaseAddDto {
    @required()
    Name: string = '';
    @prop()
    Color: string = '';
    @prop()
    StartTime: string = '';
    @prop()
    EndTime: string = '';
    @prop()
    IsActive: boolean = false;

    @prop()
    Parts: DayPartAddDto[] = [];

    @prop()
    get StartDate(): Date { return DateTime.parse(this.StartTime, 'hh:mm:ss').toDate(); }
    set StartDate(value: Date) { this.StartTime = DateTime.toDateTime(value).format('hh:mm'); }

    @prop()
    get EndDate(): Date { return DateTime.parse(this.EndTime, 'hh:mm:ss').toDate(); }
    set EndDate(value: Date) { this.EndTime = DateTime.toDateTime(value).format('hh:mm'); }

    constructor()
    {
        super();
        this.StartDate = new Date(0, 0, 0, 7, 0, 0);
        this.EndDate = new Date(0, 0, 0, 16, 0, 0);
    }
}
export class DayTypeChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    Color: string = '';
    @prop()
    StartTime: string = '';
    @prop()
    EndTime: string = '';
    @prop()
    IsActive: boolean = false;

    @prop()
    get StartDate(): Date { return DateTime.parse(this.StartTime, 'hh:mm:ss').toDate(); }
    set StartDate(value: Date) { this.StartTime = DateTime.toDateTime(value).format('hh:mm'); }

    @prop()
    get EndDate(): Date { return DateTime.parse(this.EndTime, 'hh:mm:ss').toDate(); }
    set EndDate(value: Date) { this.EndTime = DateTime.toDateTime(value).format('hh:mm'); }
}