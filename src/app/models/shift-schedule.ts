import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { RoleGetDto } from '../libs/identity/models/role';
import { PlaceGetDto } from './place';
import { CalendarGetDto } from './calendar';
import { ShiftGetDto } from './shift';

export class ShiftScheduleGetDto extends BaseGetDto {
    RoleId: number = 0;
    PlaceId: number = 0;
    CalendarId: number = 0;
    ShiftId: number = 0;
    StartTime: string = '';
    EndTime: string = '';

    Role: RoleGetDto;
    Place: PlaceGetDto;
    Calendar: CalendarGetDto;
    Shift: ShiftGetDto;
}
export class ShiftScheduleAddDto extends BaseAddDto {
    RoleId: number = 0;
    PlaceId: number = 0;
    CalendarId: number = 0;
    ShiftId: number = 0;
    StartTime: string = '';
    EndTime: string = '';
}
export class ShiftScheduleChangeDto extends BaseChangeDto {
    RoleId: number = 0;
    PlaceId: number = 0;
    CalendarId: number = 0;
    ShiftId: number = 0;
    StartTime: string = '';
    EndTime: string = '';
}