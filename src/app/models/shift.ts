import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { DayTypeGetDto } from './day-type';
import { ShiftRoleAddDto } from './shift-role';
import { prop } from '@rxweb/reactive-form-validators';
import { ShiftPartAddDto } from './shift-part';

export class ShiftGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    DayTypeId: number = 0;
    @prop()
    IsActive: boolean = false;
    
    @prop()
    DayType: DayTypeGetDto = new DayTypeGetDto;
}
export class ShiftAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    DayTypeId: number = 0;
    @prop()
    IsActive: boolean = false;

    @prop()
    RoleIds: number[];

    @prop()
    Roles: ShiftRoleAddDto[] = [];
    @prop()
    Parts: ShiftPartAddDto[] = [];

    private _DayType: DayTypeGetDto;
    @prop()
    get DayType(): DayTypeGetDto { return this._DayType; }
    set DayType(value: DayTypeGetDto) { this._DayType = value; this.DayTypeId = value.Id; }
}
export class ShiftChangeDto extends BaseChangeDto {

    @prop()
    Name: string = '';
    @prop()
    DayTypeId: number = 0;
    @prop()
    IsActive: boolean = false;

    @prop()
    RoleIds: number[];

    @prop()
    Roles: ShiftRoleAddDto[] = [];
    @prop()
    Parts: ShiftPartAddDto[] = [];

    private _DayType: DayTypeGetDto;
    @prop()
    get DayType(): DayTypeGetDto { return this._DayType; }
    set DayType(value: DayTypeGetDto) { this._DayType = value; this.DayTypeId = value.Id; }
}