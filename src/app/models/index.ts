import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';

export * from '../libs/bitspco/models';
export * from '../libs/identity/models';

export * from './color';
export * from './calendar';
export * from './cleaning-area';
export * from './cleaning-area-equipment';
export * from './container';
export * from './container-type';
export * from './day-part';
export * from './day-part-name';
export * from './day-part-schedule';
export * from './day-part-schedule-item';
export * from './day-type';
export * from './document';
export * from './document-item';
export * from './draft-exit';
export * from './draft-exit-type';
export * from './element';
export * from './elevator';
export * from './equipment';
export * from './enums';
export * from './inspection';
export * from './inspection-index';
export * from './place';
export * from './place-element';
export * from './map';
export * from './receipt-discharge';
export * from './receipt-discharge-item';
export * from './receipt-swap';
export * from './receipt-swap-item';
export * from './request';
export * from './request-cleaning';
export * from './request-discharge';
export * from './request-elevator';
export * from './request-equipment';
export * from './request-history';
export * from './request-leave';
export * from './request-swap';
export * from './route';
export * from './route-item';
export * from './setting';
export * from './shift';
export * from './shift-part';
export * from './shift-part-name';
export * from './shift-role';
export * from './shift-schedule';
export * from './shift-schedule-item';
export * from './station';
export * from './user';
export * from './use-type';
export * from './use-type-index';
export * from './user-place';
export * from './user-use-type';
export * from './waste-type';


export class IndexGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    SignificanceFactor: number = 0;
    @prop()
    ParentId?: number = 0;
    @prop()
    IsActive: boolean = false;

    Parent: IndexGetDto;
    Children: IndexGetDto[];
}
export class IndexAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    SignificanceFactor: number = 0;
    @prop()
    ParentId?: number = 0;
    @prop()
    IsActive: boolean = false;
}
export class IndexChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    SignificanceFactor: number = 0;
    @prop()
    ParentId?: number = 0;
    @prop()
    IsActive: boolean = false;
}