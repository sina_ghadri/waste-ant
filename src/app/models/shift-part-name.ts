import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';

export class ShiftPartNameGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    Color: string = '';
    @prop()
    IsWorkTime: boolean = false;
    @prop()
    IsActive: boolean = false;
}
export class ShiftPartNameAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    Color: string = '';
    @prop()
    IsWorkTime: boolean = false;
    @prop()
    IsActive: boolean = false;
}
export class ShiftPartNameChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    Color: string = '';
    @prop()
    IsWorkTime: boolean = false;
    @prop()
    IsActive: boolean = false;
}