import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { PlaceGetDto } from './place';
import { ElementGetDto } from './element';
import { Cordination } from './map';

export class PlaceElementGetDto extends BaseGetDto {
    PlaceId: number = 0;
    ElementId: number = 0;
    Cordination: Cordination;

    Place: PlaceGetDto;
    Element: ElementGetDto;
}
export class PlaceElementAddDto extends BaseAddDto {
    PlaceId: number = 0;
    ElementId: number = 0;
    Cordination: Cordination;
}
export class PlaceElementChangeDto extends BaseChangeDto {
    Cordination: Cordination;
}