import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';

export class CleaningAreaEquipmentGetDto extends BaseGetDto {

}
export class CleaningAreaEquipmentAddDto extends BaseAddDto {
    
}
export class CleaningAreaEquipmentChangeDto extends BaseChangeDto {
    
}