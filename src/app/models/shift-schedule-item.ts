import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { ShiftScheduleGetDto } from './shift-schedule';
import { UserGetDto } from '../libs/identity/models/user';

export class ShiftScheduleItemGetDto extends BaseGetDto {
    ShiftScheduleId: number = 0;
    UserId: number = 0;

    ShiftSchedule: ShiftScheduleGetDto;
    User: UserGetDto;
}
export class ShiftScheduleItemAddDto extends BaseAddDto {
    ShiftScheduleId: number = 0;
    UserId: number = 0;
}
export class ShiftScheduleItemChangeDto extends BaseChangeDto {
    ShiftScheduleId: number = 0;
    UserId: number = 0;
}