import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';

export class WasteTypeGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    @prop()
    ParentId?: number = 0;

    Parent: WasteTypeGetDto;
}
export class WasteTypeAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    @prop()
    ParentId?: number = 0;
}
export class WasteTypeChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    @prop()
    ParentId?: number = 0;
}