import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { RequestType, RequestGenerationType, RequestStatus } from './enums';
import { UserGetDto } from '../libs/identity/models/user';

export class RequestGetDto extends BaseGetDto {
    Code: string = '';
    UserId?: number = 0;
    Date: string = '';
    Type: RequestType;
    GenerationType: RequestGenerationType;
    Title: string = '';
    Description: string = '';
    Status: RequestStatus;

    User: UserGetDto;
}
export class RequestAddDto extends BaseAddDto {
    Code: string = '';
    Date: string = '';
    Type: RequestType;
    Title: string = '';
    Description: string = '';
}
export class RequestChangeDto extends BaseChangeDto {
    Code: string = '';
    Date: string = '';
    Type: RequestType;
    Title: string = '';
    Description: string = '';
}