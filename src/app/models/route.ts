import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { PlaceGetDto } from './place';
import { ElevatorGetDto } from './elevator';
import { prop } from '@rxweb/reactive-form-validators';

export class RouteGetDto extends BaseGetDto {
    @prop()
    Code: string = '';
    PlaceId: number = 0;
    @prop()
    ElevatorId: number = 0;
    @prop()
    IsActive: boolean = false;

    Place: PlaceGetDto;
    Elevator: ElevatorGetDto;
}
export class RouteAddDto extends BaseAddDto {
    @prop()
    Code: string = '';
    @prop()
    PlaceId: number = 0;
    @prop()
    ElevatorId: number = 0;
    @prop()
    IsActive: boolean = false;
}
export class RouteChangeDto extends BaseChangeDto {
    @prop()
    Code: string = '';
    @prop()
    ElevatorId: number = 0;
    @prop()
    IsActive: boolean = false;
}