import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';

export class DayPartNameGetDto extends BaseGetDto {
    @prop()
    Name: string = '';
    ResponseTime: string = '';
    @prop()
    Color: string = '';
}
export class DayPartNameAddDto extends BaseAddDto {
    @prop()
    Name: string = '';
    ResponseTime: string = '';
    @prop()
    Color: string = '';
}
export class DayPartNameChangeDto extends BaseChangeDto {
    @prop()
    Name: string = '';
    ResponseTime: string = '';
    @prop()
    Color: string = '';
}