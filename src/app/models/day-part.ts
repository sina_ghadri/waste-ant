import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { DayPartNameGetDto } from './day-part-name';
import { DayTypeGetDto } from './day-type';

export class DayPartGetDto extends BaseGetDto {
    DayTypeId: number = 0;
    DayPartNameId?: number = 0;
    StartTime: string = '';
    EndTime: string = '';

    DayPartName: DayPartNameGetDto;
    DayType: DayTypeGetDto;
}
export class DayPartAddDto extends BaseAddDto {
    DayTypeId: number = 0;
    DayPartNameId?: number = 0;
    StartTime: string = '';
    EndTime: string = '';
}
export class DayPartChangeDto extends BaseChangeDto {
    DayTypeId: number = 0;
    DayPartNameId?: number = 0;
    StartTime: string = '';
    EndTime: string = '';
}