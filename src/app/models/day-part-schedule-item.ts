import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { DayPartScheduleType } from './enums';
import { DayPartScheduleGetDto } from './day-part-schedule';
import { prop } from '@rxweb/reactive-form-validators';
import { DateTime } from '../libs/bitspco/models/datetime';

export class DayPartScheduleItemGetDto extends BaseGetDto {
    @prop()
    DayPartScheduleId: number = 0;
    @prop()
    Type: DayPartScheduleType = DayPartScheduleType.None;
    @prop()
    Time: string = '';

    DayPartSchedule: DayPartScheduleGetDto;
}
export class DayPartScheduleItemAddDto extends BaseAddDto {
    @prop()
    DayPartScheduleId: number = 0;
    @prop()
    Type: DayPartScheduleType = DayPartScheduleType.None;
    @prop()
    Time: string = '';

    @prop()
    get Date(): Date { return DateTime.parse(this.Time, 'hh:mm:ss').toDate(); }
    set Date(value: Date) { this.Time = DateTime.toDateTime(value).format('hh:mm'); }

    constructor() {
        super();
        this.Date = new Date(0, 0, 0, 0, 0, 0);
    }
}
export class DayPartScheduleItemChangeDto extends BaseChangeDto {
    @prop()
    DayPartScheduleId: number = 0;
    @prop()
    Type: DayPartScheduleType = DayPartScheduleType.None;
    @prop()
    Time: string = '';
}