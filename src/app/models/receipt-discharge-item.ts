import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { ReceiptDischargeGetDto } from './receipt-discharge';
import { StationGetDto } from './station';
import { ContainerGetDto } from './container';
import { WasteTypeGetDto } from './waste-type';

export class ReceiptDischargeItemGetDto extends BaseGetDto {
    ReceiptDischargeId: number = 0;
    StationId: number = 0;
    ContainerId: number = 0;
    WasteTypeId: number = 0;
    Time: string = '';
    Weight: number = 0;
    Volume: number = 0;

    ReceiptDischarge: ReceiptDischargeGetDto;
    Station: StationGetDto;
    Container: ContainerGetDto;
    WasteType: WasteTypeGetDto;
}
export class ReceiptDischargeItemAddDto extends BaseAddDto {
    ReceiptDischargeId: number = 0;
    StationId: number = 0;
    ContainerId: number = 0;
    WasteTypeId: number = 0;
    Time: string = '';
    Weight: number = 0;
    Volume: number = 0;
}
export class ReceiptDischargeItemChangeDto extends BaseChangeDto {
    ReceiptDischargeId: number = 0;
    StationId: number = 0;
    ContainerId: number = 0;
    WasteTypeId: number = 0;
    Time: string = '';
    Weight: number = 0;
    Volume: number = 0;
}