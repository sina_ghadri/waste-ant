import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { DocumentGetDto } from './document';
import { WasteTypeGetDto } from './waste-type';
import { StationGetDto } from './station';
import { RequestGetDto } from './request';

export class DocumentItemGetDto extends BaseGetDto {
    DocumentItemId: number = 0;
    Volume: number = 0;
    Weight?: number = 0;
    WasteTypeId: number = 0;
    StationId?: number = 0;
    RequestId?: number = 0;

    Document: DocumentGetDto;
    WasteType: WasteTypeGetDto;
    Station: StationGetDto;
    Request: RequestGetDto;
}
export class DocumentItemAddDto extends BaseAddDto {
    DocumentItemId: number = 0;
    Volume: number = 0;
    Weight?: number = 0;
    WasteTypeId: number = 0;
    StationId?: number = 0;
    RequestId?: number = 0;
}
export class DocumentItemChangeDto extends BaseChangeDto {
    DocumentItemId: number = 0;
    Volume: number = 0;
    Weight?: number = 0;
    WasteTypeId: number = 0;
    StationId?: number = 0;
    RequestId?: number = 0;
}