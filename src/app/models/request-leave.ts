import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { RequestLeaveType } from './enums';
import { RequestGetDto } from './request';

export class RequestLeaveGetDto extends BaseGetDto {
    RequestId: number = 0;
    LeaveTime: string = '';
    Type: RequestLeaveType;
    StartTime: string = '';
    EndTime: string = '';

    Request: RequestGetDto;
}
export class RequestLeaveAddDto extends BaseAddDto {
    RequestId: number = 0;
    
}
export class RequestLeaveChangeDto extends BaseChangeDto {
    
}