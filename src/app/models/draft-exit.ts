import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { WasteTypeGetDto } from './waste-type';

export class DraftExitGetDto extends BaseGetDto {
    Code: string = '';
    Time: string = '';
    Weight: number = 0;
    Volume: number = 0;
    Description: string = '';
    SalePrice: number = 0;
    DraftExitTypeId: number = 0;
    WasteTypeId: number = 0;

    DraftExitType: DraftExitGetDto;
    WasteType: WasteTypeGetDto;
}
export class DraftExitAddDto extends BaseAddDto {
    Code: string = '';
    Time: string = '';
    Weight: number = 0;
    Volume: number = 0;
    Description: string = '';
    SalePrice: number = 0;
    DraftExitTypeId: number = 0;
    WasteTypeId: number = 0;
}
export class DraftExitChangeDto extends BaseChangeDto {
    Code: string = '';
    Time: string = '';
    Weight: number = 0;
    Volume: number = 0;
    Description: string = '';
    SalePrice: number = 0;
    DraftExitTypeId: number = 0;
    WasteTypeId: number = 0;
}