import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { RequestGetDto } from './request';
import { RouteGetDto } from './route';
import { DayPartScheduleGetDto } from './day-part-schedule';

export class RequestDischargeGetDto extends BaseGetDto {
    RequestId: number = 0;
    RouteId: number = 0;
    DayPartScheduleId?: number = 0;

    Request: RequestGetDto;
    Route: RouteGetDto;
    DayPartSchedule: DayPartScheduleGetDto;
}
export class RequestDischargeAddDto extends BaseAddDto {
    RequestId: number = 0;
    RouteId: number = 0;
    DayPartScheduleId?: number = 0;
}
export class RequestDischargeChangeDto extends BaseChangeDto {
    
}