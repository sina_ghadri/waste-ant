import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { UserGetDto } from '../libs/identity/models/user';
import { PlaceGetDto } from './place';

export class UserPlaceGetDto extends BaseGetDto {
    UserId: number = 0;
    PlaceId: number = 0;

    User: UserGetDto;
    Place: PlaceGetDto;
}
export class UserPlaceAddDto extends BaseAddDto {
    UserId: number = 0;
    PlaceId: number = 0;
}
export class UserPlaceChangeDto extends BaseChangeDto {
    UserId: number = 0;
    PlaceId: number = 0;
}