import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { prop } from '@rxweb/reactive-form-validators';

export class SettingGetDto extends BaseGetDto {
    @prop()
    CaptchaShowTryCount: number = 0;
    @prop()
    LockUserTryCount: number = 0;
    @prop()
    UseLowercaseCharactersInPassword: boolean = false;
    @prop()
    UseUppercaseCharactersInPassword: boolean = false;
    @prop()
    UseNumbersInPassword: boolean = false;
    @prop()
    UseSpecialCharactersInPassword: boolean = false;
    @prop()
    MinPasswordLength: number = 0;
    @prop()
    MaxStoreUserLogByYear: number = 0;
    @prop()
    IsActive: boolean = false;
}
export class SettingAddDto extends BaseAddDto {
    @prop()
    CaptchaShowTryCount: number = 0;
    @prop()
    LockUserTryCount: number = 0;
    @prop()
    UseLowercaseCharactersInPassword: boolean = false;
    @prop()
    UseUppercaseCharactersInPassword: boolean = false;
    @prop()
    UseNumbersInPassword: boolean = false;
    @prop()
    UseSpecialCharactersInPassword: boolean = false;
    @prop()
    MinPasswordLength: number = 0;
    @prop()
    MaxStoreUserLogByYear: number = 0;
    @prop()
    IsActive: boolean = false;
}
export class SettingChangeDto extends BaseChangeDto {
    @prop()
    CaptchaShowTryCount: number = 0;
    @prop()
    LockUserTryCount: number = 0;
    @prop()
    UseLowercaseCharactersInPassword: boolean = false;
    @prop()
    UseUppercaseCharactersInPassword: boolean = false;
    @prop()
    UseNumbersInPassword: boolean = false;
    @prop()
    UseSpecialCharactersInPassword: boolean = false;
    @prop()
    MinPasswordLength: number = 0;
    @prop()
    MaxStoreUserLogByYear: number = 0;
    @prop()
    IsActive: boolean = false;
}