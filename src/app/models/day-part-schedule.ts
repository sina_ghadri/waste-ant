import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { DayPartGetDto } from './day-part';
import { PlaceGetDto } from './place';
import { DayTypeGetDto } from './day-type';
import { prop, required } from '@rxweb/reactive-form-validators';
import { DayPartScheduleItemAddDto } from './day-part-schedule-item';
import { DayPartScheduleType } from './enums';

export class DayPartScheduleGetDto extends BaseGetDto {
    @prop()
    DayPartId: number = null;
    @prop()
    PlaceId: number = null;
    @prop()
    DischargePeriod: number = null;
    @prop()
    SwapPeriod: number = null;
    @prop()
    IsActive: boolean = false;
    @prop()
    Description: string = '';

    _dayType: DayTypeGetDto;
    _dayPart: DayPartGetDto;
    _place: PlaceGetDto;

    @prop()
    get DayType(): DayTypeGetDto { return this._dayType; }
    set DayType(value: DayTypeGetDto) { this._dayType = value; }

    @prop()
    get DayPart(): DayPartGetDto { return this._dayPart; }
    set DayPart(value: DayPartGetDto) { if(value) this.DayPartId = value.Id; this._dayPart = value; }
    
    @prop()
    get Place(): PlaceGetDto { return this._place; }
    set Place(value: PlaceGetDto) { if(value) this.PlaceId = value.Id; this._place = value; }
}
export class DayPartScheduleAddDto extends BaseAddDto {

    @prop()
    @required()
    DayTypeId: number;
    @prop()
    @required()
    DayPartId: number = null;
    @prop()
    @required()
    PlaceId: number = null;
    @prop()
    @required()
    DischargePeriod: number = null;
    @prop()
    @required()
    SwapPeriod: number = null;
    @prop()
    IsActive: boolean = false;
    @prop()
    Description: string = '';
    
    @prop()
    Items: DayPartScheduleItemAddDto[] = [];
    
    constructor() {
        super();
        const normal = new DayPartScheduleItemAddDto;
        normal.Type = DayPartScheduleType.NormalCleaning;
        this.Items.push(normal);
        const minor = new DayPartScheduleItemAddDto;
        minor.Type = DayPartScheduleType.MinorCleaning;
        this.Items.push(minor);
    }
  }
export class DayPartScheduleChangeDto extends BaseChangeDto {
    
    @prop()
    @required()
    DayTypeId: number;
    @prop()
    @required()
    DayPartId: number = null;
    @prop()
    @required()
    PlaceId: number = null;
    @prop()
    @required()
    DischargePeriod: number = null;
    @prop()
    @required()
    SwapPeriod: number = null;
    @prop()
    IsActive: boolean = false;
    @prop()
    Description: string = '';
    
    @prop()
    Items: DayPartScheduleItemAddDto[] = [];
    
    constructor() {
        super();
        const normal = new DayPartScheduleItemAddDto;
        normal.Type = DayPartScheduleType.NormalCleaning;
        this.Items.push(normal);
        const minor = new DayPartScheduleItemAddDto;
        minor.Type = DayPartScheduleType.MinorCleaning;
        this.Items.push(minor);
    }
}