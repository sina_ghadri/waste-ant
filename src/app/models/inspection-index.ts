import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { InspectionGetDto } from './inspection';
import { IndexGetDto } from '.';

export class InspectionIndexGetDto extends BaseGetDto {
    InspectionId: number = 0;
    IndexId: number = 0;
    Grade: number = 0;

    Inspection: InspectionGetDto;
    Index: IndexGetDto;
}
export class InspectionIndexAddDto extends BaseAddDto {
    InspectionId: number = 0;
    IndexId: number = 0;
    Grade: number = 0;
}
export class InspectionIndexChangeDto extends BaseChangeDto {
    InspectionId: number = 0;
    IndexId: number = 0;
    Grade: number = 0;
}