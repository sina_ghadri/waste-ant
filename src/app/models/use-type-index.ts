import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { UseTypeGetDto } from './use-type';
import { IndexGetDto } from '.';

export class UseTypeIndexGetDto extends BaseGetDto {
    UseTypeId: number = 0;
    IndexId: number = 0;

    UseType: UseTypeGetDto;
    Index: IndexGetDto;
}
export class UseTypeIndexAddDto extends BaseAddDto {
    UseTypeId: number = 0;
    IndexId: number = 0;
}
export class UseTypeIndexChangeDto extends BaseChangeDto {
    UseTypeId: number = 0;
    IndexId: number = 0;
}