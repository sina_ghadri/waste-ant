import { BaseGetDto, BaseAddDto, BaseChangeDto } from '../libs/bitspco/models/base';
import { UserGetDto } from '../libs/identity/models/user';
import { StationGetDto } from './station';
import { InspectionIndexAddDto, InspectionIndexGetDto } from './inspection-index';

export class InspectionGetDto extends BaseGetDto {
    UserId: number = 0;
    StationId: number = 0;
    Time: string = '';

    User: UserGetDto;
    Station: StationGetDto;

    Indices: InspectionIndexGetDto[];
}
export class InspectionAddDto extends BaseAddDto {
    StationId: number = 0;

    Station: StationGetDto;
    Indices: InspectionIndexAddDto[];
}
export class InspectionChangeDto extends BaseChangeDto {
    Time: string = '';
    
}