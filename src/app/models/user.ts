import { prop, required, compare, pattern } from '@rxweb/reactive-form-validators';

export class UserProfile {
    @prop()
    @required()
    Firstname: string = '';
    @prop()
    @required()
    Lastname: string = '';
    @prop()
    Thumbnail: string = '';
    @prop()
    @required()
    StartTime: string = '';
    @prop()
    EndTime: string = '';
    @prop()
    @required()
    @pattern({expression:{'NationalCode': /^[0-9]{10}$/} })
    NationalCode: string = '';
    @prop()
    @pattern({expression:{'Telephone': /^[0-9]{11}$/} })
    Telephone: string = '';
    @prop()
    @pattern({expression:{'Cellphone': /^09[0-9]{9}$/} })
    Cellphone: string = '';

    @prop()
    BrandName: string = '';
    @prop()
    BrandLogo: string = '';
}

export class UserChangePassword {
    @prop()
    Password: string;
    @prop()
    @compare({fieldName: 'Password'})
    RePassword: string;
}